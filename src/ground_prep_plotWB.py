import numpy as np
import lib.plotdefs as pd
from pathlib import Path
import sys

infiles = (
    [Path(arg) for arg in sys.argv[1:]]
    if len(sys.argv[1:]) >= 1 else
    [(Path("output")
        .joinpath("ground_prep")
        .joinpath("W-B_D=+0.000MHz_aa=+0.000pi_bb=+0.000pi_th=0.000pi_W=0.050-0.500MHz_B=0.000-15.000G.npz")
    )]
)

for infile in infiles:
    print(infile)

    data = np.load(str(infile))
    W = data["W"]
    B = data["B"]
    F = data["F"]

    dW = W[1] - W[0]
    dB = B[1] - B[0]

    (pd.Plotter()
        .imshow(
            F,
            extent=[
                B[0] - dB / 2.0, B[-1] + dB / 2.0,
                W[0] - dW / 2.0, W[-1] + dW / 2.0,
            ],
            cmap="jet",
            aspect="auto",
            origin="lower",
        )
        .colorbar()
        .contour(
            B, W, F,
            levels=[
                0.5, 0.6, 0.7, 0.8, 0.9,
                0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99, 1.00
            ],
            colors=5 * ["k"] + 10 * ["w"],
            linewidths=0.5,
        )
        .ggrid().grid(False, which="both")
        .set_xlabel("Field strength [G]")
        .set_ylabel("Drive strength [MHz]")
        .set_clabel("Fidelity")
        .savefig(infile.with_suffix(".png"))
        .close()
    )

