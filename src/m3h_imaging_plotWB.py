import numpy as np
import lib.plotdefs as pd
from pathlib import Path
import sys

infiles = (
    [Path(arg) for arg in sys.argv[1:]]
    if len(sys.argv[1:]) >= 1 else
    [(Path("output")
        .joinpath("m3h_imaging")
        .joinpath("W-B_D=+0.000MHz_aa=-0.611pi_bb=+0.250pi_th=0.750pi.npz")
    )]
)

Y = 182e-3

for infile in infiles:
    print(infile)

    data = np.load(str(infile))
    W = data["W"]
    B = data["B"]
    inF = data["inF"]

    BB, WW = np.meshgrid(B, W)
    s = WW**2 / (Y**2 / 2)
    rho_ee = s / (1 + s) # assume zero detuning
    # pd.Plotter().imshow(s, origin="lower", cmap="jet", aspect="auto").show().close()

    dW = W[1] - W[0]
    dB = B[1] - B[0]

    (pd.Plotter()
        .imshow(
            np.log10(inF),
            extent=[
                B[0] - dB / 2.0, B[-1] + dB / 2.0,
                W[0] - dW / 2.0, W[-1] + dW / 2.0,
            ],
            cmap="jet",
            aspect="auto",
            origin="lower",
        )
        .colorbar()
        .ggrid().grid(False, which="both")
        .set_xlabel("Field strength [G]")
        .set_ylabel("Drive strength [MHz]")
        .set_clabel("$\\log_{10} (\\mathrm{score})$")
        .savefig(infile.with_suffix(".png"))
        .close()
    )

    (pd.Plotter()
        .imshow(
            (1 - inF) * (Y / 2 * rho_ee) * 1000.0,
            extent=[
                B[0] - dB / 2.0, B[-1] + dB / 2.0,
                W[0] - dW / 2.0, W[-1] + dW / 2.0,
            ],
            cmap="jet",
            aspect="auto",
            origin="lower",
        )
        .colorbar()
        .ggrid().grid(False, which="both")
        .set_xlabel("Field strength [G]")
        .set_ylabel("Drive strength [MHz]")
        .set_clabel("$(1 - \\mathrm{score}) \\times R$ [kHz]")
        .savefig(infile.with_stem(infile.stem + "_scatt").with_suffix(".png"))
        .close()
    )

