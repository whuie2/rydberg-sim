#![allow(dead_code)]
#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(clippy::needless_return)]

use std::{
    fs,
    path::PathBuf,
    f64::consts::PI,
};
use ndarray as nd;
use num_complex::Complex64 as C64;
use sim::{
    loop_call,
    write_npz,
    rabi,
    systems::rydberg::{
        self,
        MU_C,
        MU_R,
        RydbergState as CR,
        RydbergBasis,
        PulseType,
    },
    hilbert::{
        self,
        BasisState,
        Basis,
    },
};

fn do_single_atom(B: f64, chi: f64, outfile: Option<PathBuf>) -> f64 {
    let config: rydberg::Config = rydberg::load_config(None);

    let t: nd::Array1<f64>
        = nd::Array::range(
            0.0,
            2.0 * (2.0 * PI / config.W),
            (2.0 * PI / config.W) / 1500.0
        ); // us

    let basis: RydbergBasis
        = RydbergBasis::from_pairs([
            (CR::c0, 2.0 * PI * CR::c0.spin().1 * MU_C * B),
            (CR::c1, 2.0 * PI * CR::c1.spin().1 * MU_C * B),
            (CR::r1m, 2.0 * PI * CR::r1m.spin().1 * MU_R * B),
            (CR::r0m, 2.0 * PI * CR::r0m.spin().1 * MU_R * B),
            (CR::r0p, 2.0 * PI * CR::r0p.spin().1 * MU_R * B),
            (CR::r1p, 2.0 * PI * CR::r1p.spin().1 * MU_R * B),
        ]);

    let w: f64
        = basis.get_energy(&CR::r1p).unwrap()
        - basis.get_energy(&CR::c1).unwrap();

    let H: nd::Array3<C64>
        = rydberg::H_pulse(PulseType::Sigma_p,
            basis.clone(), w, config.W, chi, t.view(), None)
        .unwrap();
    let psi0: nd::Array1<C64> = basis.get_array(&CR::c1).unwrap();
    let phi: nd::Array1<C64> = basis.get_array(&CR::c0).unwrap();
    let psi: nd::Array2<C64> = rabi::schrodinger_evolve_rk4(&psi0, &H, &t)
        .unwrap();

    let a_psi0: nd::Array1<C64> = psi.t().dot(&psi0);
    let P_psi0: nd::Array1<f64> = a_psi0.mapv(|a| (a * a.conj()).re);
    let a_phi: nd::Array1<C64> = psi.t().dot(&phi);
    let (k0, _): (usize, _) = rabi::find_2pi_idx(&P_psi0, 1, 1e-3);

    if let Some(filename) = outfile {
        write_npz!(
            filename,
            arrays: {
                "t" => &t,
                "psi" => &psi,
                "c0" => &basis.get_array(&CR::c0).unwrap(),
                "c1" => &basis.get_array(&CR::c1).unwrap(),
                "r1m" => &basis.get_array(&CR::r1m).unwrap(),
                "r0m" => &basis.get_array(&CR::r0m).unwrap(),
                "r0p" => &basis.get_array(&CR::r0p).unwrap(),
                "r1p" => &basis.get_array(&CR::r1p).unwrap(),
            }
        );
    }

    return hilbert::phase_angle(a_phi[k0] / a_psi0[k0]);
}

fn single_atom_B_chi(outpath: PathBuf, printflag: bool) {
    if printflag { println!("single_atom_B_chi"); }
    let config: rydberg::Config = rydberg::load_config(None);

    let outfile: PathBuf
        = outpath.join(
            format!("single_W={:.1}MHz.npz",
                config.W / (2.0 * PI)
            ).as_str()
        );

    if !outfile.is_file() {
        let B: nd::Array1<f64>
            = nd::Array::linspace(0.0, config.Bmax, config.ND);
        let chi: nd::Array1<f64>
            = nd::Array::logspace(10.0,
                -2.0, (2.0_f64 / 3.0).log10(), config.ND);
        let caller = |ijk: Vec<usize>| -> (f64,) {
            ( do_single_atom(B[ijk[1]], chi[ijk[0]], None) ,)
        };
        let (PH,): (nd::ArrayD<f64>,)
            = loop_call!(
                caller => (PH: f64,),
                vars: { chi, B }
            );
        write_npz!(
            outfile,
            arrays: {
                "B" => &B,
                "chi" => &chi,
                "PH" => &PH,
            }
        );
    } else {
        println!("  Found existing data file; skipping computation...");
    }
}

fn main() {
    let outpath = PathBuf::from("output/rydberg-phases");
    if !outpath.is_dir() {
        fs::create_dir_all(outpath.as_path())
            .unwrap_or_else(|_| {
                panic!(
                    "Couldn't create directory {:?}",
                    outpath.to_str().unwrap()
                )
            });
        println!(":: mkdir -p {:?}", outpath.to_str().unwrap());
    }

    let config: rydberg::Config = rydberg::load_config(None);

    println!(
        "rydberg fidelities\n\
        ==================\n\
        Omega = {} MHz\n\
        U = {} MHz\n\
        ND = {}\n\
        ==================",
        config.W / (2.0 * PI),
        config.U / (2.0 * PI),
        config.ND
    );

    single_atom_B_chi(outpath, true);
}

