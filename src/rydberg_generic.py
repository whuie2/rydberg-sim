import numpy as np
import whooie.pyplotdefs as pd
from itertools import product

data = np.load("output/rydberg-generic/traces_ger.npz")
labels = ["".join(s for s in ss) for ss in product(*(2 * [["g", "e", "r"]]))]
# labels = ["".join(s for s in ss) for ss in product(*(2 * [["g", "r"]]))]

init = "rr"
P = pd.Plotter()
for k, (label, trace) in enumerate(zip(labels, data[init])):
    (
        P
        .plot(data["t"], abs(trace)**2, color=f"C{k % 10}", label=label)
    )
(
    P
    .set_xlabel("Time [μs]")
    .set_ylabel("Probability")
    .ggrid()
    .legend()
    .show()
)

def value_str(x: float) -> str:
    return "${:+.3f}$".format(x)

k0 = abs(data["t"] - 1 / np.sqrt(2) / 2).argmin()
A = np.array([data[init][:, k0] for init in labels]).T
print(A)
P = pd.Plotter.new(ncols=2, sharex=True, sharey=True, as_plotarray=True)
(
    P
    [0]
    .imshow(A.real, cmap=pd.colormaps["fire-ice"], vmin=-1.0, vmax=+1.0)
    .set_title("Re")
    .grid(False, which="both")
    [1]
    .imshow(A.imag, cmap=pd.colormaps["fire-ice"], vmin=-1.0, vmax=+1.0)
    .set_title("Im")
    .grid(False, which="both")
)

for i in range(A.shape[0]):
    for j in range(A.shape[1]):
        P[0].text(
            j, i,
            value_str(A.real[i, j]),
            ha="center", va="center",
            fontsize=3.0,
        )
        P[1].text(
            j, i,
            value_str(A.imag[i, j]),
            ha="center", va="center",
            fontsize=3.0,
        )

pd.show()
