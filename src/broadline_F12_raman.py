from __future__ import annotations
import numpy as np
import numpy.fft as fft
import lib.systems.broadline_F12 as broadline
from lib.systems.broadline_F12 import (
    BroadlineState as GB,
    MU_Br,
    MU_Gr,
    PulseType,
)
import lib.hilbert as hilbert
import lib.rabi as rabi
import lib.plotdefs as pd
from pathlib import Path
import sys

ext = ".png"
outdir = Path("output/broadline_F12_raman")
if not outdir.is_dir():
    print(f":: mkdir -p {outdir}")
    outdir.mkdir(parents=True)
CMAP = "rainbow"

config = broadline.load_config(Path("config.toml"))

def find_first_max(y):
    y0 = y[0]
    k0 = 0
    for k, yk in enumerate(y):
        if yk < y0:
            break
        else:
            y0 = yk
            k0 = k
    return k0

def do_raman(Delta: float=-200.0, ps: float=np.pi / 4.0, ch: float=0.0,
        B: float=config.B, plotflag: bool=False) -> (float, float, float):
    outfile_base = outdir.joinpath(
        "monochr-cw"
        f"_W={config.W / 2.0 / np.pi:.1f}MHz"
        f"_D={Delta:.3f}"
        f"_psi={ps / np.pi:.3f}pi"
        f"_chi={ch / np.pi:.3f}pi"
        f"_B={B:.1f}G.ext"
    )

    t = np.arange(
        0.0,
        5.0,
        2.0 * np.pi / config.W / 1000,
    ) # us
    
    basis = hilbert.Basis({
        GB.g0: 2.0 * np.pi * broadline.zm(GB.g0, B),
        GB.g1: 2.0 * np.pi * broadline.zm(GB.g1, B),
        GB.b0: 2.0 * np.pi * broadline.zm(GB.b0, B),
        GB.b1: 2.0 * np.pi * broadline.zm(GB.b1, B),
    })
    d = 2.0 * np.pi * Delta
    w = basis.get_energy(GB.b0) - basis.get_energy(GB.g0) + d

    psi0 = basis.get_array(GB.g0)
    phi = basis.get_array(GB.g1)
    bad = basis.get_array(GB.b0)
    H = broadline.H_poincare(
        basis, w, config.W, psi=ps, chi=ch, theta=np.pi / 2.0, t=t)
    psi = rabi.schrodinger_evolve_rk4(psi0, H, t)

    P_phi = np.abs(phi.T.conj() @ psi)**2
    P_bad = np.abs(bad.T.conj() @ psi)**2
    
    f = fft.fftfreq(P_phi.shape[0], t[1] - t[0])[:t.shape[0] // 2]
    ff = fft.fft(P_phi - P_phi.mean(), norm="ortho")[:t.shape[0] // 2]
    FF = np.abs(ff)**2

    f_sel = f[np.where(f < config.W / (2.0 * np.pi))]
    FF_sel = FF[np.where(f < config.W / (2.0 * np.pi))]

    # Find the 2pi time by both Fourier transform and first maximum; if they
    # agree to within 5% of the Fourier transform, prefer first maximum
    W_eff_FT = f_sel[find_first_max(FF_sel)]
    T_2pi_FT = 1 / W_eff_FT
    T_2pi_FM \
        = t[rabi.find_2pi_idx(np.abs(psi0.T.conj() @ psi)**2, eps=0.9e-2)[0]]
    if abs(T_2pi_FT - T_2pi_FM) / T_2pi_FT <= 0.05:
        T_2pi = T_2pi_FM
        W_eff = 1 / T_2pi_FM
    else:
        W_eff = W_eff_FT
        T_2pi = T_2pi_FT

    F = P_phi[np.where(t < T_2pi)].max()
    F_bad = P_bad[np.where(t < T_2pi)].max()

    if plotflag:
        measure = [
            ("g_0", basis.get_array(GB.g0), "C0"),
            ("g_1", basis.get_array(GB.g1), "C1"),
            ("b_0", basis.get_array(GB.b0), "C2"),
            ("b_1", basis.get_array(GB.b1), "C3"),
        ]
        P = pd.Plotter.new(nrows=2)
        for l, s, c in measure:
            P[0].plot(
                t,
                np.abs(s.T.conj() @ psi)**2,
                label=f"$\\left| {l} \\right\\rangle$",
                color=c
            )
        (P[0]
            .axvline(T_2pi, color="0.65", linestyle="--")
            .legend(loc="center right", fontsize="xx-small")
            .ggrid()
            .set_xlabel("Time [μs]")
            .set_ylabel("Probability")
            .set_title(
                f"$\\Omega = {config.W / (2.0 * np.pi):.1f}$ MHz"
                f"; $B = {B:.1f}$ G"
                f"; $\\Delta = {Delta:.1f}$ MHz"
                f"\n$\\psi / \\pi = {ps / np.pi:.3f}$"
                f"; $\\chi / \\pi = {ch / np.pi:.3f}$"
            )
        )
        N = FF_sel.shape[0]
        FF_max = np.log10(FF_sel.max())
        FF_min = np.log10(FF_sel[N // 2:].min())
        FF_range = FF_max - FF_min
        (P[1]
            .semilogy(f_sel, FF_sel, color="C1")
            .axvline(W_eff, color="0.65", linestyle="--")
            .ggrid()
            .set_ylim(
                10**(FF_min - 0.05 * FF_range), 10**(FF_max + 0.05 * FF_range))
            .set_xlabel("Frequency [MHz]")
            .set_ylabel(
                "$|\\hat{F}[P_{\\left| g_1 \\right\\rangle}]|^2$ [arb.]")
        )
        (P
            .savefig(outfile_base.with_suffix(ext))
            .close()
        )

        np.savez(
            str(outfile_base.with_suffix(".npz")),
            t=t,
            g0=basis.get_array(GB.g0),
            g1=basis.get_array(GB.g1),
            b0=basis.get_array(GB.b0),
            b1=basis.get_array(GB.b1),
            psi=psi,
        )

    return F, F_bad, W_eff

def detuning(det_max: float=300.0, B: float=config.B, ND: int=config.ND,
        printflag: bool=True):
    outfile_base = outdir.joinpath("detuning")
    
    if printflag:
        print("detuning")
        z = int(np.ceil(np.log10(ND))) + 1
        fmt = "\r  {:s} / {:s}  ({:6.2f}%) "

    if not outfile_base.with_suffix(".npz").is_file():
        D = np.linspace(-det_max, 0.0, ND)
        F = np.zeros(D.shape, dtype=np.float64)
        F_bad = np.zeros(D.shape, dtype=np.float64)
        W_eff = np.zeros(D.shape, dtype=np.float64)
        if printflag:
            print(fmt.format(
                    str(0).rjust(z), str(ND).rjust(z), 0.0
                ),
                end="",flush=True
            )
        for k, d in enumerate(D):
            F[k], F_bad[k], W_eff[k] \
                    = do_raman(Delta=-abs(d), B=B, plotflag=False)
            if printflag:
                print(fmt.format(
                        str(k + 1).rjust(z), str(ND).rjust(z),
                        100.0 * (k + 1) / D.shape[0]
                    ),
                    end="", flush=True
                )
        print("")
        np.savez(
            str(outfile_base.with_suffix(".npz")),
            D=D,
            F=F,
            F_bad=F_bad,
            W_eff=W_eff
        )
    else:
        if printflag:
            print("  Found data file; plotting only...")
        data = np.load(str(outfile_base.with_suffix(".npz")))
        D = data["D"]
        F = data["F"]
        F_bad = data["F_bad"]
        W_eff = data["W_eff"]

    P = pd.Plotter.new(nrows=2, sharex=True)
    (P[0]
        .semilogy(D, F, color="C0", label="$\\left| g_1 \\right\\rangle$")
        .semilogy(D, F_bad, color="k", label="$\\left| b_0 \\right\\rangle$")
        # .axvline(0.0, color="0.65", linestyle="--")
        .set_ylim(1e-3, 1.5)
        .legend(fontsize="xx-small")
        # .axvline(-MU_T * config.B, color="0.65", linestyle="--")
        .ggrid()
        .set_ylabel("Probability")
        .set_title(
            f"$\\Omega = {config.W / (2.0 * np.pi):.1f}$ MHz"
            f"; $B = {B:.1f}$ G"
        )
    )
    (P[1]
        .semilogy(D, W_eff, color="C1")
        # .axvline(0.0, color="0.65", linestyle="--")
        # .axvline(-MU_T * config.B, color="0.65", linestyle="--")
        .ggrid()
        .set_ylabel("$\\Omega_{eff}$ [MHz]")
        .set_xlabel("$\\Delta$ [MHz]")
    )
    # with np.errstate(divide="ignore"):
    #     P[1].semilogy(D, (config.W / (2.0 * np.pi))**2 / (4.0 * np.abs(D)),
    #         color="k", linestyle="--")
    (P
        .tight_layout(h_pad=0.01)
        .savefig(outfile_base.with_suffix(ext))
        .close()
    )

def polarization_angle(detuning: float=200.0, B_max: float=500.0,
        ND: int=config.ND, printflag: bool=True):
    outfile_base = outdir.joinpath("polarization_angle")

    if printflag:
        print("polarization_angle")
        z = int(np.ceil(np.log10(ND))) + 1
        fmt = "\r  {:s} / {:s};  {:s} / {:s}  ({:6.2f}%) "

    if not outfile_base.with_suffix(".npz").is_file():
        psi = np.linspace(np.pi / 12, 5 * np.pi / 12, ND)
        B = np.linspace(0.0, B_max, ND)
        F = np.zeros((psi.shape[0], B.shape[0]), dtype=np.float64)
        F_bad = np.zeros((psi.shape[0], B.shape[0]), dtype=np.float64)
        W_eff = np.zeros((psi.shape[0], B.shape[0]), dtype=np.float64)
        if printflag:
            print(fmt.format(
                    str(1).rjust(z), str(ND).rjust(z),
                    str(0).rjust(z), str(ND).rjust(z),
                    0.0
                ),
                end="",flush=True
            )
        for i, ps in enumerate(psi):
            for j, b in enumerate(B):
                F[i, j], F_bad[i, j], W_eff[i, j] \
                    = do_raman(Delta=-abs(detuning), ps=ps, B=b, plotflag=False)
                if printflag:
                    print(fmt.format(
                            str(i + 1).rjust(z), str(psi.shape[0]).rjust(z),
                            str(j + 1).rjust(z), str(B.shape[0]).rjust(z),
                            100.0 * (i * B.shape[0] + j + 1)
                                / (psi.shape[0] * B.shape[0])
                        ),
                        end="", flush=True
                    )
        print("")
        np.savez(
            str(outfile_base.with_suffix(".npz")),
            psi=psi,
            B=B,
            F=F,
            F_bad=F_bad,
            W_eff=W_eff
        )
    else:
        if printflag:
            print("  Found data file; plotting only...")
        data = np.load(str(outfile_base.with_suffix(".npz")))
        psi = data["psi"]
        B = data["B"]
        F = data["F"]
        F_bad = data["F_bad"]
        W_eff = data["W_eff"]

    BB, PS = np.meshgrid(B, psi)
    dB = abs(B[1] - B[0])
    dpsi = abs(psi[1] - psi[0])

    (pd.Plotter()
        .imshow(np.log10(1.0 - F),
            extent=[
                B[0] - dB / 2.0, B[-1] + dB / 2.0,
                (psi[0] - dpsi / 2.0) / np.pi, (psi[-1] + dpsi / 2.0) / np.pi,
            ],
            origin="lower", aspect="auto", cmap=CMAP
        )
        .colorbar()
        .contour(BB, PS / np.pi, np.log10(1.0 - F),
            levels=[
                np.log10(1.0 - 0.999),
                np.log10(1.0 - 0.99),
                np.log10(1.0 - 0.9),
            ],
            colors="k", linestyles="-"
        )
        .set_xlabel("Field strength [G]")
        .set_ylabel("$\\psi / \\pi$")
        .set_clabel("$\\log_{10} (1 - P_{\\left| g_1 \\right\\rangle})$")
        .set_title(f"$\\Delta = {-detuning:.1f}$ MHz")
        .grid(False)
        .savefig(
            outfile_base
                .with_stem(outfile_base.stem + "_fidelity")
                .with_suffix(ext)
        )
    )

    (pd.Plotter()
        .imshow(np.log10(F_bad),
            extent=[
                B[0] - dB / 2.0, B[-1] + dB / 2.0,
                (psi[0] - dpsi / 2.0) / np.pi, (psi[-1] + dpsi / 2.0) / np.pi,
            ],
            origin="lower", aspect="auto", cmap=CMAP
        )
        .colorbar()
        .contour(BB, PS / np.pi, np.log10(F_bad),
            levels=[
                np.log10(0.001),
                np.log10(0.01),
                np.log10(0.1),
            ],
            colors="k", linestyles="-"
        )
        .set_xlabel("Field strength [G]")
        .set_ylabel("$\\psi / \\pi$")
        .set_clabel("$\\log_{10} (P_{\\left| b_0 \\right\\rangle})$")
        .set_title(f"$\\Delta = {-detuning:.1f}$ MHz")
        .grid(False)
        .savefig(
            outfile_base
                .with_stem(outfile_base.stem + "_b0-population")
                .with_suffix(ext)
        )
    )

    (pd.Plotter()
        .imshow(np.log10(W_eff),
            extent=[
                B[0] - dB / 2.0, B[-1] + dB / 2.0,
                (psi[0] - dpsi / 2.0) / np.pi, (psi[-1] + dpsi / 2.0) / np.pi,
            ],
            origin="lower", aspect="auto", cmap=CMAP
        )
        .colorbar()
        .contour(BB, PS / np.pi, np.log10(W_eff),
            levels=[
                np.log10(1.0),
            ],
            colors="k", linestyles="-"
        )
        .set_xlabel("Field strength [G]")
        .set_ylabel("$\\psi / \\pi$")
        .set_clabel("$\\log_{10} \\Omega_{eff}$ [MHz]")
        .set_title(f"$\\Delta = {-detuning:.1f}$ MHz")
        .grid(False)
        .savefig(
            outfile_base
                .with_stem(outfile_base.stem + "_rabi-eff")
                .with_suffix(ext)
        )
    )

def main():
    print("broadline_raman (F = 1/2)")
    print("=========================")

    print("single-run tests:")
    tests = [
        {"Delta":   -0.0, "ps": np.pi / 12, "B": 500.0, "plotflag": True},
        {"Delta":  -50.0, "ps": np.pi / 12, "B": 500.0, "plotflag": True},
        {"Delta": -100.0, "ps": np.pi / 12, "B": 500.0, "plotflag": True},
        {"Delta": -200.0, "ps": np.pi / 12, "B": 500.0, "plotflag": True},
        {"Delta": -300.0, "ps": np.pi / 12, "B": 500.0, "plotflag": True},
    ]
    z = int(round(np.log10(len(tests))))
    fmt = "\r  {:s} / {:s}  ({:6.2f}%) "
    print(
        fmt.format(str(0).rjust(z), str(len(tests)).rjust(z), 0.0),
        end="", flush=True
    )
    for k, kwargs in enumerate(tests):
        do_raman(**kwargs)
        print(
            fmt.format(
                str(k + 1).rjust(z), str(len(tests)).rjust(z),
                100.0 * (k + 1) / len(tests)
            ),
            end="", flush=True
        )
    print("")

    Dmax = config.W**2 / (2.0 * config.Weff) / (2.0 * np.pi)
    
    detuning(Dmax + 100.0, B=200.0)

    polarization_angle(detuning=125.0, B_max=500.0, ND=100)

if __name__ == "__main__":
    main()

