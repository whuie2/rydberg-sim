#![allow(dead_code)]
#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(clippy::needless_return)]

use std::{
    fs,
    path::PathBuf,
    f64::consts::PI,
};
use ndarray::{
    self as nd,
    s,
};
use num_complex::Complex64 as C64;
use sim::{
    loop_call,
    write_npz,
    rabi,
    systems::clock::{
        self,
        ClockState as GC,
        ClockBasis,
        PulseType,
    },
    hilbert::{
        Basis,
    },
};

fn do_single_atom(B: f64, chi: f64, outfile: Option<PathBuf>) -> f64 {
    let config: clock::Config = clock::load_config(None);

    let t: nd::Array1<f64>
        = nd::Array::range(
            0.0,
            20.0,
            (2.0 * PI / config.W) / 1500.0
        ); // us

    let basis: ClockBasis
        = ClockBasis::from_pairs([
            (GC::g0, 2.0 * PI * clock::zm(GC::g0, B)),
            (GC::g1, 2.0 * PI * clock::zm(GC::g1, B)),
            (GC::c0, 2.0 * PI * clock::zm(GC::c0, B)),
            (GC::c1, 2.0 * PI * clock::zm(GC::c1, B)),
        ]);

    let w: f64
        = basis.get_energy(&GC::c1).unwrap()
        - basis.get_energy(&GC::g0).unwrap();

    let H: nd::Array3<C64>
        = clock::H_pulse(PulseType::Sigma_p,
            basis.clone(), w, config.W, chi, t.view(), None)
        .unwrap();
    let psi0: nd::Array1<C64> = basis.get_array(&GC::g0).unwrap();
    let phi: nd::Array1<C64> = basis.get_array(&GC::c1).unwrap();
    let psi: nd::Array2<C64> = rabi::schrodinger_evolve_rk4(&psi0, &H, &t)
        .unwrap();

    let P_psi0: nd::Array1<f64>
        = psi.t().dot(&psi0).mapv(|a| (a * a.conj()).re);
    let P_phi: nd::Array1<f64>
        = psi.t().dot(&phi).mapv(|a| (a * a.conj()).re);
    let (k0, _): (usize, _) = rabi::find_2pi_idx(&P_psi0, 1, 1e-3);

    if let Some(filename) = outfile {
        write_npz!(
            filename,
            arrays: {
                "t" => &t,
                "psi" => &psi,
                "g0" => &basis.get_array(&GC::g0).unwrap(),
                "g1" => &basis.get_array(&GC::g1).unwrap(),
                "c0" => &basis.get_array(&GC::c0).unwrap(),
                "c1" => &basis.get_array(&GC::c1).unwrap(),
            }
        );
    }

    return *P_phi.slice(s![..k0 + 1]).iter()
        .max_by(|x, y| x.partial_cmp(y).unwrap()).unwrap();
}

fn single_atom_B_chi(outpath: PathBuf, printflag: bool) {
    if printflag { println!("single_atom_B_chi"); }
    let config: clock::Config = clock::load_config(None);

    let outfile: PathBuf
        = outpath.join(
            format!("single_W={:.1}MHz.npz",
                config.W / (2.0 * PI)
            ).as_str()
        );
    if !outfile.is_file() {
        let B: nd::Array1<f64>
            = nd::Array::linspace(0.0, config.Bmax, config.ND);
        let chi: nd::Array1<f64>
            = nd::Array::logspace(10.0,
                -2.0, (2.0_f64 / 3.0).log10(), config.ND);
        let caller = |ijk: Vec<usize>| -> (f64,) {
            ( do_single_atom(B[ijk[1]], chi[ijk[0]], None) ,)
        };
        let (F,): (nd::ArrayD<f64>,)
            = loop_call!(
                caller => (F: f64,),
                vars: { chi, B }
            );
        write_npz!(
            outfile,
            arrays: {
                "B" => &B,
                "chi" => &chi,
                "F" => &F,
            }
        );
    } else {
        println!("  Found existing data file; skipping computation...");
    }
}

fn main() {
    let outpath = PathBuf::from("output/clock-fidelities");
    if !outpath.is_dir() {
        fs::create_dir_all(outpath.as_path())
            .unwrap_or_else(|_| {
                panic!(
                    "Couldn't create directory {:?}",
                    outpath.to_str().unwrap()
                )
            });
        println!(":: mkdir -p {:?}", outpath.to_str().unwrap());
    }

    let config: clock::Config = clock::load_config(None);

    println!(
        "clock fidelities\n\
        ================\n\
        Omega = {} MHz\n\
        ND = {}\n\
        ================",
        config.W / (2.0 * PI),
        config.ND
    );

    single_atom_B_chi(outpath, true);
}
