from __future__ import annotations
import numpy as np
import lib.systems.rydberg as rydberg
from lib.systems.rydberg import (
    RydbergState as CR,
    MU_R,
    MU_C,
    PulseType,
)
import lib.hilbert as hilbert
import lib.rabi as rabi
import lib.plotdefs as pd
from pathlib import Path
import sys

ext = ".pdf"
outdir = Path("output/rydberg_fidelities")
if not outdir.is_dir():
    print(f":: mkdir -p {outdir}")
    outdir.mkdir(parents=True)

config = rydberg.load_config(Path("config.toml"))

pd.set_font("myriad-ttf/MyriadPro-Regular.ttf", "MyriadPro")
CMAP = pd.colormaps["fire-ice"]

def do_single_atom(B: float=200.0, chi: float=0.0, plotflag: bool=False,
        cropflag: bool=False):
    outfile_base = outdir.joinpath(f"single_B={B:.1f}G_chi={chi:.3f}.ext")

    t = np.arange(
        0.000,
        2.000,
        2 * np.pi / config.W / 1500
    ) # us
    if cropflag:
        t = t[np.where(t < 4.0 * np.pi / config.W)]

    basis = hilbert.Basis({
        CR.c0: 2.0 * np.pi * CR.c0.spin()[1] * MU_R * B,
        CR.c1: 2.0 * np.pi * CR.c1.spin()[1] * MU_R * B,
        CR.r1m: 2.0 * np.pi * CR.r1m.spin()[1] * MU_R * B,
        CR.r0m: 2.0 * np.pi * CR.r0m.spin()[1] * MU_R * B,
        CR.r0p: 2.0 * np.pi * CR.r0p.spin()[1] * MU_R * B,
        CR.r1p: 2.0 * np.pi * CR.r1p.spin()[1] * MU_R * B,
    })
    w = basis.get_energy(CR.r1p) - basis.get_energy(CR.c1)

    H = rydberg.H_pulse(rydberg.PulseType.Sigma_p, basis, w, config.W, chi, t)
    psi0 = basis.get_array(CR.c1)
    phi = basis.get_array(CR.r1p)
    psi = rabi.schrodinger_evolve_rk4(psi0, H, t)

    k0, _ = rabi.find_2pi_idx(np.abs(psi0.T.conj() @ psi)**2)
    P_phi = np.abs(phi.T.conj() @ psi)**2
    F = P_phi[:k0 + 1].max()

    if plotflag:
        measure = [
            ("c_0", basis.get_array(CR.c0), "C0"),
            ("c_1", basis.get_array(CR.c1), "C1"),
            ("r_1^-", basis.get_array(CR.r1m), "C2"),
            ("r_0^-", basis.get_array(CR.r0m), "C3"),
            ("r_0^+", basis.get_array(CR.r0p), "C4"),
            ("r_1^+", basis.get_array(CR.r1p), "C5"),
        ]
        P = pd.Plotter()
        for l, s, c in measure:
            P.plot(
                t,
                np.abs(s.T.conj() @ psi)**2,
                label=f"$\\left| {l} \\right\\rangle$",
                color=c
            )
        (P
            .axvline(t[k0], color="0.65", linestyle="--")
            .legend(loc="center left", fontsize="xx-small")
            .ggrid()
            .set_xlabel("Time [μs]")
            .set_ylabel("Probability")
            .set_title(f"$B = {B:.3f}$ G, $\\chi = {chi:.3f}$")
            .savefig(outfile_base.with_suffix(ext))
            .close()
        )
        np.savez(
            str(outfile_base.with_suffix(".npz")),
            t=t,
            c0=basis.get_array(CR.c0),
            c1=basis.get_array(CR.c1),
            r1m=basis.get_array(CR.r1m),
            r0m=basis.get_array(CR.r0m),
            r0p=basis.get_array(CR.r0p),
            r1p=basis.get_array(CR.r1p),
            psi=psi,
        )

    return F

def single_atom_F(Bmax: float=config.Bmax, chimax: float=2.0 / 3.0,
        printflag: bool=True):
    outfile_base = outdir.joinpath("single_atom_F.ext")

    if printflag:
        print("single_atom_F")
        z = int(np.ceil(np.log10(config.ND))) + 1
        fmt = "\r  {:s} / {:s}; {:s} / {:s}  ({:6.2f}%) "

    if not outfile_base.with_suffix(".npz").is_file():
        B = np.linspace(0.0, Bmax, config.ND)
        dB = B[1] - B[0]
        chi = np.logspace(-2.0, np.log10(chimax), config.ND)
        logchi = np.log10(chi)
        dchi = logchi[1] - logchi[0]
        F = np.zeros((chi.shape[0], B.shape[0]), dtype=np.float64)
        if printflag:
            print(fmt.format(
                    str(0).rjust(z), str(chi.shape[0]).rjust(z),
                    str(0).rjust(z), str(B.shape[0]).rjust(z),
                    0.0
                ),
                end="", flush=True
            )
        for i, x in enumerate(chi):
            for j, b in enumerate(B):
                F[i, j] = do_single_atom(b, x, plotflag=False, cropflag=True)
                if printflag:
                    print(fmt.format(
                            str(i + 1).rjust(z), str(chi.shape[0]).rjust(z),
                            str(j + 1).rjust(z), str(B.shape[0]).rjust(z),
                            100.0 * (i * B.shape[0] + j + 1)
                                / (chi.shape[0] * B.shape[0])
                        ),
                        end="", flush=True
                    )
        print("")
        np.savez(
            str(outfile_base.with_suffix(".npz")),
            B=B,
            chi=chi,
            F=F
        )
    else:
        if printflag:
            print("  Found data file; skipping...")
        data = np.load(str(outfile_base.with_suffix(".npz")))
        B = data["B"]
        dB = B[1] - B[0]
        chi = data["chi"]
        logchi = np.log10(chi)
        dchi = logchi[1] - logchi[0]
        F = data["F"]

    BB, XX = np.meshgrid(B, logchi)

    (pd.Plotter()
        .imshow(np.log10(1.0 - F),
            extent=[
                B.min() - dB / 2.0, B.max() + dB / 2.0,
                logchi.min() - dchi / 2.0, logchi.max() + dchi / 2.0,
            ],
            origin="lower", aspect="auto", cmap=CMAP
        )
        .colorbar()
        .contour(BB, XX, np.log10(1.0 - F), levels=[np.log10(1.0 - 0.99)],
            colors="k", linestyles="-"
        )
        .set_xlabel("Field strength [G]")
        .set_ylabel("$\\log_{10} \\chi$")
        .set_clabel("$\\log_{10} (1 - P_{\\left| r_1^+ \\right\\rangle})$")
        .grid(False)
        .savefig(outfile_base.with_suffix(ext))
        .close()
    )

    do_single_atom(B.min(), chi.min(), plotflag=True, cropflag=False)
    do_single_atom(B.min(), chi.max(), plotflag=True, cropflag=False)
    do_single_atom(B.max(), chi.min(), plotflag=True, cropflag=False)
    do_single_atom(B.max(), chi.max(), plotflag=True, cropflag=False)

def do_double_atom(B: float=200.0, chi: float=0.0, plotflag: bool=False,
        cropflag: bool=False):
    outfile_base = outdir.joinpath(f"double_B={B:.1f}G_chi={chi:.3f}.ext")

    t = np.arange(
        0.000,
        2.000,
        2 * np.pi / config.W / 1500
    ) # us
    if cropflag:
        t = t[np.where(t < 4.0 * np.pi / config.W)]

    basis = hilbert.Basis({
        CR.c0: 2.0 * np.pi * CR.c0.spin()[1] * MU_R * B,
        CR.c1: 2.0 * np.pi * CR.c1.spin()[1] * MU_R * B,
        CR.r1m: 2.0 * np.pi * CR.r1m.spin()[1] * MU_R * B,
        CR.r0m: 2.0 * np.pi * CR.r0m.spin()[1] * MU_R * B,
        CR.r0p: 2.0 * np.pi * CR.r0p.spin()[1] * MU_R * B,
        CR.r1p: 2.0 * np.pi * CR.r1p.spin()[1] * MU_R * B,
    })
    w = basis.get_energy(CR.r1p) - basis.get_energy(CR.c1)

    H = rydberg.H_pulse(rydberg.PulseType.Sigma_p, basis, w, config.W, chi, t)
    Basis, HH = rydberg.gen_multiatom(basis, 2, H, config.U * np.ones(t.shape))
    psi0 = Basis.get_array((CR.c1, CR.c1))
    phi = (
        Basis.get_array((CR.c1, CR.r1p))
        + Basis.get_array((CR.r1p, CR.c1))
    ) / np.sqrt(2.0)
    psi = rabi.schrodinger_evolve_rk4(psi0, HH, t)

    k0, _ = rabi.find_2pi_idx(np.abs(psi0.T.conj() @ psi)**2)
    P_phi = np.abs(phi.T.conj() @ psi)**2
    F = P_phi[:k0 + 1].max()

    if plotflag:
        measure = [
            ("c_1 c_1", Basis.get_array((CR.c1, CR.c1)), "C0"),
            ("B",
                (
                    Basis.get_array((CR.c1, CR.r1p))
                    + Basis.get_array((CR.r1p, CR.c1))
                ) / np.sqrt(2.0),
                "C1"
            ),
            ("D",
                (
                    Basis.get_array((CR.c1, CR.r1p))
                    - Basis.get_array((CR.r1p, CR.c1))
                ) / np.sqrt(2.0),
                "C2"
            ),
        ]
        P = pd.Plotter()
        for l, s, c in measure:
            P.plot(
                t,
                np.abs(s.T.conj() @ psi)**2,
                label=f"$\\left| {l} \\right\\rangle$",
                color=c
            )
        (P
            .axvline(t[k0], color="0.65", linestyle="--")
            .legend(loc="center left", fontsize="xx-small")
            .ggrid()
            .set_xlabel("Time [μs]")
            .set_ylabel("Probability")
            .set_title(f"$B = {B:.3f}$ G, $\\chi = {chi:.3f}$")
            .savefig(outfile_base.with_suffix(ext))
            .close()
        )
        np.savez(
            str(outfile_base.with_suffix(".npz")),
            t=t,
            **{
                ",".join([s1.name, s2.name]): Basis.get_array((s1, s2))
                for s1, s2 in Basis.arrays.keys()
            },
            psi=psi,
        )

    return F

def double_atom_F(Bmax: float=config.Bmax, chimax: float=2.0 / 3.0,
        printflag: bool=True):
    outfile_base = outdir.joinpath("double_atom_F.ext")

    if printflag:
        print("double_atom_F")
        z = int(np.ceil(np.log10(config.ND))) + 1
        fmt = "\r  {:s} / {:s}; {:s} / {:s}  ({:6.2f}%) "

    if not outfile_base.with_suffix(".npz").is_file():
        B = np.linspace(0.0, Bmax, config.ND)
        dB = B[1] - B[0]
        chi = np.logspace(-2.0, np.log10(chimax), config.ND)
        logchi = np.log10(chi)
        dchi = logchi[1] - logchi[0]
        F = np.zeros((chi.shape[0], B.shape[0]), dtype=np.float64)
        if printflag:
            print(fmt.format(
                    str(0).rjust(z), str(chi.shape[0]).rjust(z),
                    str(0).rjust(z), str(B.shape[0]).rjust(z),
                    0.0
                ),
                end="", flush=True
            )
        for i, x in enumerate(chi):
            for j, b in enumerate(B):
                F[i, j] = do_double_atom(b, x, plotflag=False, cropflag=True)
                if printflag:
                    print(fmt.format(
                            str(i + 1).rjust(z), str(chi.shape[0]).rjust(z),
                            str(j + 1).rjust(z), str(B.shape[0]).rjust(z),
                            100.0 * (i * B.shape[0] + j + 1)
                                / (chi.shape[0] * B.shape[0])
                        ),
                        end="", flush=True
                    )
        print("")
        np.savez(
            str(outfile_base.with_suffix(".npz")),
            B=B,
            chi=chi,
            F=F
        )
    else:
        if printflag:
            print("  Found data file; skipping...")
        data = np.load(str(outfile_base.with_suffix(".npz")))
        B = data["B"]
        dB = B[1] - B[0]
        chi = data["chi"]
        logchi = np.log10(chi)
        dchi = logchi[1] - logchi[0]
        F = data["F"]

    BB, XX = np.meshgrid(B, logchi)

    (pd.Plotter()
        .imshow(np.log10(1.0 - F),
            extent=[
                B.min() - dB / 2.0, B.max() + dB / 2.0,
                logchi.min() - dchi / 2.0, logchi.max() + dchi / 2.0,
            ],
            origin="lower", aspect="auto", cmap=CMAP
        )
        .colorbar()
        .contour(BB, XX, np.log10(1.0 - F), levels=[np.log10(1.0 - 0.99)],
            colors="k", linestyles="-"
        )
        .set_xlabel("Field strength [G]")
        .set_ylabel("$\\log_{10} \\chi$")
        .set_clabel("$\\log_{10} (1 - P_{\\left| r_1^+ \\right\\rangle})$")
        .grid(False)
        .savefig(outfile_base.with_suffix(ext))
        .close()
    )

    do_double_atom(B.min(), chi.min(), plotflag=True, cropflag=False)
    do_double_atom(B.min(), chi.max(), plotflag=True, cropflag=False)
    do_double_atom(B.max(), chi.min(), plotflag=True, cropflag=False)
    do_double_atom(B.max(), chi.max(), plotflag=True, cropflag=False)

if __name__ == "__main__":
    single_atom_F()
    # double_atom_F()

