from __future__ import annotations
import numpy as np
from lib import plot_bloch_sphere
import lib.systems.rydberg as rydberg
from lib.systems.rydberg import (
    RydbergState as CR,
    MU_R,
    MU_C,
    PulseType,
)
import lib.hilbert as hilbert
import lib.rabi as rabi
import lib.plotdefs as pd
from pathlib import Path
import sys

ext = ".pdf"
outdir = Path("output/rydberg_phases")
if not outdir.is_dir():
    print(f":: mkdir -p {outdir}")
    outdir.mkdir(parents=True)

config = rydberg.load_config(Path("config.toml"))

pd.set_font("myriad-ttf/MyriadPro-Regular.ttf", "MyriadPro")
CMAP = pd.colormaps["fire-ice"]

THETA0 = np.pi / 2.0
PHI0 = 0.0

def do_single_atom(B: float=200.0, chi: float=0.0, plotflag: bool=False,
        cropflag: bool=False):
    outfile_base = outdir.joinpath(f"single_B={B:.1f}G_chi={chi:.3f}.ext")

    t = np.arange(
        0.000,
        0.35,
        2 * np.pi / config.W / 1500
    ) # us
    if cropflag:
        t = t[np.where(t < 4.0 * np.pi / config.W)]

    basis = hilbert.Basis({
        CR.c0: 2.0 * np.pi * CR.c0.spin()[1] * MU_C * B,
        CR.c1: 2.0 * np.pi * CR.c1.spin()[1] * MU_C * B,
        CR.r1m: 2.0 * np.pi * CR.r1m.spin()[1] * MU_R * B,
        CR.r0m: 2.0 * np.pi * CR.r0m.spin()[1] * MU_R * B,
        CR.r0p: 2.0 * np.pi * CR.r0p.spin()[1] * MU_R * B,
        CR.r1p: 2.0 * np.pi * CR.r1p.spin()[1] * MU_R * B,
    })
    w = basis.get_energy(CR.r1p) - basis.get_energy(CR.c1)

    H = rydberg.H_pulse(rydberg.PulseType.Sigma_p, basis, w, config.W, chi, t)
    psi0 = (
        np.cos(THETA0 / 2.0) * basis.get_array(CR.c1)
        + np.exp(1.0j * PHI0) * np.sin(THETA0 / 2.0) * basis.get_array(CR.c0)
    )
    psi = rabi.schrodinger_evolve_rk4(psi0, H, t)

    c1 = basis.get_array(CR.c1)
    c0 = basis.get_array(CR.c0)
    r1p = basis.get_array(CR.r1p)
    k0, _ = rabi.find_2pi_idx(np.abs(psi.T @ c1.conj())**2)
    ph = hilbert.phase_angle(
        (psi[:, k0] @ c0.conj()) / (psi[:, k0] @ c1.conj())
    )

    if plotflag:
        measure = [
            ("c_1", c1, "C0"),
            ("c_0", c0, "C3"),
            ("r_1^+", r1p, "C1"),
        ]
        P = pd.Plotter.new(nrows=2, sharex=True)
        for l, s, c in measure:
            P[0].plot(
                t,
                np.abs(psi.T @ s.conj())**2,
                label=f"$\\left| {l} \\right\\rangle$",
                color=c
            )
            P[1].plot(
                t,
                hilbert.phase_angle(psi.T @ s.conj()) / np.pi,
                color=c
            )
        (P[1]
            .plot(
                t,
                hilbert.phase_angle(
                    (psi.T @ c0.conj()) / (psi.T @ c1.conj())
                ) / np.pi,
                color="k",
                label="$c_0 - c_1$"
            )
            .plot(
                t,
                hilbert.phase_angle(
                    (psi.T @ r1p.conj()) / (psi.T @ c1.conj())
                ) / np.pi,
                color="0.5",
                label="$r_1^+ - c_1$"
            )
        )
        (P[0]
            .axvline(t[k0], color="0.65", linestyle="--")
            .legend(loc="upper left", fontsize="xx-small")
            .ggrid()
            .set_ylabel("Probability")
            .set_title(f"$B = {B:.3f}$ G, $\\chi = {chi:.3f}$")
        )
        (P[1]
            .axvline(t[k0], color="0.65", linestyle="--")
            .legend(loc="upper left", fontsize="xx-small")
            .ggrid()
            .set_ylabel("$\\Delta \\varphi / \\pi$")
            .set_xlabel("Time [μs]")
        )
        (P
            .tight_layout(h_pad=0.1)
            .savefig(outfile_base.with_suffix(ext))
            .close()
        )

        (plot_bloch_sphere(
                psi.T @ c1.conj(), psi.T @ c0.conj(), k0, ["$c_1$", "$c_0$"])
            .savefig(outfile_base
                .with_stem("bloch-c1-c0_" + outfile_base.stem)
                .with_suffix(ext)
            )
            .close()
        )
        (plot_bloch_sphere(
                psi.T @ c1.conj(), psi.T @ r1p.conj(), k0, ["$c_1$", "$r_1^+$"])
            .savefig(outfile_base
                .with_stem("bloch-c1-r1p_" + outfile_base.stem)
                .with_suffix(ext)
            )
            .close()
        )

        np.savez(
            str(outfile_base.with_suffix(".npz")),
            t=t,
            c0=basis.get_array(CR.c0),
            c1=basis.get_array(CR.c1),
            r1m=basis.get_array(CR.r1m),
            r0m=basis.get_array(CR.r0m),
            r0p=basis.get_array(CR.r0p),
            r1p=basis.get_array(CR.r1p),
            psi=psi,
        )

    return ph

def single_atom_ph(Bmax: float=config.Bmax, chimax: float=2.0 / 3.0,
        printflag: bool=True):
    outfile_base = outdir.joinpath("single_atom_ph.ext")

    if printflag:
        print("single_atom_ph")
        z = int(np.ceil(np.log10(config.ND))) + 1
        fmt = "\r  {:s} / {:s}; {:s} / {:s}  ({:.2f}%) "

    if not outfile_base.with_suffix(".npz").is_file():
        B = np.linspace(0.0, Bmax, config.ND)
        dB = B[1] - B[0]
        chi = np.logspace(-2.0, np.log10(chimax), config.ND)
        logchi = np.log10(chi)
        dchi = logchi[1] - logchi[0]
        ph = np.zeros((chi.shape[0], B.shape[0]), dtype=np.float64)
        if printflag:
            print(fmt.format(
                    str(0).rjust(z), str(chi.shape[0]).rjust(z),
                    str(0).rjust(z), str(B.shape[0]).rjust(z),
                    0.0
                ),
                end="", flush=True
            )
        for i, x in enumerate(chi):
            for j, b in enumerate(B):
                ph[i, j] = do_single_atom(b, x, plotflag=False, cropflag=True)
                if printflag:
                    print(fmt.format(
                            str(i + 1).rjust(z), str(chi.shape[0]).rjust(z),
                            str(j + 1).rjust(z), str(B.shape[0]).rjust(z),
                            100.0 * (i * B.shape[0] + j + 1)
                                / (chi.shape[0] * B.shape[0])
                        ),
                        end="", flush=True
                    )
        print("")
        np.savez(
            str(outfile_base.with_suffix(".npz")),
            B=B,
            chi=chi,
            ph=ph
        )
    else:
        if printflag:
            print("  Found data file; skipping...")
        data = np.load(str(outfile_base.with_suffix(".npz")))
        B = data["B"]
        dB = B[1] - B[0]
        chi = data["chi"]
        logchi = np.log10(chi)
        dchi = logchi[1] - logchi[0]
        ph = data["ph"]

    ph_lim = (ph[0, -1] / abs(ph[0, -1])) * np.pi
    (pd.Plotter()
        .imshow(np.log10((ph_lim - (ph - PHI0)) / (np.pi)),
            extent=[
                B.min() - dB / 2.0, B.max() + dB / 2.0,
                logchi.min() - dchi / 2.0, logchi.max() + dchi / 2.0,
            ],
            origin="lower", aspect="auto", cmap=CMAP
        )
        .colorbar()
        .set_xlabel("Field strength [G]")
        .set_ylabel("$\\log_{10} \\chi$")
        .set_clabel("$\\log_{10}[(\\Delta \\varphi - \\pi) / \\pi]$")
        .grid(False)
        .savefig(outfile_base.with_suffix(ext))
        .close()
    )

    do_single_atom(B.min(), chi.min(), plotflag=True, cropflag=False)
    do_single_atom(B.min(), chi.max(), plotflag=True, cropflag=False)
    do_single_atom(B.max(), chi.min(), plotflag=True, cropflag=False)
    do_single_atom(B.max(), chi.max(), plotflag=True, cropflag=False)

if __name__ == "__main__":
    single_atom_ph()

