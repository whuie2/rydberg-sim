#![allow(dead_code)]
#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(clippy::needless_return)]

use std::{
    fs,
    path::PathBuf,
    f64::consts::PI,
};
use ndarray::{
    self as nd,
    array,
};
use num_complex::Complex64 as C64;
use sim::{
    loop_call,
    write_npz,
    rabi,
    systems::m3h_imaging::{
        self,
        NarrowlineState as GN,
        NarrowlineBasis,
    },
    hilbert::{
        Basis,
    },
};

const TITLE: &str = "-3/2 imaging";

fn do_pumping(
    W: f64,
    detuning: f64,
    aa: f64,
    bb: f64,
    th: f64,
    B: f64,
    outpath: Option<PathBuf>
) -> f64
{
    let t: nd::Array1<f64>
        = nd::Array::range(
            0.0,
            (1.0 / m3h_imaging::GAMMA) * 10.0,
            (2.0 * PI / W).min(1.0 / m3h_imaging::GAMMA) / 1000.0,
        );

    let basis: NarrowlineBasis
        = NarrowlineBasis::from_pairs([
            (GN::g0, 2.0 * PI * m3h_imaging::zm(GN::g0, B)),
            (GN::g1, 2.0 * PI * m3h_imaging::zm(GN::g1, B)),
            (GN::n0, 2.0 * PI * (m3h_imaging::zm(GN::n0, B) - 2.3)),
            (GN::n1, 2.0 * PI * m3h_imaging::zm(GN::n1, B)),
            (GN::n2, 2.0 * PI * m3h_imaging::zm(GN::n2, B)),
            (GN::n3, 2.0 * PI * (m3h_imaging::zm(GN::n3, B) - 2.3)),
        ]);

    // let psi0: nd::Array1<C64> = &(
    //     &basis.get_array(&GN::g0).unwrap()
    //     + &basis.get_array(&GN::g1).unwrap()
    // ) / 2.0_f64.sqrt();
    let psi0: nd::Array1<C64> = basis.get_array(&GN::g0).unwrap();

    let w: f64
        = basis.get_energy(&GN::n0).unwrap()
        - basis.get_energy(&GN::g0).unwrap()
        + (2.0 * PI * detuning);

    let H: nd::Array3<C64>
        = m3h_imaging::H_poincare(
            basis.clone(), w, W, aa, bb, th, t.view(), 0.0)
        .unwrap();

    let rho: nd::Array3<C64> = rabi::liouville_evolve_rk4(&psi0, &H, &t)
        .unwrap();

    // let Y: nd::Array2<f64>
    //     = m3h_imaging::Y_lindblad(basis.clone());

    // let rho: nd::Array3<C64> = rabi::lindblad_evolve_rk4(&psi0, &H, &Y, &t)
    //     .unwrap();

    let score: f64
        = [GN::g1, GN::n1, GN::n2, GN::n3].into_iter()
        .map(|s| {
            let a: nd::Array1<C64> = basis.get_array(&s).unwrap();
            let P: nd::Array1<f64>
                = rho.axis_iter(nd::Axis(2))
                .map(|r| a.dot(&r).dot(&a).norm())
                .collect();
            *P.iter().max_by(|x, y| x.partial_cmp(y).unwrap()).unwrap()
        })
        .sum();

    if let Some(dir) = outpath {
        write_npz!(
            dir.join(format!(
                "W={:.1}MHz_D={:+.3}MHz_aa={:+.3}pi_bb={:+.3}pi_th={:.3}pi_B={:.1}G.npz",
                W / (2.0 * PI),
                detuning,
                aa / PI,
                bb / PI,
                th / PI,
                B,
            ).as_str()),
            arrays: {
                "t" => &t,
                "rho" => &rho,
                "g0" => &basis.get_array(&GN::g0).unwrap(),
                "g1" => &basis.get_array(&GN::g1).unwrap(),
                "n0" => &basis.get_array(&GN::n0).unwrap(),
                "n1" => &basis.get_array(&GN::n1).unwrap(),
                "n2" => &basis.get_array(&GN::n2).unwrap(),
                "n3" => &basis.get_array(&GN::n3).unwrap(),
                "detuning" => &array![detuning],
                "W" => &array![W / (2.0 * PI)],
                "aa" => &array![aa],
                "bb" => &array![bb],
                "th" => &array![th],
                "B" => &array![B],
                "H" => &H,
                // "Y" => &Y,
                "score" => &array![score],
            }
        );
    }

    return score;
}

fn single_test(outpath: PathBuf) {
    let config: m3h_imaging::Config = m3h_imaging::load_config(None);
    let detuning: f64 = 0.0; // MHz
    println!(
        "{}\n{}\n\
        Omega = {} MHz\n\
        Delta = {} MHz\n\
        B = {} G\n\
        aa = {} deg\n\
        bb = {} deg\n\
        th = {} deg\n\
        {1}",
        TITLE,
        "=".repeat(TITLE.len()),
        config.W / (2.0 * PI),
        detuning,
        config.B,
        config.aa * 180.0 / PI,
        config.bb * 180.0 / PI,
        config.th * 180.0 / PI,
    );
    do_pumping(
        config.W,
        detuning,
        config.aa,
        config.bb,
        config.th,
        config.B,
        Some(outpath)
    );
}

fn WB_test(outdir: PathBuf) {
    let config: m3h_imaging::Config = m3h_imaging::load_config(None);
    let detuning: f64 = 0.0; // MHz
    println!(
        "{}\n{}\n\
        Delta = {} MHz\n\
        aa = {} deg\n\
        bb = {} deg\n\
        th = {} deg\n\
        {1}",
        TITLE,
        "=".repeat(TITLE.len()),
        detuning,
        config.aa * 180.0 / PI,
        config.bb * 180.0 / PI,
        config.th * 180.0 / PI,
    );

    let outfile: PathBuf
        = outdir.join(format!(
            "W-B_D={:+.3}MHz_aa={:+.3}pi_bb={:+.3}pi_th={:.3}pi.npz",
            detuning,
            config.aa / PI,
            config.bb / PI,
            config.th / PI,
        ).as_str());

    let W: nd::Array1<f64> = nd::Array::linspace(0.05, 0.5, 50);
    let B: nd::Array1<f64> = nd::Array::linspace(0.0, 15.0, 50);

    let caller = |ijk: Vec<usize>| -> (f64,) {
        (
            do_pumping(
                W[ijk[0]] * (2.0 * PI),
                detuning,
                config.aa,
                config.bb,
                config.th,
                B[ijk[1]],
                None
            ),
        )
    };
    let (inF,): (nd::ArrayD<f64>,) = loop_call!(
        caller => (inF: f64,),
        vars: { W, B }
    );
    write_npz!(
        outfile,
        arrays: {
            "W" => &W,
            "B" => &B,
            "inF" => &inF,
        }
    );
}

fn main() {
    let outpath = PathBuf::from("output/m3h_imaging");
    if !outpath.is_dir() {
        fs::create_dir_all(outpath.as_path())
            .unwrap_or_else(|_| {
                panic!(
                    "Couldn't create directory {:?}",
                    outpath.to_str().unwrap()
                )
            });
        println!(":: mkdir -p {:?}", outpath.to_str().unwrap());
    }

    single_test(outpath);
    // WB_test(outpath);
}

