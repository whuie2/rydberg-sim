# Rydberg sim

---

Library and set of executables for simple time-dependent simulations of various
Yb state manifolds. Originally intended just for simulating Rabi oscillations in
the presence of Rydberg interactions, but can now easily be extended to other
systems as well.

All one needs to get started should be apparent from the example code within the
library files (`lib/*`) and the executables (`src/*`), provided knowledge of
Python and/or Rust.

~~All simulation code can be run completely from and within Python~~, but an
optional Rust library with limited Python bindings is also provided for speed.
To use it, one should first [install Rust][install-rust]. To use the Python
bindings, then install [setuptools-rust][setuptools-rust] and run the setup
script:
```bash
cd <path-to-repo>
python setup.py build_ext -i
```
The resulting compiled library files can be imported from Python like any other
module.

Elements of the full Rust library can be freely used with the usual `use`
statements. To create a new binary for compilation, add the source file to
`src/`, and the path and name information to `Cargo.toml`:
```toml
[[bin]]
path = "path/to/binary/source"
name = "binary name"
```

[install-rust]: https://www.rust-lang.org/tools/install/
[setuptools-rust]: https://pypi.org/project/setuptools-rust/

