from __future__ import annotations
import numpy as np
from sympy.physics.quantum.cg import CG
from sympy.physics.wigner import wigner_3j
from itertools import product
from enum import Enum

"""
Functions to generate Hamiltonians for the 171Yb ground-clock manifold:

                c0  ======
                            c1  ======


                g0  ======
                            g1  ======

Provides additional functionality for time-dependence as well as couplings for
sigma+, sigma-, and pi polarized light with adjustable polarization impurity.
Implicitly applies the rotating wave approximation. All energies are to be
taken in s^-1.
"""

class Basis:
    def __init__(self, states: dict[str, float]):
        self.states = {
            S: np.array(
                [1 if s == S else 0 for s in states.keys()],
                dtype=np.complex128
            )
            for S in states.keys()
        }
        self.energies = states
        self.labels = list(states.keys())
        self.indices = {l: i for i, l in enumerate(self.labels)}

    def __getitem__(self, key: str | int):
        if isinstance(key, int):
            return self.labels[key]
        elif isinstance(key, str):
            return self.states[key]
        else:
            raise KeyError

    def __iter__(self):
        return iter(self.items())

    def items(self):
        return [(l, self.states[l], self.energies[l]) for l in self.labels]

    def keys(self):
        return self.labels

    def values(self):
        return [(self.states[l], self.energies[l]) for l in self.labels]

    def __len__(self):
        return len(self.labels)

    def label(self, index: int):
        return self.labels[index]

    def index(self, label: str):
        return self.indices[label]

    def energy(self, k: str | int):
        if isinstance(k, str):
            return self.energies[k]
        elif isinstance(k, int):
            return self.energies[self.labels[k]]
        else:
            raise KeyError

    def to_multiatom(self, n: int, delim: str=","):
        return Basis({
            delim.join(S): sum(self.energies[s] for s in S)
            for S in product(*[self.labels for k in range(n)])
        })

trans_w3j = lambda F0, mF0, F1, mF1: \
        float(
            abs(
                (-1)**(F1 - 1 + mF0) * np.sqrt(2 * F0 + 1)
                * wigner_3j(F1, 1, F0, mF1, mF0 - mF1, -mF0).doit().evalf()
            )
        )
w3j = {
    ("g1", "c1"): trans_w3j(1/2, +1/2, 1/2, +1/2),
    ("c1", "g1"): trans_w3j(1/2, +1/2, 1/2, +1/2),

    ("g1", "c0"): trans_w3j(1/2, +1/2, 1/2, -1/2),
    ("c0", "g1"): trans_w3j(1/2, +1/2, 1/2, -1/2),

    ("g0", "c0"): trans_w3j(1/2, -1/2, 1/2, -1/2),
    ("c0", "g0"): trans_w3j(1/2, -1/2, 1/2, -1/2),

    ("g0", "c1"): trans_w3j(1/2, -1/2, 1/2, +1/2),
    ("c1", "g0"): trans_w3j(1/2, -1/2, 1/2, +1/2),
}

class PulseType(Enum):
    Sigma_p = 0
    Sigma_m = 1
    Pi = 2
    Sigma_p_var = 3

def trapz_prog(y, dx=1.0):
    return np.append(0, np.cumsum(dx * (y[:-1] + y[1:]) / 2.0))

def _gen_time_dep(t, integrate: list[float | np.ndarray],
        extrude: list[float]) -> list[np.ndarray]:
    assert all(np.shape(X) in {t.shape, ()} for X in integrate)
    dt = abs(t[1] - t[0])
    ones = np.ones(t.shape)
    return [
        trapz_prog(X, dt) if len(np.shape(X)) > 0 else X * t
        for X in integrate
    ] + [X * ones for X in extrude]

def H_pulse(pulse_type: PulseType, basis: Basis, w, W, chi, t, phi=0) \
        -> np.ndarray:
    """
    Thin wrapper around `H_sigma_p`, `H_sigma_m`, or `H_pi`, depending on the
    value of `pulse_type`.
    """
    if pulse_type == PulseType.Sigma_p:
        return H_sigma_p(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Sigma_m:
        return H_sigma_m(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Pi:
        return H_pi(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Sigma_p_var:
        return H_sigma_p_var(basis, w, W, chi, t, phi)
    else:
        raise Exception

def H_sigma_p(basis: Basis, w, W, chi, t, phi=0) -> np.ndarray:
    """
    Construct the Hamiltonian with only off-diagonal elements for a sigma+
    drive. Requires time dependence.

    Parameters
    ----------
    basis : Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Drive frequency relative to the difference between the averages of the
        ground and clock manifolds.
    W : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Rabi frequency of the principal drive. Defined to be the frequency of
        oscillation in populations, not amplitudes.
    chi : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Polarization impurity.
    phi : float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=2, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma+ light.
    """
    [w_, E_g0, E_g1, E_c0, E_c1, W_, chi_] \
            = _gen_time_dep(
                t,
                integrate=[
                    w,
                    basis.energy("g0"),
                    basis.energy("g1"),
                    basis.energy("c0"),
                    basis.energy("c1"),
                ],
                extrude=[W, chi]
            )
    H = np.zeros((len(basis), len(basis), t.shape[0]), dtype=np.complex128)
    for (i, s1), (j, s2) \
            in product(enumerate(basis.labels), enumerate(basis.labels)):
        gc = ("g0", "c1")
        if (tr := (s1, s2)) == ("g0", "c0"):
            w0_ = E_c0 - E_g0
            H[i, j, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(+1j * (w_ - w0_ + phi))
            H[j, i, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(-1j * (w_ - w0_ + phi))
        elif (tr := (s1, s2)) == ("g0", "c1"):
            w0_ = E_c1 - E_g0
            H[i, j, :] = (W_ / 2) * np.sqrt(1 - chi_) \
                    * np.exp(+1j * (w_ - w0_ + phi))
            H[j, i, :] = (W_ / 2) * np.sqrt(1 - chi_) \
                    * np.exp(-1j * (w_ - w0_ + phi))
        elif (tr := (s1, s2)) == ("g1", "c0"):
            w0_ = E_c0 - E_g1
            H[i, j, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(+1j * (w_ - w0_ + phi))
            H[j, i, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(-1j * (w_ - w0_ + phi))
        elif (tr := (s1, s2)) == ("g1", "c1"):
            w0_ = E_c1 - E_g1
            H[i, j, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(+1j * (w_ - w0_ + phi))
            H[j, i, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(-1j * (w_ - w0_ + phi))
    return H

def H_sigma_m(basis: Basis, w, W, chi, t, phi=0) -> np.ndarray:
    """
    Construct the Hamiltonian with only off-diagonal elements for a sigma-
    drive. Requires time dependence.

    Parameters
    ----------
    basis : Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Drive frequency relative to the difference between the averages of the
        ground and clock manifolds.
    W : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Rabi frequency of the principal drive. Defined to be the frequency of
        oscillation in populations, not amplitudes.
    chi : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Polarization impurity.
    phi : float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=2, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma- light.
    """
    [w_, E_g0, E_g1, E_c0, E_c1, W_, chi_] \
            = _gen_time_dep(
                t,
                integrate=[w] + [basis.energy(s) for s in basis.labels],
                extrude=[W, chi]
            )
    H = np.zeros((len(basis), len(basis), t.shape[0]), dtype=np.complex128)
    for (i, s1), (j, s2) \
            in product(enumerate(basis.labels), enumerate(basis.labels)):
        gc = ("g1", "c0")
        if (tr := (s1, s2)) == ("g0", "c0"):
            w0_ = E_c0 - E_g0
            H[i, j, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(+1j * (w_ - w0_ + phi))
            H[j, i, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(-1j * (w_ - w0_ + phi))
        elif (tr := (s1, s2)) == ("g0", "c1"):
            w0_ = E_c1 - E_g0
            H[i, j, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(+1j * (w_ - w0_ + phi))
            H[j, i, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(-1j * (w_ - w0_ + phi))
        elif (tr := (s1, s2)) == ("g1", "c0"):
            w0_ = E_c0 - E_g1
            H[i, j, :] = (W_ / 2) * np.sqrt(1 - chi_) \
                    * np.exp(+1j * (w_ - w0_ + phi))
            H[j, i, :] = (W_ / 2) * np.sqrt(1 - chi_) \
                    * np.exp(-1j * (w_ - w0_ + phi))
        elif (tr := (s1, s2)) == ("g1", "c1"):
            w0_ = E_c1 - E_g1
            H[i, j, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(+1j * (w_ - w0_ + phi))
            H[j, i, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(-1j * (w_ - w0_ + phi))
    return H

def H_pi(basis: Basis, w, W, chi, t, phi=0) -> np.ndarray:
    """
    Construct the Hamiltonian with only off-diagonal elements for a pi-polarized
    drive. Requires time dependence.

    Parameters
    ----------
    basis : Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Drive frequency relative to the difference between the averages of the
        ground and clock manifolds.
    W : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Rabi frequency of the principal drive. Defined to be the frequency of
        oscillation in populations, not amplitudes.
    chi : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Polarization impurity.
    phi : float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=2, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for pi-polarized light.
    """
    [w_, E_g0, E_g1, E_c0, E_c1, W_, chi_] \
            = _gen_time_dep(
                t,
                integrate=[w] + [basis.energy(s) for s in basis.labels],
                extrude=[W, chi]
            )
    H = np.zeros((len(basis), len(basis), t.shape[0]), dtype=np.complex128)
    for (i, s1), (j, s2) \
            in product(enumerate(basis.labels), enumerate(basis.labels)):
        gc0 = ("g0", "c0")
        # gc1 = ("g1", "c1")
        if (tr := (s1, s2)) == ("g0", "c0"):
            w0_ = E_c0 - E_g0
            H[i, j, :] = (W_ / 2) * np.sqrt(1 - chi_) \
                    * np.exp(+1j * (w_ - w0_ + phi))
            H[j, i, :] = (W_ / 2) * np.sqrt(1 - chi_) \
                    * np.exp(-1j * (w_ - w0_ + phi))
        elif (tr := (s1, s2)) == ("g0", "c1"):
            w0_ = E_c1 - E_g0
            H[i, j, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc0] \
                    * np.exp(+1j * (w_ - w0_ + phi))
            H[j, i, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc0] \
                    * np.exp(-1j * (w_ - w0_ + phi))
        elif (tr := (s1, s2)) == ("g1", "c0"):
            w0_ = E_c0 - E_g1
            H[i, j, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc0] \
                    * np.exp(+1j * (w_ - w0_ + phi))
            H[j, i, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc0] \
                    * np.exp(-1j * (w_ - w0_ + phi))
        elif (tr := (s1, s2)) == ("g1", "c1"):
            w0_ = E_c1 - E_g1
            H[i, j, :] = (W_ / 2) * np.sqrt(1 - chi_) * w3j[tr] / wd3[gc0] \
                    * np.exp(+1j * (w_ - w0_ + phi))
            H[j, i, :] = (W_ / 2) * np.sqrt(1 - chi_) * w3j[tr] / wd3[gc0] \
                    * np.exp(-1j * (w_ - w0_ + phi))
    return H

def H_sigma_p_var(basis: Basis, w, W, chi, t, phi=0) -> np.ndarray:
    """
    Construct the Hamiltonian with only off-diagonal elements for a sigma+
    drive, treating the principal g0-c1 transition as zero-energy. Requires time
    dependence.

    Parameters
    ----------
    basis : Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Drive frequency relative to the difference between the averages of the
        ground and clock manifolds.
    W : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Rabi frequency of the principal drive. Defined to be the frequency of
        oscillation in populations, not amplitudes.
    chi : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Polarization impurity.
    phi : float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=2, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma+ light.
    """
    [w_, E_g0, E_g1, E_c0, E_c1, W_, chi_] \
            = _gen_time_dep(
                t,
                integrate=[w] + [basis.energy(s) for s in basis.labels],
                extrude=[W, chi]
            )
    H = np.zeros((len(basis), len(basis), t.shape[0]), dtype=np.complex128)
    for (i, s1), (j, s2) \
            in product(enumerate(basis.labels), enumerate(basis.labels)):
        gc = ("g0", "c1")
        if (tr := (s1, s2)) == ("g0", "c0"):
            w0_ = E_c0 - E_c1
            H[i, j, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(+1j * (w_ - w0_ + phi))
            H[j, i, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(-1j * (w_ - w0_ + phi))
        elif (tr := (s1, s2)) == ("g0", "c1"):
            w0_ = 0
            H[i, j, :] = (W_ / 2) * np.sqrt(1 - chi_) \
                    * np.exp(+1j * (w_ - w0_ + phi))
            H[j, i, :] = (W_ / 2) * np.sqrt(1 - chi_) \
                    * np.exp(-1j * (w_ - w0_ + phi))
        elif (tr := (s1, s2)) == ("g1", "c0"):
            w0_ = E_c0 - E_c1 + E_g0 - E_g1
            H[i, j, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(+1j * (w_ - w0_ + phi))
            H[j, i, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(-1j * (w_ - w0_ + phi))
        elif (tr := (s1, s2)) == ("g1", "c1"):
            w0_ = E_g0 - E_g1
            H[i, j, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(+1j * (w_ - w0_ + phi))
            H[j, i, :] = (W_ / 2) * np.sqrt(chi_ / 2) * w3j[tr] / w3j[gc] \
                    * np.exp(-1j * (w_ - w0_ + phi))
    return H

