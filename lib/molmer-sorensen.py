from __future__ import annotations
import numpy as np

"""
Functions provided to construct time series for detuning and Rabi frequency
ramps following the scheme found in

A. Mitra, M. J. Martin, G. W. Biedermann, A. M. Marino, P. M. Poggi, and I. H.
Deutsch, "Robust Mølmer-Sørensen gate for neutral atoms using rapid adiabatic
Rydberg dressing," Phys. Rev. A 101, 030301(R) (2020).
"""

def Delta(D: (float, float), T: (float, float, float, float), t: np.ndarray) \
        -> np.ndarray:
    """
    Construct the correct detuning time series for given time grid.

    Parameters
    ----------
    D : (float, float)
        Minimum and maximum detunings, respectively.
    T : (float, float, float, float)
        Characteristic times of the ramp sequence.
    t : numpy.ndarray[shape=(k,), dtype=numpy.float64]
        Array of time gridpoints.

    Returns
    -------
    det : numpy.ndarray[shape=(k,), dtype=numpy.float64]
        Detuning time series.
    """
    det = (
        (t >= T[0]) * (t < T[1]) \
            * (D[0] + (D[1] - D[0]) / (T[1] - T[0]) * (t - T[0]))
        + (t >= T[1]) * (t < T[2]) \
            * (D[1])
        + (t >= T[2]) * (t <= T[3]) \
            * (D[1] + (D[0] - D[1]) / (T[3] - T[2]) * (t - T[2]))
    )
    return det

def Omega(W: (float, float), T: (float, float, float, float), tw: float,
    t: np.ndarray) -> np.ndarray:
    """
    Construct the correct Rabi frequency time series for given time grid.

    Parameters
    ----------
    W : (float, float)
        Minimum and maximum Rabi frequencies, respectively.
    T : (float, float, float, float)
        Characteristic times of the ramp sequence.
    tw : float
        Gaussian width of the ramped sections.
    t : numpy.ndarray[shape=(k,), dtype=numpy.float64]
        Array of time gridpoints.

    Returns
    -------
    rab : numpy.ndarray[shape(k,), dtype=numpy.float64]
        Rabi frequency time series.
    """
    rab = (
        (t >= T[0]) * (t < T[1]) \
            * (W[0] + (W[1] - W[0]) * np.exp(-(t - T[1])**2 / (2 * tw**2)))
        (t >= T[1]) * (t < T[2]) \
            * (W[1])
        (t >= T[2]) * (t <= T[3]) \
            * (W[0] + (W[1] - W[0]) * np.exp(-(t - T[2])**2 / (2 * tw**2)))
    )
    return rab

def nu1(W: np.ndarray, D: np.ndarray, dt: float, plus=True) -> float:
    """
    Calculate the total rotation angle for the given pulse time sequence.

    Parameters
    ----------
    W : numpy.ndarray[shape=(k,), dtype=numpy.float64]
        Rabi frequency.
    D : numpy.ndarray[shape=(k,), dtype=numpy.float64]
        Detuning.
    dt : float
        Grid sample time.

    Returns
    -------
    nu1 : float
        Rotation angle.
    """
    nu1 = -np.trapz(
        (D
            + (2 * plus - 1)
                * (np.sqrt(2 * W**2 + D**2) - 2 * np.sqrt(W**2 + D**2))
        ) / 4,
        dx=dt
    )
    return nu1

def nu2(W: np.ndarray, D: np.ndarray, dt: float, plus=True) -> float:
    """
    Calculate the total twist angle for the given pulse time sequence.

    Parameters
    ----------
    W : numpy.ndarray[shape=(k,), dtype=numpy.float64]
        Rabi frequency.
    D : numpy.ndarray[shape=(k,), dtype=numpy.float64]
        Detuning.
    dt : float
        Grid sample time.

    Returns
    -------
    nu2 : float
        Twist angle.
    """
    nu2 = np.trapz(
        (D
            + (2 * plus - 1)
                * (np.sqrt(2 * W**2 + D**2) - 2 * np.sqrt(W**2 + D**2))
        ) / 2,
        dx=dt
    )
    return nu2

