use pyo3::{
    prelude as pyo,
};
use ndarray::{
    self as nd,
};
use numpy::{
    self as np,
    ToPyArray,
    IntoPyArray,
};
use rand::{
    self as rnd,
};
use crate::{
    perlin::*,
};

/// Holds the coefficients for interpolating cubic polynomials used in the
/// generation of Perlin noise in a single octave. The entire assembly is
/// defined over the [0, 1] interval.
#[pyo::pyclass(name = "Octave")]
#[pyo3(text_signature = "(coeffs)")]
#[derive(Clone, Default, Debug)]
pub struct PyOctave {
    pub coeffs: nd::Array2<f64>,
}

impl PyOctave {
    pub fn new(coeffs: nd::Array2<f64>) -> Self { Self { coeffs } }

    pub fn from_gradients(g: nd::ArrayView1<f64>) -> Self {
        // map each consecutive pair of gradients to lie on either end of the
        // [0, 1] interval and find the coefficients for a cubic f such that:
        //      f(0) = f(1) = 0
        //      f'(0) = g0
        //      f'(1) = g1
        // where g0, g1 are the appropriate gradients
        let coeffs: nd::Array2<f64> = nd::Array::from_shape_fn(
            (g.len() - 1, 3),
            |(i, j)| {
                if j == 0 {
                    g[i]
                } else if j == 1 {
                    -2.0 * g[i] - g[i + 1]
                } else {
                    g[i] + g[i + 1]
                }
            }
        );
        return Self { coeffs };
    }
}

impl OctaveMethods for PyOctave {
    fn get_coeffs(&self) -> nd::ArrayView2<f64> { self.coeffs.view() }

    fn get_coeffs_mut(&mut self) -> nd::ArrayViewMut2<f64> {
        return self.coeffs.view_mut()
    }

    fn set_coeffs(&mut self, val: nd::Array2<f64>) { self.coeffs = val; }
}

#[pyo::pymethods]
impl PyOctave {
    #[new]
    fn __new__(coeffs: &np::PyArray2<f64>) -> Self {
        return Self { coeffs: coeffs.to_owned_array() };
    }

    #[staticmethod]
    #[pyo3(name = "new", text_signature = "(coeffs)")]
    fn py_new(coeffs: &np::PyArray2<f64>) -> Self {
        return Self { coeffs: coeffs.to_owned_array() };
    }

    #[staticmethod]
    #[pyo3(name = "from_gradients", text_signature = "(g)")]
    fn py_from_gradients(g: &np::PyArray1<f64>) -> Self {
        return Octave::from_gradients(g.to_owned_array().view()).into();
    }

    #[getter(coeffs)]
    fn py_get_coeffs(&self) -> pyo::Py<np::PyArray2<f64>> {
        return pyo::Python::with_gil(|py|
            self.coeffs.to_pyarray(py).to_owned()
        );
    }

    #[setter(coeffs)]
    fn py_set_coeffs(&mut self, val: &np::PyArray2<f64>) {
        self.coeffs = val.to_owned_array();
    }

    /// Evaluate the octave at `n` evenly spaced gridpoints over its entire
    /// range.
    #[pyo3(name = "eval", text_signature = "($self, n)")]
    fn py_eval(&self, n: usize) -> pyo::Py<np::PyArray1<f64>> {
        return pyo::Python::with_gil(|py|
            OctaveMethods::eval(self, n).into_pyarray(py).to_owned()
        );
    }

    /// Evaluate the first derivative of the octave at `n` evenly spaced
    /// gridpoints over its entire range.
    #[pyo3(name = "eval_deriv", text_signature = "($self, n)")]
    fn py_eval_deriv(&self, n: usize) -> pyo::Py<np::PyArray1<f64>> {
        return pyo::Python::with_gil(|py|
            OctaveMethods::eval_deriv(self, n).into_pyarray(py).to_owned()
        );
    }
}

impl From<Octave> for PyOctave {
    fn from(O: Octave) -> Self { Self { coeffs: O.coeffs } }
}

impl From<PyOctave> for Octave {
    fn from(O: PyOctave) -> Self { Self { coeffs: O.coeffs } }
}

#[pyo::pyclass(name = "Noise")]
#[pyo3(text_signature = "(order, gradients, octaves)")]
#[derive(Clone, Default, Debug)]
pub struct PyNoise {
    pub order: usize,
    pub gradients: Vec<nd::Array1<f64>>,
    pub octaves: Vec<PyOctave>,
}

impl PyNoise {
    pub fn new(order: usize, subdiv: usize, std_g: f64) -> Self {
        let mut rng: rnd::rngs::ThreadRng = rnd::thread_rng();
        let gradients: Vec<nd::Array1<f64>> = (0..order)
            .map(|k|
                gen_gradients_rng(subdiv.pow(k as u32) + 1, std_g, &mut rng)
            )
            .collect();
        let octaves: Vec<PyOctave> = gradients.iter()
            .map(|g| PyOctave::from_gradients(g.view()))
            .collect();
        return Self { order, gradients, octaves };
    }

    pub fn from_gradients(gradients: Vec<nd::Array1<f64>>) -> Self {
        let order: usize = gradients.len();
        let octaves: Vec<PyOctave> = gradients.iter()
            .map(|g| PyOctave::from_gradients(g.view()))
            .collect();
        return Self { order, gradients, octaves };
    }
}

impl NoiseMethods<PyOctave> for PyNoise {
    fn get_order(&self) -> usize { self.order }

    fn set_order(&mut self, val: usize) { self.order = val; }

    fn get_gradients(&self) -> &Vec<nd::Array1<f64>> { &self.gradients }

    fn get_gradients_mut(&mut self) -> &mut Vec<nd::Array1<f64>> {
        return &mut self.gradients;
    }

    fn set_gradients(&mut self, val: Vec<nd::Array1<f64>>) {
        self.gradients = val;
    }

    fn get_octaves(&self) -> &Vec<PyOctave> { &self.octaves }

    fn get_octaves_mut(&mut self) -> &mut Vec<PyOctave> { &mut self.octaves }

    fn set_octaves(&mut self, val: Vec<PyOctave>) { self.octaves = val; }
}

#[pyo::pymethods]
impl PyNoise {
    #[new]
    fn __new__(order: usize, mut gradients: Vec<&np::PyArray1<f64>>,
               octaves: Vec<PyOctave>)
        -> Self
    {
        let G: Vec<nd::Array1<f64>> = gradients.drain(..)
            .map(|g| g.to_owned_array())
            .collect();
        return Self { order, gradients: G, octaves };
    }

    /// Construct by setting the number of octaves `order`, number of
    /// subdivisions `subdiv` in each octave, and standard deviation `std_g` of
    /// the (normal) distribution from which gradients are sampled.
    #[staticmethod]
    #[pyo3(name = "new", text_signature = "(order=1, subdiv=2, std_g=1.0)")]
    #[args(order = "1", subdiv = "2", std_g = "1.0")]
    fn py_new(order: usize, subdiv: usize, std_g: f64) -> Self {
        return Self::new(order, subdiv, std_g);
    }

    /// Construct from a list of arrays holding gradients where each array
    /// corresponds to an octave.
    #[staticmethod]
    #[pyo3(name = "from_gradients", text_signature = "(gradients)")]
    fn py_from_gradients(mut gradients: Vec<&np::PyArray1<f64>>) -> Self {
        let G: Vec<nd::Array1<f64>> = gradients.drain(..)
            .map(|g| g.to_owned_array())
            .collect();
        return Self::from_gradients(G);
    }

    /// Generate an array of Perlin noise at `n` gridpoints spaced evenly over
    /// the [0, 1] interval with successive octaves weighted relatively by
    /// `amp_scale` and the entire sum normalized such that the standard
    /// deviation of the resulting noise about zero is `std`.
    #[pyo3(
        name = "sample",
        text_signature = "($self, n, std=1.0, amp_scale=1.0)"
    )]
    #[args(std = "1.0", amp_scale = "1.0")]
    fn py_sample(&self, n: usize, std: f64, amp_scale: f64)
        -> pyo::Py<np::PyArray1<f64>>
    {
        return pyo::Python::with_gil(|py|
            NoiseMethods::sample(self, n, std, amp_scale)
                .into_pyarray(py)
                .to_owned()
        );
    }
}

impl From<Noise> for PyNoise {
    fn from(mut N: Noise) -> Self {
        let octaves: Vec<PyOctave> = N.octaves.drain(..)
            .map(|O| O.into())
            .collect();
        return Self { order: N.order, gradients: N.gradients, octaves };
    }
}

impl From<PyNoise> for Noise {
    fn from(mut N: PyNoise) -> Self {
        let octaves: Vec<Octave> = N.octaves.drain(..)
            .map(|O| O.into())
            .collect();
        return Self { order: N.order, gradients: N.gradients, octaves };
    }
}

#[pyo::pymodule]
fn perlin(_py: pyo::Python, m: &pyo::PyModule) -> pyo::PyResult<()> {
    m.add_class::<PyOctave>()?;
    m.add_class::<PyNoise>()?;

    /// Generate `N` gradients whose values are normally distributed about zero
    /// with standard deviation `std`.
    #[pyfn(m,
        std = "1.0"
    )]
    #[pyo3(name = "gen_gradients", text_signature = "(N, std=1.0)")]
    fn py_gen_gradients(py: pyo::Python, N: usize, std: f64)
        -> pyo::Py<np::PyArray1<f64>>
    {
        return gen_gradients(N, std).into_pyarray(py).to_owned();
    }

    /// Generate an array of (`n` samples of) Perlin noise of `order` octaves,
    /// where successive octaves are weighted by relative amplitude `amp_scale`
    /// and generated from `subdiv`^k + 1 gradients, where k ranges from 0 to
    /// `order` - 1. The resulting array is normalized such that its standard
    /// deviation about zero is `std`.
    #[pyfn(m,
        std = "1.0", order = "1", amp_scale = "1.0", subdiv = "2",
        std_g = "1.0"
    )]
    #[pyo3(
        name = "noise",
        text_signature = "(n, std=1.0, order=1, amp_scale=1.0, subdiv=2, std_g=1.0)"
    )]
    fn py_noise(n: usize, std: f64, order: usize, amp_scale: f64, subdiv: usize,
                std_g: f64)
        -> pyo::Py<np::PyArray1<f64>>
    {
        return PyNoise::new(order, subdiv, std_g).py_sample(n, std, amp_scale);
    }

    return Ok(());
}
