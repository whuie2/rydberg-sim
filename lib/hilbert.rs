#![allow(unused_parens)]

//! Provides various constructs to describe states and bases, combinations
//! thereof, and ultimately the construction of time-dependent Hamiltonians and
//! non-hermitian elements of the Lindbladian equation.

use std::{
    ops::Mul,
};
use ndarray::{
    self as nd,
    ScalarOperand,
};
use itertools::Itertools;
use num_complex::Complex64 as C64;
use wigner_symbols::Wigner3jm;
use crate::{
    error::*,
};

/// Methods to characterize a single basis state of a single particle with
/// well-defined total spin and projection spin.
pub trait BasisState {
    /// Get the `(total, projection)` spin of the basis state. It is the
    /// programmer's responsibility to ensure that the returned spin values are
    /// sensible; i.e. non-negative total spin, projection not exceeding total
    /// spin magnitude, values only taking integer or half-integer values.
    fn spin(&self) -> (f64, f64);
}

/// Quickly construct a set of [`BasisState`]s as an enum under the identifier
/// `$name`.
///
/// The enum's variants are specified inside curly braces along with their total
/// spin and spin projection, which are used to generate an implementation of
/// `BasisState`. The enum itself also derives `Copy`, `Clone`, `Debug`,
/// `PartialEq`, `Eq` and `Hash`.
///
/// # Example
/// The following macro invocation:
/// ```
/// use sim::hilbert::*;
///
/// mkstate!(
///     SpinManifold : {
///         SpinUp = 0.5,  0.5,
///         SpinDn = 0.5, -0.5,
///     }
/// );
/// ```
/// is equivalent to
/// ```
/// use sim::hilbert::*;
///
/// #[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
/// pub enum SpinManifold {
///     SpinUp,
///     SpinDn,
/// }
///
/// impl BasisState for SpinManifold {
///     fn spin(&self) -> (f64, f64) {
///         return match self {
///             &Self::SpinUp => (0.5,  0.5),
///             &Self::SpinDn => (0.5, -0.5),
///         };
///     }
/// }
/// ```
#[macro_export]
macro_rules! mkstate {
    ( $name:ident : { $( $var:ident = $F:expr, $mF:expr ),+ $(,)? } ) => {
        #[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
        pub enum $name {
            $( $var, )+
        }

        impl BasisState for $name {
            fn spin(&self) -> (f64, f64) {
                return match self {
                    $(
                        &Self::$var => ($F, $mF),
                    )+
                };
            }
        }
    }
}

/// Represents the behavior of a fully instantiated collection of
/// [`BasisState`]s, likely held in an [`indexmap::IndexMap`].
///
/// The collection should be completely ordered so that consistent matrices can
/// be constructed from the basis as a whole. Each basis state is associated
/// with an index giving its position in that order, as well as an energy to be
/// provided at construction. Basis states can also be used to generate
/// appropriate basis column vectors for easier management of the numerics.
pub trait Basis<S>
where S: BasisState,
{
    fn len(&self) -> usize;

    fn is_empty(&self) -> bool { self.len() == 0 }

    /// Return a `Vec` of the contained basis states in a set order.
    fn get_order(&self) -> Vec<&S>;

    /// Find the index of a given state in the order of the basis, if it exists.
    fn get_idx(&self, state: &S) -> HilbertResult<usize>;

    /// Get the state associated with a given index in the order of the basis,
    /// if it exists.
    fn get_state(&self, idx: usize) -> HilbertResult<&S> {
        return self.get_order().get(idx)
            .copied()
            .ok_or(HilbertError::NotInBasis);
    }

    /// Get the energy of a particular state, if it exists.
    fn get_energy(&self, state: &S) -> HilbertResult<f64>;

    /// Get the energy of the state associated with a particular index, if it
    /// exists.
    fn get_energy_i(&self, idx: usize) -> HilbertResult<f64> {
        let state: &S = self.get_order().get(idx)
            .ok_or(HilbertError::NotInBasis)?;
        return self.get_energy(state);
    }

    /// Get the basis state column vector of a particular state, if it exists.
    fn get_array(&self, state: &S) -> HilbertResult<nd::Array1<C64>>;

    /// Get the basis state column vector of the state associated with a
    /// particular index, if it exists.
    fn get_array_i(&self, idx: usize) -> HilbertResult<nd::Array1<C64>> {
        let state: &S = self.get_order().get(idx)
            .ok_or(HilbertError::NotInBasis)?;
        return self.get_array(state);
    }
}

/// Quickly make a [`Basis`] type to hold an instantiated collection of
/// `$state`s, which are assumed to implement [`BasisState`].
///
/// The constructed type is backed by two [`indexmap::IndexMap`]s, one holding
/// the energies of the states and one holding pre-constructed column vector
/// arrays. Implementations of `new` and `from_pairs` constructors are generated
/// as well, with signatures
/// ```ignore
/// impl $name {
///     pub fn new(energies: indexmap::IndexMap<$state, f64>) -> Self;
///
///     pub fn from_pairs<const N: usize>(pairs: [($state, f64); N])-> Self;
/// }
/// ```
/// An implementation of [`Basis`] is also generated.
///
/// Note that instances of this type need not contain all states of type
/// `$state`, in case you want to calculate things using only a subset of the
/// available states in a particular manifold.
#[macro_export]
macro_rules! mkbasis {
    ( $name:ident[$state:ty] ) => {
        #[derive(Clone, Debug)]
        pub struct $name {
            energies: indexmap::IndexMap<$state, f64>,
            arrays:
                indexmap::IndexMap<
                    $state,
                    ndarray::Array1<num_complex::Complex64>
                >,
        }

        impl $name {
            pub fn new(energies: indexmap::IndexMap<$state, f64>) -> Self {
                let mut arrays:
                    indexmap::IndexMap<
                        $state,
                        ndarray::Array1<num_complex::Complex64>
                    >
                    = indexmap::IndexMap::new();
                for (k, S) in energies.keys().enumerate() {
                    arrays.insert(
                        *S,
                        ndarray::Array1::from_shape_fn(
                            energies.len(),
                            |j| {
                                if j == k {
                                    num_complex::Complex64::new(1.0, 0.0)
                                } else {
                                    num_complex::Complex64::new(0.0, 0.0)
                                }
                            }
                        )
                    );
                }
                return $name { energies, arrays };
            }

            pub fn from_pairs<const N: usize>(pairs: [($state, f64); N])
                -> Self
            {
                return Self::new(
                    std::iter::FromIterator::from_iter(pairs.into_iter()));
            }
                    
        }

        impl Basis<$state> for $name {
            fn len(&self) -> usize { self.energies.len() }

            fn get_order(&self) -> Vec<&$state> {
                return self.energies.keys().collect();
            }

            fn get_idx(&self, state: &$state)
                -> $crate::error::HilbertResult<usize>
            {
                return self.energies.get_index_of(state)
                    .ok_or($crate::error::HilbertError::NotInBasis);
            }

            fn get_energy(&self, state: &$state)
                -> $crate::error::HilbertResult<f64>
            {
                return self.energies.get(state)
                    .map(|e| *e)
                    .ok_or($crate::error::HilbertError::NotInBasis);
            }

            fn get_energy_i(&self, idx: usize)
                -> $crate::error::HilbertResult<f64>
            {
                return self.energies.get_index(idx)
                    .map(|(_s, e)| *e)
                    .ok_or($crate::error::HilbertError::NotInBasis);
            }

            fn get_array(&self, state: &$state)
                -> $crate::error::HilbertResult<
                    ndarray::Array1<num_complex::Complex64>
                >
            {
                return self.arrays.get(state)
                    .map(|a| a.clone())
                    .ok_or($crate::error::HilbertError::NotInBasis);
            }

            fn get_array_i(&self, idx: usize)
                -> $crate::error::HilbertResult<
                    ndarray::Array1<num_complex::Complex64>
                >
            {
                return self.arrays.get_index(idx)
                    .map(|(_s, a)| a.clone())
                    .ok_or($crate::error::HilbertError::NotInBasis);
            }
        }
    }
}

/// Marker trait to note that a given type is a "product state"; i.e. formed as
/// a Kronecker product of some number of subsystems.
pub trait ProdState { }

impl<S: BasisState, const A: usize> ProdState for [S; A] { }
impl<S: BasisState> ProdState for Vec<S> { }

/// Describes the behavior of a collection of [`ProdState`]s.
///
/// The expectations here are essentially the same as for a [`Basis`], with the
/// exception that the states are [`ProdState`]s instead.
pub trait ProdBasis<P>
where P: ProdState
{
    fn len(&self) -> usize;

    fn is_empty(&self) -> bool { self.len() == 0 }

    /// Return a `Vec` of the contained basis states in a set order.
    fn get_order(&self) -> Vec<&P>;

    /// Get the energy of a particular state, if it exists.
    fn get_energy(&self, state: &P) -> HilbertResult<f64>;

    /// Get the energy of the state associated with a particular index, if it
    /// exists.
    fn get_energy_i(&self, idx: usize) -> HilbertResult<f64> {
        return self.get_energy(self.get_order()[idx]);
    }

    /// Find the index of a given state in the order of the basis, if it exists.
    fn get_idx(&self, state: &P) -> HilbertResult<usize>;

    /// Get the state associated with a given index in the order of the basis,
    /// if it exists.
    fn get_state(&self, idx: usize) -> HilbertResult<&P> {
        return self.get_order().get(idx)
            .copied()
            .ok_or(HilbertError::NotInBasis);
    }

    /// Get the basis state column vector of a particular state, if it exists.
    fn get_array(&self, state: &P) -> HilbertResult<nd::Array1<C64>>;

    /// Get the basis state column vector of the state associated with a
    /// particular index, if it exists.
    fn get_array_i(&self, idx: usize) -> HilbertResult<nd::Array1<C64>> {
        return self.get_array(self.get_order()[idx]);
    }
}

/// Quickly make a [`ProdBasis`] type to hold an instantiated collection of
/// `$state`s, which are assumed to implement [`ProdState`].
///
/// The expansion of this macro is essentially the same as [`mkbasis`], with the
/// exception that everything is for [`ProdState`]s and the `from_pairs`
/// constructor is omitted.
/// ```ignore
/// impl $name {
///     // `N` is the number of subsystems composing the product basis
///     pub fn new(N: usize, energies: indexmap::IndexMap<$state, f64>) -> Self;
/// }
/// ```
#[macro_export]
macro_rules! mkprodbasis {
    ( $name:ident[$prodstate:ty] ) => {
        #[derive(Clone, Debug)]
        pub struct $name {
            pub N: usize,
            energies: indexmap::IndexMap<$prodstate, f64>,
            arrays:
                indexmap::IndexMap<
                    $prodstate,
                    ndarray::Array1<num_complex::Complex64>
                >,
        }

        impl $name {
            pub fn new(N: usize, energies: indexmap::IndexMap<$prodstate, f64>)
                -> $crate::error::HilbertResult<Self>
            {
                if !energies.keys().all(|PS| PS.len() == N) {
                    return Err($crate::error::HilbertError::NonEqNProdState);
                }
                let mut arrays:
                    indexmap::IndexMap<
                        $prodstate,
                        ndarray::Array1<num_complex::Complex64>
                    >
                    = indexmap::IndexMap::new();
                for (k, S) in energies.keys().enumerate() {
                    arrays.insert(
                        S.clone(),
                        ndarray::Array1::from_shape_fn(
                            energies.len(),
                            |j| {
                                if j == k {
                                    num_complex::Complex64::new(1.0, 0.0)
                                } else {
                                    num_complex::Complex64::new(0.0, 0.0)
                                }
                            }
                        )
                    );
                }
                return Ok($name { N, energies, arrays });
            }
        }

        impl ProdBasis<$prodstate> for $name {
            fn len(&self) -> usize { self.energies.len() }

            fn get_order(&self) -> Vec<&$prodstate> {
                return self.energies.keys().collect();
            }

            fn get_idx(&self, state: &$prodstate)
                -> $crate::error::HilbertResult<usize>
            {
                return self.energies.get_index_of(state)
                    .ok_or($crate::error::HilbertError::NotInBasis);
            }

            fn get_energy(&self, state: &$prodstate)
                -> $crate::error::HilbertResult<f64>
            {
                return self.energies.get(state)
                    .map(|e| *e)
                    .ok_or($crate::error::HilbertError::NotInBasis);
            }

            fn get_energy_i(&self, idx: usize)
                -> $crate::error::HilbertResult<f64>
            {
                return self.energies.get_index(idx)
                    .map(|(_s, e)| *e)
                    .ok_or($crate::error::HilbertError::NotInBasis);
            }

            fn get_array(&self, state: &$prodstate)
                -> $crate::error::HilbertResult<
                    ndarray::Array1<num_complex::Complex64>
                >
            {
                return self.arrays.get(state)
                    .map(|a| a.clone())
                    .ok_or($crate::error::HilbertError::NotInBasis);
            }

            fn get_array_i(&self, idx: usize)
                -> $crate::error::HilbertResult<
                    ndarray::Array1<num_complex::Complex64>
                >
            {
                return self.arrays.get_index(idx)
                    .map(|(_s, a)| a.clone())
                    .ok_or($crate::error::HilbertError::NotInBasis);
            }
        }
    }
}

/// Perform a Kronecker product between 2D arrays `a` and `b`.
pub fn kron<A, B, C>(a: &nd::Array2<A>, b: &nd::Array2<B>) -> nd::Array2<C>
where
    A: Clone + Mul<B, Output = C>,
    B: Clone + ScalarOperand,
{
    let shape_a = a.shape();
    let shape_b = b.shape();
    let shape_c = (shape_a[0] * shape_b[0], shape_a[1] * shape_b[1]);
    return nd::Array::from_iter(
        a.axis_iter(nd::Axis(0)).cartesian_product(b.axis_iter(nd::Axis(0)))
            .flat_map(|(ai, bi)| {
                ai.iter().cartesian_product(bi.iter())
                    .map(|(aij, bij)| aij.clone() * bij.clone())
                    .collect::<Vec<C>>()
                    .into_iter()
            })
    )
        .into_shape(shape_c)
        .expect("error reshaping kron product")
        .into_dimensionality::<nd::Ix2>()
        .expect("error casting dimensionality of kron product");
}

/// Compute the "progressive" integral of the function `y` using the trapeoidal
/// rule with uniform step size `dx`. The progressive integral is defined as
/// ```text
/// I(x) = \int_a^x y(x') dx'
/// ```
pub fn trapz_prog(y: nd::ArrayView1<f64>, dx: f64) -> nd::Array1<f64> {
    let mut acc: f64 = 0.0;
    let mut I: nd::Array1<f64> = nd::Array1::zeros(y.raw_dim());
    for k in 1..y.len() {
        acc += dx * (y[k] + y[k - 1]) / 2.0;
        I[k] = acc;
    }
    return I;
}

/// Generate the time dependence of several quantities using `t` as the time
/// coordinate.
///
/// Quantites are broken up by argument into three categories:
///
/// - Elements `c` of `integrate_c` are constants, for which time dependence is
/// calculated as `c * t`.
/// - Elements `f[t]` of `integrate_t` are (sampled) functions of time (e.g.
/// phase frequency), for which time dependence is calculated as the
/// ["progressive" integral][trapz_prog] of `f[t]`.
/// - Elements of `extrude` are quantities for which total time dependence is
/// constant, meaning that they just need to be "extruded" into an array of
/// equal length to that of `t`.
///
/// Time dependence arrays are all collected into a single returned `Vec` in the
/// above order.
pub fn gen_time_dep(t: nd::ArrayView1<f64>, integrate_c: Vec<f64>,
                integrate_t: Vec<nd::ArrayView1<f64>>, extrude: Vec<f64>)
    -> HilbertResult<Vec<nd::Array1<f64>>>
{
    if !integrate_t.iter().map(|x_t| x_t.len()).all(|N| N == t.len()) {
        return Err(HilbertError::NonEqTimeDep);
    }
    let dt: f64 = (t[1] - t[0]).abs();
    let ones: nd::Array1<f64> = nd::Array::ones(t.len());
    let mut res: Vec<nd::Array1<f64>> = Vec::new();
    for c in integrate_c.iter() {
        res.push(t.mapv(|ti| *c * ti));
    }
    for x_t in integrate_t.iter() {
        res.push(trapz_prog(*x_t, dt));
    }
    for x in extrude.iter() {
        res.push(*x * &ones);
    }
    return Ok(res);
}

/// Calculate the appropriate normalized Wigner 3j symbol (/Clebsch-Gordan
/// coefficienc) for an electric dipole transition from ground [spin
/// state][BasisState::spin] `FF0` to excited state `FF1`.
///
/// The 3j symbol is
/// ```text
/// ( F1       1       F0  )
/// ( mF1  mF0 - mF1  -mF0 )
/// ```
/// which is then multiplied by `sqrt(2 F0 + 1)`. The absolute value of this is
/// returned.
pub fn trans_w3j(FF0: (f64, f64), FF1: (f64, f64)) -> f64
{
    let (F0, mF0): (f64, f64) = FF0;
    let (F1, mF1): (f64, f64) = FF1;
    let wig = f64::from(
        Wigner3jm { // elements are represented as the number of halves
            tj1: (2.0 * F1) as i32,
            tm1: (2.0 * mF1) as i32,
            tj2: 2_i32,
            tm2: (2.0 * (mF0 - mF1)) as i32,
            tj3: (2.0 * F0) as i32,
            tm3: (-2.0 * mF0) as i32,
        }.value()
    );
    return (2.0 * F0 + 1.0).sqrt() * wig.abs();
}

/// Calculate the phase angle of a complex number.
pub fn phase_angle(z: C64) -> f64 {
    return (z / z.norm()).ln().im;
}

/// Calculate the polarization impurity weighting factor for the transition from
/// `s1` to `s2` for impurity parameter `chi` and nominal drive polarization
/// `q`.
pub fn pol_impurity<S>(s1: &S, s2: &S, chi: f64, q: f64) -> f64
where S: BasisState
{
    return if s2.spin().1 - s1.spin().1 == q {
        (1.0 - chi).sqrt()
    } else {
        (chi / 2.0).sqrt()
    }
}

/// Calculate the polarization weighting factor for the transition from `s1` to
/// `s2` where `alpha` is the mixing angle between horizontal and vertical
/// components, `beta` is their relative phase angle (both relative to
/// horizontal), and `theta` is the angle of the incoming *k*-vector relative to
/// the magnetic field.
///
/// Note: The complex phases associated with these weighting factors is not
/// exactly correct. See the Kaufman erratum ([PRX **12** 021027
/// (2022)][erratum])
///
/// [erratum]: https://journals.aps.org/prx/abstract/10.1103/PhysRevX.13.029902
pub fn pol_poincare<S>(s1: &S, s2: &S, alpha: f64, beta: f64, theta: f64)
    -> num_complex::Complex64
where S: BasisState
{
    return match (s2.spin().1 - s1.spin().1).round() as i8 {
        -1_i8 => (
            alpha.cos()
            + num_complex::Complex64::i()
                * (num_complex::Complex64::i() * beta).exp()
                * alpha.sin() * theta.cos()
        ) / 2.0_f64.sqrt(),
         0_i8 => (
            (num_complex::Complex64::i() * beta).exp()
                * alpha.sin() * theta.sin()
        ),
         1_i8 => (
            alpha.cos()
            - num_complex::Complex64::i()
                * (num_complex::Complex64::i() * beta).exp()
                * alpha.sin() * theta.cos()
        ) / 2.0_f64.sqrt(),
        _ => num_complex::Complex64::new(0.0, 0.0),
    }
}

/// Compute the Clebsch-Gordan coefficient between ground and exicted states
/// `s1` and `s2`, normalized to `N`.
pub fn cg_normalized<S>(s1: &S, s2: &S, N: f64) -> f64
where S: BasisState
{
    return trans_w3j(s1.spin(), s2.spin()) / N;
}

/// Generate a function that creates the time-dependent Hamiltonian matrix for a
/// drive applied to a manifold of states with a single polarization-principal
/// transition and some number of other polarization-parasitic transitions.
///
/// The generated function signature is
/// ```ignore
/// pub fn $name(
///     basis: $basis_ty,
///     w: f64,
///     W: f64,
///     chi: f64,
///     t: ndarray::ArrayView1<f64>,
///     phi: f64,
/// ) -> HilbertResult<ndarray::Array3<num_complex::Complex64>>
/// ```
/// where `basis` is the [`Basis`] type containing states and energies, `w` is
/// the frequency of the drive, `W` is the strength (Rabi frequency) of the
/// drive, `chi` is the polarization impurity parameter, `t` contains time
/// coordinates, and `phi` is the overall phase of the drive. All frequencies
/// (including energies) should be in angular units. The returned Hamiltonian
/// has shape `(N, N, T)`, where `N` is the size of the basis, and `T` is the
/// number of time coordinates.
#[macro_export]
macro_rules! pulse_fn {
    (
        $name:ident : {
            basis: $basis_ty:ty : $state_ty:ty,
            principal: $prin_s1:path => $prin_s2:path,
            parasitic: { $( $para_s1:path => $para_s2:path ),* $(,)? } $(,)?
        }
    ) => {
        #[allow(clippy::too_many_arguments)]
        pub fn $name(basis: $basis_ty, w: f64, W: f64, chi: f64,
                t: ndarray::ArrayView1<f64>, phi: f64)
            -> $crate::error::HilbertResult<
                ndarray::Array3<num_complex::Complex64>
            >
        {
            let n: usize = basis.len();
            let energies: Vec<f64>
                = (0..n).map(|i| basis.get_energy_i(i).unwrap()).collect();

            let mut H: ndarray::Array3<num_complex::Complex64>
                = ndarray::Array::zeros((n, n, t.len()));
            let transitions
                = itertools::Itertools::cartesian_product(
                    (0..n).map(|i| (i, *basis.get_state(i).unwrap())),
                    (0..n).map(|j| (j, *basis.get_state(j).unwrap())),
                );
            let F0: f64 = $prin_s1.spin().0;
            let F1: f64 = $prin_s2.spin().0;
            let N_w3j: f64 = $crate::hilbert::trans_w3j((F0, F0), (F1, F1));
            let q: f64 = $prin_s2.spin().1 - $prin_s1.spin().1;

            let drive
                = |i: usize, s1: &$state_ty, j: usize, s2: &$state_ty|
                -> (
                    ndarray::Array1<num_complex::Complex64>,
                    ndarray::Array1<num_complex::Complex64>
                )
                {
                    let pol: f64 = pol_impurity(s1, s2, chi, q);
                    let cg: f64 = cg_normalized(s1, s2, N_w3j);
                    let w0: f64 = energies[j] - energies[i];
                    let phase: ndarray::Array1<f64> = (w - w0) * &t + phi;
                    (
                        phase.mapv(
                            |ph| (W / 2.0) * pol * cg
                                * ( num_complex::Complex64::i() * ph).exp()
                        ),
                        phase.mapv(
                            |ph| (W / 2.0) * pol * cg
                                * (-num_complex::Complex64::i() * ph).exp()
                        ),
                    )
                };

            let mut add_drive
                = |
                    i: usize, j: usize,
                    drive: (
                        ndarray::Array1<num_complex::Complex64>,
                        ndarray::Array1<num_complex::Complex64>
                    )
                |
                {
                    (drive.0).move_into(H.slice_mut(ndarray::s![i, j, ..]));
                    (drive.1).move_into(H.slice_mut(ndarray::s![j, i, ..]));
                };

            for ((i, s1), (j, s2)) in transitions {
                match (s1, s2) {
                    ($prin_s1, $prin_s2)
                        => add_drive(j, i, drive(i, &s1, j, &s2)),
                    $(
                        ($para_s1, $para_s2)
                            => add_drive(j, i, drive(i, &s1, j, &s2)),
                    )*
                    _ => { },
                };
            }
            return Ok(H);
        }
    }
}

/// Generate a function that creates the time-dependent Hamiltonian matrix for a
/// time-dependent drive applied to a manifold of states with a single
/// polarization-principal transition and some number of other
/// polarization-parasitic transitions.
///
/// The generated function signature is
/// ```ignore
/// pub fn $name(
///     basis: $basis_ty,
///     w: ndarray::ArrayView1<f64>,
///     W: ndarray::ArrayView1<f64>,
///     chi: ndarray::ArrayView1<f64>,
///     t: ndarray::ArrayView1<f64>,
///     phi: f64,
/// ) -> HilbertResult<ndarray::Array3<num_complex::Complex64>>
/// ```
/// where `basis` is the [`Basis`] type containing states and energies, `w` is
/// the frequency of the drive, `W` is the strength (Rabi frequency) of the
/// drive, `chi` is the polarization impurity parameter, `t` contains time
/// coordinates, and `phi` is the overall phase of the drive. All frequencies
/// (including energies) should be in angular units. The returned Hamiltonian
/// has shape `(N, N, T)`, where `N` is the size of the basis, and `T` is the
/// number of time coordinates.
#[macro_export]
macro_rules! pulse_fn_t {
    (
        $name:ident : {
            basis: $basis_ty:ty : $state_ty:ty,
            principal: $prin_s1:path => $prin_s2:path,
            parasitic: { $( $para_s1:path => $para_s2:path ),* $(,)? } $(,)?
        }
    ) => {
        #[allow(clippy::too_many_arguments)]
        pub fn $name(basis: $basis_ty, w: ndarray::ArrayView1<f64>,
                 W: ndarray::ArrayView1<f64>, chi: ndarray::ArrayView1<f64>,
                 t: ndarray::ArrayView1<f64>, phi: f64)
            -> $crate::error::HilbertResult<
                ndarray::Array3<num_complex::Complex64>
            >
        {
            if ![w.view(), W.view(), chi.view()].iter()
                .all(|x| x.len() == t.len())
            {
                return Err($crate::error::HilbertError::NonEqTimeDep);
            }
            let n: usize = basis.len();
            let energies: Vec<f64>
                = (0..n).map(|i| basis.get_energy_i(i).unwrap()).collect();
            let time_dep: Vec<ndarray::Array1<f64>>
                = gen_time_dep(t, energies, vec![w], vec![])?;
            // time_dep = [E1, ..., En,  w]
            //             ^0       ^n-1 ^n

            let mut H: ndarray::Array3<num_complex::Complex64>
                = ndarray::Array::zeros((n, n, t.len()));
            let transitions
                = itertools::Itertools::cartesian_product(
                    (0..n).map(|i| (i, *basis.get_state(i).unwrap())),
                    (0..n).map(|j| (j, *basis.get_state(j).unwrap())),
                );
            let F0: f64 = $prin_s1.spin().0;
            let F1: f64 = $prin_s2.spin().0;
            let N_w3j: f64 = $crate::hilbert::trans_w3j((F0, F0), (F1, F1));
            let q: f64 = $prin_s2.spin().1 - $prin_s1.spin().1;

            let drive
                = |i: usize, s1: &$state_ty, j: usize, s2: &$state_ty|
                -> (
                    ndarray::Array1<num_complex::Complex64>,
                    ndarray::Array1<num_complex::Complex64>
                )
                {
                    let pol: ndarray::Array1<f64>
                        = chi.mapv(|x| pol_impurity(s1, s2, x, q));
                    let w0: ndarray::Array1<f64> = &time_dep[j] - &time_dep[i];
                    let amp: ndarray::Array1<f64>
                        = &W / 2.0 * cg_normalized(s1, s2, N_w3j) * &pol;
                    let phase: ndarray::Array1<f64> = &time_dep[n] - &w0 + phi;
                    (
                        &phase.mapv(
                            |ph| ( num_complex::Complex64::i() * ph).exp()
                        ) * &amp,
                        &phase.mapv(
                            |ph| (-num_complex::Complex64::i() * ph).exp()
                        ) * &amp,
                    )
                };

            let mut add_drive
                = |
                    i: usize, j: usize,
                    drive: (
                        ndarray::Array1<num_complex::Complex64>,
                        ndarray::Array1<num_complex::Complex64>
                    )
                |
                {
                    (drive.0).move_into(H.slice_mut(ndarray::s![i, j, ..]));
                    (drive.1).move_into(H.slice_mut(ndarray::s![j, i, ..]));
                };

            for ((i, s1), (j, s2)) in transitions {
                match (s1, s2) {
                    ($prin_s1, $prin_s2)
                        => add_drive(j, i, drive(i, &s1, j, &s2)),
                    $(
                        ($para_s1, $para_s2)
                            => add_drive(j, i, drive(i, &s1, j, &s2)),
                    )*
                    _ => { },
                };
            }
            return Ok(H);
        }
    }
}

/// Generate a function that creates the time-dependent Hamiltonian matrix for a
/// drive with well-defined polarization applied to a manifold of states.
///
/// A "stretched" transition (that between the states of maximal spin projection
/// quantum numbers in the ground and excited manifolds) must be specified for
/// Clebsch-Gordan coefficient normalization.
///
/// The generated function signature is
/// ```ignore
/// pub fn $name(
///     basis: $basis_ty,
///     w: f64,
///     W: f64,
///     alpha: f64,
///     beta: f64,
///     theta: f64,
///     t: ndarray::ArrayView1<f64>,
///     phi: f64,
/// ) -> HilbertResult<ndarray::Array3<num_complex::Complex64>>
/// ```
/// where `basis` is the [`Basis`] type containing states and energies, `w` is
/// the frequency of the drive, `W` is the strength (Rabi frequency) of the
/// drive, `alpha` is the mixing angle between horizontal and vertical
/// polarization components, `beta` is the phase angle between them (both are
/// relative to the horizontal component), `theta` is the angle of the incoming
/// *k*-vector relative to the magnetic field, `t` contains time coordinates,
/// and `phi` is the overall phase of the drive. All frequencies (including
/// energies) should be in angular units. The returned Hamiltonian has shape
/// `(N, N, T)`, where `N` is the size of the basis, and `T` is the number of
/// time coordinates.
///
/// Note: The complex phases associated with the polarization weighting factors
/// is not exactly correct. See the Kaufman erratum ([PRX **12** 021027
/// (2022)][erratum])
///
/// [erratum]: https://journals.aps.org/prx/abstract/10.1103/PhysRevX.13.029902
#[macro_export]
macro_rules! pulse_fn_poincare {
    (
        $name:ident : {
            basis: $basis_ty:ty : $state_ty:ty,
            transitions: { $( $tran_s1:path => $tran_s2:path ),+ $(,)? },
            stretch: $stretch_s1:path => $stretch_s2:path $(,)?
        }
    ) => {
        #[allow(clippy::too_many_arguments)]
        pub fn $name(
            basis: $basis_ty,
            w: f64,
            W: f64,
            alpha: f64,
            beta: f64,
            theta: f64,
            t: ndarray::ArrayView1<f64>,
            phi: f64
        ) -> $crate::error::HilbertResult<
                ndarray::Array3<num_complex::Complex64>
            >
        {
            let n: usize = basis.len();
            let energies: Vec<f64>
                = (0..n).map(|i| basis.get_energy_i(i).unwrap()).collect();

            let mut H: ndarray::Array3<num_complex::Complex64>
                = ndarray::Array::zeros((n, n, t.len()));
            let transitions
                = itertools::Itertools::cartesian_product(
                    (0..n).map(|i| (i, *basis.get_state(i).unwrap())),
                    (0..n).map(|j| (j, *basis.get_state(j).unwrap())),
                );
            let N_w3j: f64
                = $crate::hilbert::trans_w3j(
                    $stretch_s1.spin(), $stretch_s2.spin());

            let drive
                = |i: usize, s1: &$state_ty, j: usize, s2: &$state_ty|
                -> ndarray::Array1<num_complex::Complex64>
                {
                    let pol: num_complex::Complex64
                        = pol_poincare(s1, s2, alpha, beta, theta);
                    let cg: f64 = cg_normalized(s1, s2, N_w3j);
                    let w0: f64 = energies[j] - energies[i];
                    let phase: ndarray::Array1<f64> = (w - w0) * &t + phi;
                    phase.mapv(
                        |ph| (W / 2.0) * pol * cg
                            * (num_complex::Complex64::i() * ph).exp()
                    )
                };

            let mut add_drive
                = |
                    i: usize, j: usize,
                    drive: ndarray::Array1<num_complex::Complex64>
                |
                {
                    (drive.clone())
                        .move_into(H.slice_mut(ndarray::s![i, j, ..]));
                    (drive.mapv(|X| X.conj()))
                        .move_into(H.slice_mut(ndarray::s![j, i, ..]));
                };

            for ((i, s1), (j, s2)) in transitions {
                match (s1, s2) {
                    $(
                        ($tran_s1, $tran_s2)
                            => add_drive(j, i, drive(i, &s1, j, &s2)),
                    )+
                    _ => { },
                };
            }
            return Ok(H);
        }
    }
}

/// Generate a function that creates the time-dependent Hamiltonian matrix for a
/// time-dependent drive with well-defined, time-dependent polarization applied
/// to a manifold of states.
///
/// A "stretched" transition (that between the states of maximal spin projection
/// quantum numbers in the ground and excited manifolds) must be specified for
/// Clebsch-Gordan coefficient normalization.
///
/// The generated function signature is
/// ```ignore
/// pub fn $name(
///     basis: $basis_ty,
///     w: ndarray::ArrayView1<f64>,
///     W: ndarray::ArrayView1<f64>,
///     alpha: ndarray::ArrayView1<f64>,
///     beta: ndarray::ArrayView1<f64>,
///     theta: ndarray::ArrayView1<f64>,
///     t: ndarray::ArrayView1<f64>,
///     phi: f64,
/// ) -> HilbertResult<ndarray::Array3<num_complex::Complex64>>
/// ```
/// where `basis` is the [`Basis`] type containing states and energies, `w` is
/// the frequency of the drive, `W` is the strength (Rabi frequency) of the
/// drive, `alpha` is the mixing angle between horizontal and vertical
/// polarization components, `beta` is the phase angle between them (both are
/// relative to the horizontal component), `theta` is the angle of the incoming
/// *k*-vector relative to the magnetic field, `t` contains time coordinates,
/// and `phi` is the overall phase of the drive. All frequencies (including
/// energies) should be in angular units. The returned Hamiltonian has shape
/// `(N, N, T)`, where `N` is the size of the basis, and `T` is the number of
/// time coordinates.
///
/// Note: The complex phases associated with the polarization weighting factors
/// is not exactly correct. See the Kaufman erratum ([PRX **12** 021027
/// (2022)][erratum])
///
/// [erratum]: https://journals.aps.org/prx/abstract/10.1103/PhysRevX.13.029902
#[macro_export]
macro_rules! pulse_fn_poincare_t {
    (
        $name:ident : {
            basis: $basis_ty:ty : $state_ty:ty,
            transitions: { $( $tran_s1:path => $tran_s2:path ),+ $(,)? },
            stretch: $stretch_s1:path => $stretch_s2:path $(,)?
        }
    ) => {
        #[allow(clippy::too_many_arguments)]
        pub fn $name(
            basis: $basis_ty,
            w: ndarray::ArrayView1<f64>,
            W: ndarray::ArrayView1<f64>,
            alpha: ndarray::ArrayView1<f64>,
            beta: ndarray::ArrayView1<f64>,
            theta: ndarray::ArrayView1<f64>,
            t: ndarray::ArrayView1<f64>,
            phi: f64
        ) -> $crate::error::HilbertResult<
            ndarray::Array3<num_complex::Complex64>
        >
        {
            if ![
                    w.view(), W.view(), alpha.view(), beta.view(), theta.view()
                ].iter()
                .all(|x| x.len() == t.len())
            {
                return Err($crate::error::HilbertError::NonEqTimeDep);
            }
            let n: usize = basis.len();
            let energies: Vec<f64>
                = (0..n).map(|i| basis.get_energy_i(i).unwrap()).collect();
            let time_dep: Vec<ndarray::Array1<f64>>
                = gen_time_dep(t, energies, vec![w], vec![])?;
            // time_dep = [E1, ..., En,  w]
            //             ^0       ^n-1 ^n

            let mut H: ndarray::Array3<num_complex::Complex64>
                = ndarray::Array::zeros((n, n, t.len()));
            let transitions
                = itertools::Itertools::cartesian_product(
                    (0..n).map(|i| (i, *basis.get_state(i).unwrap())),
                    (0..n).map(|j| (j, *basis.get_state(j).unwrap())),
                );
            let N_w3j: f64
                = $crate::hilbert::trans_w3j(
                    $stretch_s1.spin(), $stretch_s2.spin());

            let drive
                = |i: usize, s1: &$state_ty, j: usize, s2: &$state_ty|
                -> ndarray::Array1<num_complex::Complex64>
                {
                    let pol: ndarray::Array1<num_complex::Complex64>
                        = alpha.iter().zip(beta.iter()).zip(theta.iter())
                        .map(|((aa, bb), th)|
                            pol_poincare(s1, s2, *aa, *bb, *th)
                        )
                        .collect();
                    let cg: f64 = cg_normalized(s1, s2, N_w3j);
                    let w0: ndarray::Array1<f64> = &time_dep[j] - &time_dep[i];
                    let amp: ndarray::Array1<num_complex::Complex64>
                        = &pol * &W / 2.0 * cg;
                    let phase: ndarray::Array1<f64> = &time_dep[n] - &w0 + phi;
                    &amp * &phase.mapv(
                        |ph| (num_complex::Complex64::i() * ph).exp()
                    )
                };

            let mut add_drive
                = |
                    i: usize, j: usize,
                    drive: ndarray::Array1<num_complex::Complex64>
                |
                {
                    (drive.clone())
                        .move_into(H.slice_mut(ndarray::s![i, j, ..]));
                    (drive.mapv(|X| X.conj()))
                        .move_into(H.slice_mut(ndarray::s![j, i, ..]));
                };

            for ((i, s1), (j, s2)) in transitions {
                match (s1, s2) {
                    $(
                        ($tran_s1, $tran_s2)
                            => add_drive(j, i, drive(i, &s1, j, &s2)),
                    )+
                    _ => { },
                };
            }
            return Ok(H);
        }
    }
}

/// Generate a function that creates the time-dependent Hamiltonian matrix for a
/// time-dependent drive with explicitly defined phase and well-defined
/// polarization applied to a manifold of states.
///
/// A "stretched" transition (that between the states of maximal spin projection
/// quantum numbers in the ground and excited manifolds) must be specified for
/// Clebsch-Gordan coefficient normalization.
///
/// The generated function signature is
/// ```ignore
/// pub fn $name(
///     basis: $basis_ty,
///     phi: ndarray::ArrayView1<f64>,
///     W: ndarray::ArrayView1<f64>,
///     alpha: f64,
///     beta: f64,
///     theta: f64,
///     t: ndarray::ArrayView1<f64>,
/// ) -> HilbertResult<ndarray::Array3<num_complex::Complex64>>
/// ```
/// where `basis` is the [`Basis`] type containing states and energies, `phi` is
/// the explicit phase of the drive (it should be monotonically increasing for
/// constant drive frequency), `W` is the strength (Rabi frequency) of the
/// drive, `alpha` is the mixing angle between horizontal and vertical
/// polarization components, `beta` is the phase angle between them (both are
/// relative to the horizontal component), `theta` is the angle of the incoming
/// *k*-vector relative to the magnetic field, and `t` contains time
/// coordinates. All frequencies (including energies) should be in angular
/// units. The returned Hamiltonian has shape `(N, N, T)`, where `N` is the size
/// of the basis, and `T` is the number of time coordinates.
///
/// Note: The complex phases associated with the polarization weighting factors
/// is not exactly correct. See the Kaufman erratum ([PRX **12** 021027
/// (2022)][erratum])
///
/// [erratum]: https://journals.aps.org/prx/abstract/10.1103/PhysRevX.13.029902
#[macro_export]
macro_rules! pulse_fn_poincare_trace {
    (
        $name:ident : {
            basis: $basis_ty:ty : $state_ty:ty,
            transitions: { $( $tran_s1:path => $tran_s2:path ),+ $(,)? },
            stretch: $stretch_s1:path => $stretch_s2:path $(,)?
        }
    ) => {
        #[allow(clippy::too_many_arguments)]
        pub fn $name(
            basis: $basis_ty,
            phi: ndarray::ArrayView1<f64>,
            W: ndarray::ArrayView1<f64>,
            alpha: f64,
            beta: f64,
            theta: f64,
            t: ndarray::ArrayView1<f64>,
        ) -> $crate::error::HilbertResult<
            ndarray::Array3<num_complex::Complex64>
        >
        {
            if phi.len() != W.len() {
                return Err($crate::error::HilbertError::NonEqTimeDep);
            }
            let n: usize = basis.len();
            let energies: Vec<f64>
                = (0..n).map(|i| basis.get_energy_i(i).unwrap()).collect();

            let mut H: ndarray::Array3<num_complex::Complex64>
                = ndarray::Array::zeros((n, n, t.len()));
            let transitions
                = itertools::Itertools::cartesian_product(
                    (0..n).map(|i| (i, *basis.get_state(i).unwrap())),
                    (0..n).map(|j| (j, *basis.get_state(j).unwrap())),
                );
            let N_w3j: f64
                = $crate::hilbert::trans_w3j(
                    $stretch_s1.spin(), $stretch_s2.spin());

            let drive
                = |i: usize, s1: &$state_ty, j: usize, s2: &$state_ty|
                -> ndarray::Array1<num_complex::Complex64>
                {
                    let pol: num_complex::Complex64
                        = pol_poincare(s1, s2, alpha, beta, theta);
                    let cg: f64 = cg_normalized(s1, s2, N_w3j);
                    let w0: f64 = energies[j] - energies[i];
                    ndarray::Array1::from_iter(
                        phi.iter().zip(W.iter()).zip(t.iter()).map(
                            |((ph, w), tt)| (w / 2.0) * pol * cg
                                * (
                                    num_complex::Complex64::i()
                                        * (ph - w0 * tt)
                                ).exp()
                        )
                    )
                };

            let mut add_drive
                = |
                    i: usize, j: usize,
                    drive: ndarray::Array1<num_complex::Complex64>
                |
                {
                    (drive.clone())
                        .move_into(H.slice_mut(ndarray::s![i, j, ..]));
                    (drive.mapv(|X| X.conj()))
                        .move_into(H.slice_mut(ndarray::s![j, i, ..]));
                };

            for ((i, s1), (j, s2)) in transitions {
                match (s1, s2) {
                    $(
                        ($tran_s1, $tran_s2)
                            => add_drive(j, i, drive(i, &s1, j, &s2)),
                    )+
                    _ => { },
                };
            }
            return Ok(H);
        }
    }
}

/// Generate a function that creates the time-dependent Hamiltonian matrix for a
/// time-dependent drive with explicitly defined phase and well-defined,
/// time-dependent polarization applied to a manifold of states.
///
/// A "stretched" transition (that between the states of maximal spin projection
/// quantum numbers in the ground and excited manifolds) must be specified for
/// Clebsch-Gordan coefficient normalization.
///
/// The generated function signature is
/// ```ignore
/// pub fn $name(
///     basis: $basis_ty,
///     phi: ndarray::ArrayView1<f64>,
///     W: ndarray::ArrayView1<f64>,
///     alpha: ndarray::ArrayView1<f64>,
///     beta: ndarray::ArrayView1<f64>,
///     theta: ndarray::ArrayView1<f64>,
///     t: ndarray::ArrayView1<f64>,
/// ) -> HilbertResult<ndarray::Array3<num_complex::Complex64>>
/// ```
/// where `basis` is the [`Basis`] type containing states and energies, `phi` is
/// the explicit phase of the drive (it should be monotonically increasing for
/// constant drive frequency), `W` is the strength (Rabi frequency) of the
/// drive, `alpha` is the mixing angle between horizontal and vertical
/// polarization components, `beta` is the phase angle between them (both are
/// relative to the horizontal component), `theta` is the angle of the incoming
/// *k*-vector relative to the magnetic field, and `t` contains time
/// coordinates. All frequencies (including energies) should be in angular
/// units. The returned Hamiltonian has shape `(N, N, T)`, where `N` is the size
/// of the basis, and `T` is the number of time coordinates.
///
/// Note: The complex phases associated with the polarization weighting factors
/// is not exactly correct. See the Kaufman erratum ([PRX **12** 021027
/// (2022)][erratum])
///
/// [erratum]: https://journals.aps.org/prx/abstract/10.1103/PhysRevX.13.029902
#[macro_export]
macro_rules! pulse_fn_poincare_trace_t {
    (
        $name:ident : {
            basis: $basis_ty:ty : $state_ty:ty,
            transitions: { $( $tran_s1:path => $tran_s2:path ),+ $(,)? },
            stretch: $stretch_s1:path => $stretch_s2:path $(,)?
        }
    ) => {
        #[allow(clippy::too_many_arguments)]
        pub fn $name(
            basis: $basis_ty,
            phi: ndarray::ArrayView1<f64>,
            W: ndarray::ArrayView1<f64>,
            alpha: ndarray::ArrayView1<f64>,
            beta: ndarray::ArrayView1<f64>,
            theta: ndarray::ArrayView1<f64>,
            t: ndarray::ArrayView1<f64>,
        ) -> $crate::error::HilbertResult<
            ndarray::Array3<num_complex::Complex64>
        >
        {
            if [
                    phi.view(), W.view(),
                    alpha.view(), beta.view(), theta.view()
                ].iter().any(|x| x.len() != t.len())
            {
                return Err($crate::error::HilbertError::NonEqTimeDep);
            }
            let n: usize = basis.len();
            let energies: Vec<f64>
                = (0..n).map(|i| basis.get_energy_i(i).unwrap()).collect();
            let time_dep: Vec<ndarray::Array1<f64>>
                = gen_time_dep(t, energies, vec![], vec![])?;
            // time_dep = [E1, ..., En]
            //             ^0       ^n-1

            let mut H: ndarray::Array3<num_complex::Complex64>
                = ndarray::Array::zeros((n, n, t.len()));
            let transitions
                = itertools::Itertools::cartesian_product(
                    (0..n).map(|i| (i, *basis.get_state(i).unwrap())),
                    (0..n).map(|j| (j, *basis.get_state(j).unwrap())),
                );
            let N_w3j: f64
                = $crate::hilbert::trans_w3j(
                    $stretch_s1.spin(), $stretch_s2.spin());

            let drive
                = |i: usize, s1: &$state_ty, j: usize, s2: &$state_ty|
                -> ndarray::Array1<num_complex::Complex64>
                {
                    let pol: ndarray::Array1<num_complex::Complex64>
                        = alpha.iter().zip(beta.iter()).zip(theta.iter())
                        .map(|((aa, bb), th)|
                            pol_poincare(s1, s2, *aa, *bb, *th)
                        )
                        .collect();
                    let cg: f64 = cg_normalized(s1, s2, N_w3j);
                    let w0: ndarray::Array1<f64> = &time_dep[j] - &time_dep[i];
                    let amp: ndarray::Array1<num_complex::Complex64>
                        = &pol * &W / 2.0 * cg;
                    let phase: ndarray::Array1<f64> = &phi - &(w0 * &t);
                    &amp * &phase.mapv(
                        |ph| (num_complex::Complex64::i() * ph).exp()
                    )
                };

            let mut add_drive
                = |
                    i: usize, j: usize,
                    drive: ndarray::Array1<num_complex::Complex64>
                |
                {
                    (drive.clone())
                        .move_into(H.slice_mut(ndarray::s![i, j, ..]));
                    (drive.mapv(|X| X.conj()))
                        .move_into(H.slice_mut(ndarray::s![j, i, ..]));
                };

            for ((i, s1), (j, s2)) in transitions {
                match (s1, s2) {
                    $(
                        ($tran_s1, $tran_s2)
                            => add_drive(j, i, drive(i, &s1, j, &s2)),
                    )+
                    _ => { },
                };
            }
            return Ok(H);
        }
    }
}

/// Generate a function that creates a non-Hermitian, real matrix giving decay
/// rates between states in a manifold. It is assumed that decay from the
/// excited manifold to the ground manifold is characterized by a single rate
/// that is weighted only by Clebsch-Gordan coefficients.
///
/// The generated function signature is
/// ```ignore
/// pub fn $name(basis: $basis_ty) -> HilbertResult<ndarray::Array2<f64>>
/// ```
/// where `basis` is the [`Basis`] type containing states and energies. All
/// frequencies (including energies) should be in angular units.
#[macro_export]
macro_rules! couplings_fn_lindblad {
    (
        $name:ident : {
            basis: $basis_ty:ty : $state_ty:ty,
            decay: { $(
                $decay_e:path => $decay_g:path
            ),+ $(,)? } = $rate_f64:expr
        }
    ) => {
        #[allow(clippy::too_many_arguments)]
        pub fn $name(basis: $basis_ty) -> ndarray::Array2<f64> {
            let n: usize = basis.len();
            let transitions
                = itertools::Itertools::cartesian_product(
                    (0..n).map(|i| *basis.get_state(i).unwrap()),
                    (0..n).map(|j| *basis.get_state(j).unwrap()),
                );
            let Y: ndarray::Array2<f64>
                = ndarray::Array::from_iter(
                    transitions.map(
                        |(s1, s2)| match (s1, s2) {
                            $(
                                ($decay_g, $decay_e) => {
                                    $rate_f64 * trans_w3j(s1.spin(), s2.spin())
                                },
                            )+
                            _ => 0.0,
                        }
                    )
                ).into_shape((n, n)).unwrap();
            return Y;
        }
    };
    (
        $name:ident : {
            basis: $basis_ty:ty : $state_ty:ty,
            decay: { $(
                $decay_e:path => $decay_g:path = $rate_f64:expr
            ),+ $(,)? }
        }
    ) => {
        pub fn $name(basis: $basis_ty) -> ndarray::Array2<f64> {
            let n: usize = basis.len();
            let transitions
                = itertools::Itertools::cartesian_product(
                    (0..n).map(|i| *basis.get_state(i).unwrap()),
                    (0..n).map(|j| *basis.get_state(j).unwrap()),
                );
            let Y: ndarray::Array2<f64>
                = ndarray::Array::from_iter(
                    transitions.map(
                        |(s1, s2)| match (s1, s2) {
                            $(
                                ($decay_e, $decay_g) => {
                                    $rate_f64 * trans_w3j(s2.spin(), s1.spin())
                                },
                            )+
                            _ => 0.0,
                        }
                    )
                ).into_shape((n, n)).unwrap();
            return Y;
        }
    };
}

/// Generate a function that calculates non-linear Breit-Rabi energy shifts due
/// to a static magnetic field.
///
/// It is assumed that the function is characterized by five real parameters
/// `A0, ..., A4`, giving the shift as
/// ```text
/// A0 + A1 * B + A2 * sqrt(1 + A3 * B + A4 * B^2)
/// ```
/// The returned function has signature
/// ```ignore
/// fn $name(B: f64) -> f64
/// ```
#[macro_export]
macro_rules! zm_br_fn {
    (
        $name:ident : { $A0:expr, $A1:expr, $A2:expr, $A3:expr, $A4:expr $(,)? }
    ) => {
        fn $name(B: f64) -> f64 {
            return (
                $A0 + $A1 * B + $A2 * (1.0 + $A3 * B + $A4 * B.powi(2)).sqrt()
            );
        }
    }
}

