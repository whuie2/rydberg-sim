from __future__ import annotations
import numpy as np
from lib.hilbert import *
from lib import config_fn
from lib.constants import G_3P1_F12, G_1S0_F12, MU_B
from pathlib import Path
import toml

"""
Functions to generate Hamiltonians for the 171Yb ground(1S0)-narrowline(3P1,
F=1/2) manifold:

                             n1 ======
                 n0 ======


                 g0 ======
                             g1 ======

Provides additional functionality for non-trivial time dependence as well as
couplings for sigma+, sigma-, and pi polarized light with adjustable
polarization impurity. Implicitly applies the rotating wave approximation. All
energies are to be taken in s^-1 (i.e. angular frequency).
"""

class NarrowlineState(State):
    g0 = 0
    g1 = 1
    n0 = 2
    n1 = 3

    def spin(self) -> (float, float):
        if self == NarrowlineState.g0:
            return (1/2, -1/2)
        elif self == NarrowlineState.g1:
            return (1/2, +1/2)
        elif self == NarrowlineState.n0:
            return (1/2, -1/2)
        elif self == NarrowlineState.n1:
            return (1/2, +1/2)

MU_N = MU_B * G_3P1_F12 # MHz G^-1
MU_G = MU_B * G_1S0_F12 # MHz G^-1
_zm_br_g0 = zm_br_fn("_zm_br_g0", "",
    [
        -1.10727922e-08,
        +3.74955336e-04,
        +7.63360649e-09,
        +1.94851695e-02,
        +1.39214067e-08,
    ]
)
_zm_br_g1 = zm_br_fn("_zm_br_g1", "",
    [
        -1.41632243e-06,
        -3.78301182e-04,
        +1.41632838e-06,
        +4.72462887e+00,
        +5.58055706e+00,
    ]
)
def zm(state: NarrowlineState, B: float) -> float:
    if state == NarrowlineState.g0:
        return _zm_br_g0(B)
    elif state == NarrowlineState.g1:
        return _zm_br_g1(B)
    elif state in {NarrowlineState.n0, NarrowlineState.n1}:
        return MU_N * state.spin()[1] * B
    else:
        raise Exception("Invalid state")

class PulseType(Enum):
    Sigma_p = 0
    Sigma_m = 1
    Pi = 2

H_sigma_p = pulse_fn(
    name = "H_sigma_p",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a sigma+
    drive.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : float
        Drive frequency relative to the energies defined in `basis`.
    W : float
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : float
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma+ light.
    """,
    principal = (NarrowlineState.g0, NarrowlineState.n1),
    parasitic = [
        (NarrowlineState.g0, NarrowlineState.n0),
        (NarrowlineState.g1, NarrowlineState.n1),
        (NarrowlineState.g1, NarrowlineState.n0),
    ],
)

H_sigma_m = pulse_fn(
    name = "H_sigma_m",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a sigma-
    drive.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : float
        Drive frequency relative to the energies defined in `basis`.
    W : float
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : float
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma- light.
    """,
    principal = (NarrowlineState.g1, NarrowlineState.n0),
    parasitic = [
        (NarrowlineState.g1, NarrowlineState.n1),
        (NarrowlineState.g0, NarrowlineState.n1),
        (NarrowlineState.g0, NarrowlineState.n0),
    ],
)

H_pi = pulse_fn(
    name = "H_pi",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a pi
    drive.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : float
        Drive frequency relative to the energies defined in `basis`.
    W : float
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : float
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for pi light.
    """,
    principal = (NarrowlineState.g1, NarrowlineState.n1),
    parasitic = [
        (NarrowlineState.g1, NarrowlineState.n0),
        (NarrowlineState.g0, NarrowlineState.n1),
        (NarrowlineState.g0, NarrowlineState.n0),
    ],
)

def H_pulse(pulse_type: PulseType, basis: Basis, w, W, chi, t, phi=0.0) \
        -> np.ndarray:
    """
    Thin wrapper around `H_sigma_p`, `H_sigma_m`, or `H_pi`, depending on the
    value of `pulse_type`.
    """
    if pulse_type == PulseType.Sigma_p:
        return H_sigma_p(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Sigma_m:
        return H_sigma_m(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Pi:
        return H_pi(basis, w, W, chi, t, phi)
    else:
        raise Exception

H_poincare = pulse_fn_poincare(
    name = "H_poincare",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for arbitrary
    polarizations of the drive with respect to the quantization axis.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : float
        Drive frequency relative to the energies defined in `basis`.
    W : float
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    th, ph : float
        Poincare sphere coordinates.
    t : numpy.ndarray[dim=1, dtype=numpy.float64]
        Time coordinate grid.
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for the specified
        polarization.
    """,
    transitions = [
        (NarrowlineState.g0, NarrowlineState.n1),
        (NarrowlineState.g0, NarrowlineState.n0),
        (NarrowlineState.g1, NarrowlineState.n1),
        (NarrowlineState.g1, NarrowlineState.n0),
    ],
)

H_sigma_p_t = pulse_fn_t(
    name = "H_sigma_p_t",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a sigma+
    drive. Requires time dependence

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.float64]
        Drive frequency relative to the energies defined in `basis`.
    W : numpy.ndarray[dim=1, dtype=numpy.float64]
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : numpy.ndarray[dim=1, dtype=numpy.float64]
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma+ light.
    """,
    principal = (NarrowlineState.g0, NarrowlineState.n1),
    parasitic = [
        (NarrowlineState.g0, NarrowlineState.n0),
        (NarrowlineState.g1, NarrowlineState.n1),
        (NarrowlineState.g1, NarrowlineState.n0),
    ],
)

H_sigma_m_t = pulse_fn_t(
    name = "H_sigma_m_t",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a sigma-
    drive. Requires time dependence.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.float64]
        Drive frequency relative to the energies defined in `basis`.
    W : numpy.ndarray[dim=1, dtype=numpy.float64]
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : numpy.ndarray[dim=1, dtype=numpy.float64]
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma- light.
    """,
    principal = (NarrowlineState.g1, NarrowlineState.n0),
    parasitic = [
        (NarrowlineState.g1, NarrowlineState.n1),
        (NarrowlineState.g0, NarrowlineState.n1),
        (NarrowlineState.g0, NarrowlineState.n0),
    ],
)

H_pi_t = pulse_fn_t(
    name = "H_pi_t",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a pi
    drive. Requires time dependence.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.float64]
        Drive frequency relative to the energies defined in `basis`.
    W : numpy.ndarray[dim=1, dtype=numpy.float64]
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : numpy.ndarray[dim=1, dtype=numpy.float64]
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for pi light.
    """,
    principal = (NarrowlineState.g1, NarrowlineState.n1),
    parasitic = [
        (NarrowlineState.g1, NarrowlineState.n0),
        (NarrowlineState.g0, NarrowlineState.n1),
        (NarrowlineState.g0, NarrowlineState.n0),
    ],
)

def H_pulse_t(pulse_type: PulseType, basis: Basis, w, W, chi, t, phi=0.0) \
        -> np.ndarray:
    """
    Thin wrapper around `H_sigma_p_t`, `H_sigma_m_t`, or `H_pi_t`, depending on
    the value of `pulse_type`.
    """
    if pulse_type == PulseType.Sigma_p:
        return H_sigma_p(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Sigma_m:
        return H_sigma_m(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Pi:
        return H_pi(basis, w, W, chi, t, phi)
    else:
        raise Exception

H_poincare_t = pulse_fn_poincare_t(
    name = "H_poincare_t",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for arbitrary
    polarizations of the drive with respect to the quantization axis. Requires
    time dependence.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.float64]
        Drive frequency relative to the energies defined in `basis`.
    W : numpy.ndarray[dim=1, dtype=numpy.float64]
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    th, ph : numpy.ndarray[dim=1, dtype=numpy.float64]
        Poincare sphere coordinates.
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for the specified
        polarization.
    """,
    transitions = [
        (NarrowlineState.g0, NarrowlineState.n1),
        (NarrowlineState.g0, NarrowlineState.n0),
        (NarrowlineState.g1, NarrowlineState.n1),
        (NarrowlineState.g1, NarrowlineState.n0),
    ],
)

# Targeted keys
# -------------
# W : float = 5.0
#   Drive strength in MHz. Returned in us^-1 (angular frequency).
# Weff : float = 1.0
#   Targeted effective Rabi frequency in MHz. Returned in us^-1 (angular
#   frequency).
# B : float = 200.0
#   Magnetic field strength in G.
# ND : int = 100
#   Number of points to use on each dimension of any renderable data array,
#   except for time.
load_config, Config = config_fn(
    "config.toml",
    "narrowline",
    [
        ("W", 5.0, float, "W", lambda W: 2.0 * np.pi * W),
        ("Weff", 1.0, float, "Weff", lambda W: 2.0 * np.pi * W),
        ("B", 200.0, float, "B", lambda B: B),
        ("ND", 100, int, "ND", lambda ND: ND),
    ],
)

