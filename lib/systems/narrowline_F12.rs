use std::{
    f64::consts::PI,
};
use ndarray as nd;
use num_complex::Complex64 as C64;
use crate::{
    zm_br_fn,
    mkstate,
    mkbasis,
    pulse_fn,
    pulse_fn_t,
    pulse_fn_poincare,
    pulse_fn_poincare_t,
    config_fn,
    error::*,
    hilbert::{
        *,
    },
    constants::{
        G_3P1_F12,
        G_1S0_F12,
        MU_B,
    },
};

pub const MU_N: f64 = MU_B * G_3P1_F12; // MHz G^-1
pub const MU_G: f64 = MU_B * G_1S0_F12; // MHz G^-1
zm_br_fn!(
    zm_br_g0 : {
        -1.10727922e-08,
         3.74955336e-04,
         7.63360649e-09,
         1.94851695e-02,
         1.39214067e-08,
    }
);
zm_br_fn!(
    zm_br_g1 : {
        -1.41632243e-06,
        -3.78301182e-04,
         1.41632838e-06,
         4.72462887e+00,
         5.58055706e+00,
    }
);

pub fn zm(state: NarrowlineState, B: f64) -> f64 {
    return match state {
        NarrowlineState::g0 => zm_br_g0(B),
        NarrowlineState::g1 => zm_br_g1(B),
        NarrowlineState::n0 => MU_N * state.spin().1 * B,
        NarrowlineState::n1 => MU_N * state.spin().1 * B,
    };
}

mkstate!(
    NarrowlineState : {
        g0 = 0.5, -0.5,
        g1 = 0.5,  0.5,
        n0 = 0.5, -0.5,
        n1 = 0.5,  0.5,
    }
);

mkbasis!( NarrowlineBasis[NarrowlineState] );

pub enum PulseType {
    Sigma_p,
    Sigma_m,
    Pi,
}

pulse_fn!(
    H_sigma_p : {
        basis: NarrowlineBasis : NarrowlineState,
        principal:
            NarrowlineState::g0 => NarrowlineState::n1,
        parasitic: {
            NarrowlineState::g0 => NarrowlineState::n0,
            NarrowlineState::g1 => NarrowlineState::n1,
            NarrowlineState::g1 => NarrowlineState::n0,
        }
    }
);

pulse_fn!(
    H_sigma_m : {
        basis: NarrowlineBasis : NarrowlineState,
        principal:
            NarrowlineState::g1 => NarrowlineState::n0,
        parasitic: {
            NarrowlineState::g1 => NarrowlineState::n1,
            NarrowlineState::g0 => NarrowlineState::n1,
            NarrowlineState::g0 => NarrowlineState::n0,
        }
    }
);

pulse_fn!(
    H_pi : {
        basis: NarrowlineBasis : NarrowlineState,
        principal:
            NarrowlineState::g1 => NarrowlineState::n1,
        parasitic: {
            NarrowlineState::g1 => NarrowlineState::n0,
            NarrowlineState::g0 => NarrowlineState::n1,
            NarrowlineState::g0 => NarrowlineState::n0,
        }
    }
);

pub fn H_pulse(pulse_type: PulseType, basis: NarrowlineBasis, w: f64, W: f64,
        chi: f64, t: nd::ArrayView1<f64>, phi: Option<f64>)
    -> HilbertResult<nd::Array3<C64>>
{
    return match pulse_type {
        PulseType::Sigma_p
            => H_sigma_p(basis, w, W, chi, t, phi.unwrap_or(0.0)),
        PulseType::Sigma_m
            => H_sigma_m(basis, w, W, chi, t, phi.unwrap_or(0.0)),
        PulseType::Pi
            => H_pi(basis, w, W, chi, t, phi.unwrap_or(0.0)),
    };
}

pulse_fn_poincare!(
    H_poincare : {
        basis: NarrowlineBasis : NarrowlineState,
        transitions: {
            NarrowlineState::g1 => NarrowlineState::n1,
            NarrowlineState::g1 => NarrowlineState::n0,
            NarrowlineState::g0 => NarrowlineState::n1,
            NarrowlineState::g0 => NarrowlineState::n0,
        },
        stretch: NarrowlineState::g0 => NarrowlineState::n1,
    }
);

pulse_fn_t!(
    H_sigma_p_t : {
        basis: NarrowlineBasis : NarrowlineState,
        principal:
            NarrowlineState::g0 => NarrowlineState::n1,
        parasitic: {
            NarrowlineState::g0 => NarrowlineState::n0,
            NarrowlineState::g1 => NarrowlineState::n1,
            NarrowlineState::g1 => NarrowlineState::n0,
        }
    }
);

pulse_fn_t!(
    H_sigma_m_t : {
        basis: NarrowlineBasis : NarrowlineState,
        principal:
            NarrowlineState::g1 => NarrowlineState::n0,
        parasitic: {
            NarrowlineState::g1 => NarrowlineState::n1,
            NarrowlineState::g0 => NarrowlineState::n1,
            NarrowlineState::g0 => NarrowlineState::n0,
        }
    }
);

pulse_fn_t!(
    H_pi_t : {
        basis: NarrowlineBasis : NarrowlineState,
        principal:
            NarrowlineState::g1 => NarrowlineState::n1,
        parasitic: {
            NarrowlineState::g1 => NarrowlineState::n0,
            NarrowlineState::g0 => NarrowlineState::n1,
            NarrowlineState::g0 => NarrowlineState::n0,
        }
    }
);

pub fn H_pulse_t(pulse_type: PulseType, basis: NarrowlineBasis,
                 w: nd::ArrayView1<f64>, W: nd::ArrayView1<f64>,
                 chi: nd::ArrayView1<f64>, t: nd::ArrayView1<f64>,
                 phi: Option<f64>)
    -> HilbertResult<nd::Array3<C64>>
{
    return match pulse_type {
        PulseType::Sigma_p
            => H_sigma_p_t(basis, w, W, chi, t, phi.unwrap_or(0.0)),
        PulseType::Sigma_m
            => H_sigma_m_t(basis, w, W, chi, t, phi.unwrap_or(0.0)),
        PulseType::Pi
            => H_pi_t(basis, w, W, chi, t, phi.unwrap_or(0.0)),
    };
}

pulse_fn_poincare_t!(
    H_poincare_t : {
        basis: NarrowlineBasis : NarrowlineState,
        transitions: {
            NarrowlineState::g1 => NarrowlineState::n1,
            NarrowlineState::g1 => NarrowlineState::n0,
            NarrowlineState::g0 => NarrowlineState::n1,
            NarrowlineState::g0 => NarrowlineState::n0,
        },
        stretch: NarrowlineState::g0 => NarrowlineState::n1,
    }
);

/// Targeted keys
/// -------------
/// W = 5.0_f64
///     Drive strength in MHz. Returned in us^-1 (angular frequency).
/// Weff = 1.0_f64
///     Targeted effective Rabi frequency in MHz. Returned in us^-1 (angular
///     frequency).
/// B = 200.0_f64
///     Magnetic field strength in G.
/// ND = 100_usize
///     Number of points to use on each dimension of any renderable data array,
///     except for time.
config_fn!(
    "config.toml", "narrowline" => {
        "W", 5.0, f64
            => W : f64 = |W: f64| -> f64 { 2.0 * PI * W },
        "Weff", 1.0, f64
            => Weff : f64 = |W: f64| -> f64 { 2.0 * PI * W },
        "B", 3.5, f64
            => B : f64 = |B: f64| -> f64 { B },
        "ND", 100, i64
            => ND : usize = |ND: i64| -> usize { ND as usize }
    }
);

