from __future__ import annotations
import numpy as np
from lib.hilbert import *
from lib import config_fn
from lib.constants import G_3S1_F32, G_3P0_F12, MU_B
from pathlib import Path
import toml

"""
Functions to generate Hamiltonians for the 171Yb clock-Rydberg manifold:

                                        r1+ ======
                            r0+ ======
                r0- ======
    r0- ======


                c0  ======
                            c1  ======

Provides additional functionality for non-trivial time-dependence as well as
couplings for sigma+, sigma-, and pi polarized light with adjustable
polarization impurity. Implicitly applies the rotating wave approximation. All
energies are to be taken in s^-1 (i.e. angular frequency).
"""

class RydbergState(State):
    c0 = 0
    c1 = 1
    r1m = 2
    r0m = 3
    r0p = 4
    r1p = 5

    def spin(self) -> (float, float):
        if self == RydbergState.c0:
            return (1/2, -1/2)
        elif self == RydbergState.c1:
            return (1/2, +1/2)
        elif self == RydbergState.r1m:
            return (3/2, -3/2)
        elif self == RydbergState.r0m:
            return (3/2, -1/2)
        elif self == RydbergState.r0p:
            return (3/2, +1/2)
        elif self == RydbergState.r1p:
            return (3/2, +3/2)

    def is_rydberg(self) -> bool:
        return self in {
            RydbergState.r1m,
            RydbergState.r0m,
            RydbergState.r0p,
            RydbergState.r1p
        }

MU_R = MU_B * G_3S1_F32 # MHz G^-1
MU_C = MU_B * G_3P0_F12 # MHz G^-1
C6 = 300000.0 # MHz um^6

class PulseType(Enum):
    Sigma_p = 0
    Sigma_m = 1
    Pi = 2

H_sigma_p = pulse_fn(
    name = "H_sigma_p",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a sigma+
    drive.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : float
        Drive frequency relative to the energies defined in `basis`.
    W : float
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : float
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma+ light.
    """,
    principal = (RydbergState.c1, RydbergState.r1p),
    parasitic = [
        (RydbergState.c1, RydbergState.r0p),
        (RydbergState.c1, RydbergState.r0m),
        (RydbergState.c0, RydbergState.r0p),
        (RydbergState.c0, RydbergState.r0m),
        (RydbergState.c0, RydbergState.r1m),
    ],
)

H_sigma_m = pulse_fn(
    name = "H_sigma_m",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a sigma-
    drive.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : float
        Drive frequency relative to the energies defined in `basis`.
    W : float
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : float
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma- light.
    """,
    principal = (RydbergState.c1, RydbergState.r0m),
    parasitic = [
        (RydbergState.c1, RydbergState.r1p),
        (RydbergState.c1, RydbergState.r0p),
        (RydbergState.c0, RydbergState.r0p),
        (RydbergState.c0, RydbergState.r0m),
        (RydbergState.c0, RydbergState.r1m),
    ],
)

H_pi = pulse_fn(
    name = "H_pi",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a pi
    drive.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : float
        Drive frequency relative to the energies defined in `basis`.
    W : float
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : float
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for pi light.
    """,
    principal = (RydbergState.c1, RydbergState.r0p),
    parasitic = [
        (RydbergState.c1, RydbergState.r1p),
        (RydbergState.c1, RydbergState.r0m),
        (RydbergState.c0, RydbergState.r0p),
        (RydbergState.c0, RydbergState.r0m),
        (RydbergState.c0, RydbergState.r1m),
    ],
)

def H_pulse(pulse_type: PulseType, basis: Basis, w, W, chi, t, phi=0.0) \
        -> np.ndarray:
    """
    Thin wrapper around `H_sigma_p`, `H_sigma_m`, or `H_pi`, depending on the
    value of `pulse_type`.
    """
    if pulse_type == PulseType.Sigma_p:
        return H_sigma_p(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Sigma_m:
        return H_sigma_m(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Pi:
        return H_pi(basis, w, W, chi, t, phi)
    else:
        raise Exception

H_poincare = pulse_fn_poincare(
    name = "H_poincare",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for arbitrary
    polarizations of the drive with respect to the quantization axis.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : float
        Drive frequency relative to the energies defined in `basis`.
    W : float
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    psi, chi : float
        Angles (psi for polarization angle relative to the direction
        perpendicular to the quantization axis and chi for ellipticity) that
        generate Stokes parameters and double to give coordinates on the
        Poincare sphere.
    theta : float
        Angle relative to the quantization axis at which light meets the atom.
    t : numpy.ndarray[dim=1, dtype=numpy.float64]
        Time coordinate grid.
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for the specified
        polarization.
    """,
    transitions = [
        (RydbergState.c1, RydbergState.r1p),
        (RydbergState.c1, RydbergState.r0p),
        (RydbergState.c1, RydbergState.r0m),
        (RydbergState.c0, RydbergState.r0p),
        (RydbergState.c0, RydbergState.r0m),
        (RydbergState.c0, RydbergState.r1m),
    ],
)

H_sigma_p_t = pulse_fn_t(
    name = "H_sigma_p_t",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a sigma+
    drive. Requires time dependence

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.float64]
        Drive frequency relative to the energies defined in `basis`.
    W : numpy.ndarray[dim=1, dtype=numpy.float64]
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : numpy.ndarray[dim=1, dtype=numpy.float64]
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma+ light.
    """,
    principal = (RydbergState.c1, RydbergState.r1p),
    parasitic = [
        (RydbergState.c1, RydbergState.r0p),
        (RydbergState.c1, RydbergState.r0m),
        (RydbergState.c0, RydbergState.r0p),
        (RydbergState.c0, RydbergState.r0m),
        (RydbergState.c0, RydbergState.r1m),
    ],
)

H_sigma_m_t = pulse_fn_t(
    name = "H_sigma_m_t",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a sigma-
    drive. Requires time dependence.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.float64]
        Drive frequency relative to the energies defined in `basis`.
    W : numpy.ndarray[dim=1, dtype=numpy.float64]
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : numpy.ndarray[dim=1, dtype=numpy.float64]
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma- light.
    """,
    principal = (RydbergState.c1, RydbergState.r0m),
    parasitic = [
        (RydbergState.c1, RydbergState.r1p),
        (RydbergState.c1, RydbergState.r0p),
        (RydbergState.c0, RydbergState.r0p),
        (RydbergState.c0, RydbergState.r0m),
        (RydbergState.c0, RydbergState.r1m),
    ],
)

H_pi_t = pulse_fn_t(
    name = "H_pi_t",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a pi
    drive. Requires time dependence.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.float64]
        Drive frequency relative to the energies defined in `basis`.
    W : numpy.ndarray[dim=1, dtype=numpy.float64]
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : numpy.ndarray[dim=1, dtype=numpy.float64]
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for pi light.
    """,
    principal = (RydbergState.c1, RydbergState.r0p),
    parasitic = [
        (RydbergState.c1, RydbergState.r1p),
        (RydbergState.c1, RydbergState.r0m),
        (RydbergState.c0, RydbergState.r0p),
        (RydbergState.c0, RydbergState.r0m),
        (RydbergState.c0, RydbergState.r1m),
    ],
)

def H_pulse_t(pulse_type: PulseType, basis: Basis, w, W, chi, t, phi=0.0) \
        -> np.ndarray:
    """
    Thin wrapper around `H_sigma_p_t`, `H_sigma_m_t`, or `H_pi_t`, depending on
    the value of `pulse_type`.
    """
    if pulse_type == PulseType.Sigma_p:
        return H_sigma_p(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Sigma_m:
        return H_sigma_m(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Pi:
        return H_pi(basis, w, W, chi, t, phi)
    else:
        raise Exception

H_poincare_t = pulse_fn_poincare_t(
    name = "H_poincare_t",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for arbitrary
    polarizations of the drive with respect to the quantization axis. Requires
    time dependence.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.float64]
        Drive frequency relative to the energies defined in `basis`.
    W : numpy.ndarray[dim=1, dtype=numpy.float64]
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    psi, chi : numpy.ndarray[dim=1, dtype=numpy.float64]
        Angles (psi for polarization angle relative to the direction
        perpendicular to the quantization axis and chi for ellipticity) that
        generate Stokes parameters and double to give coordinates on the
        Poincare sphere.
    theta : numpy.ndarray[dim=1, dtype=numpy.float64]
        Angle relative to the quantization axis at which light meets the atom.
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for the specified
        polarization.
    """,
    transitions = [
        (RydbergState.c1, RydbergState.r1p),
        (RydbergState.c1, RydbergState.r0p),
        (RydbergState.c1, RydbergState.r0m),
        (RydbergState.c0, RydbergState.r0p),
        (RydbergState.c0, RydbergState.r0m),
        (RydbergState.c0, RydbergState.r1m),
    ],
)

def gen_multiatom(basis, n, H, U, basis_n=None) -> np.ndarray:
    """
    Generate the n-atom Hamiltonian from a single-atom Hamilonian (assume all
    atoms are subject to the same drive).

    Parameters
    ----------
    basis : Basis
        Single-atom basis.
    n : int
        Number of atoms.
    H : numpy.ndarray[shape=(_, _, k), dtype=numpy.complex128]
        Single-atom Hamiltonian. Can be time-dependent, with the third index
        corresponding to time, which requires `U` be time-dependent as well.
    U : float | numpy.ndarray[shape=(k,), dtype=numpy.complex128]
        Rydberg interaction strength. If time-dependent (passed as an array),
        then `H` is required to be time-dependent as well.

    Returns
    -------
    B_n : Basis
        n-atom basis.
    H_n : numpy.ndarray[shape=(_, _, k), dtype=numpy.complex128]
        n-atom Hamiltonian.
    """
    time_dep = all([len(H.shape) == 3, isinstance(U, np.ndarray)])
    time_indep = all([len(H.shape) == 2, isinstance(U, float)])
    assert time_dep ^ time_indep
    B_n = basis.to_multiatom(n) if basis_n is None else basis_n
    if not time_dep:
        H_n = sum(
            np.kron(
                np.kron(
                    np.eye(len(basis)**k, dtype=np.complex128),
                    H
                ),
                np.eye(len(basis)**(n - k - 1), dtype=np.complex128)
            )
            for k in range(n)
        )
        for i, s in enumerate(B_n.labels):
            k = sum(map(lambda x: x.is_rydberg(), s))
            if k > 1:
                H_n[i, i] += (k - 1) * U
        return B_n, H_n
    else:
        assert H.shape[-1] == U.shape[0]
        H_n = np.array([
            gen_multiatom(basis, n, H[:, :, i], U[i], B_n)[1].T
            for i in range(U.shape[0])
        ], dtype=np.complex128).T
        return B_n, H_n

def gen_chain(basis, n, H, U, basis_n=None) -> np.ndarray:
    """
    Generate the n-atom Hamiltonian for a 1D Rydberg chain from a single-atom
    Hamilonian (assume all atoms are subject to the same drive).

    Parameters
    ----------
    basis : Basis
        Single-atom basis.
    n : int
        Number of atoms.
    H : numpy.ndarray[shape=(_, _, k), dtype=numpy.complex128]
        Single-atom Hamiltonian. Can be time-dependent, with the third index
        corresponding to time, which requires `U` be time-dependent as well.
    U : float | numpy.ndarray[shape=(k,), dtype=numpy.complex128]
        Rydberg interaction strength. If time-dependent (passed as an array),
        then `H` is required to be time-dependent as well.

    Returns
    -------
    B_n : Basis
        n-atom basis.
    H_n : numpy.ndarray[shape=(_, _, k), dtype=numpy.complex128]
        n-atom Hamiltonian.
    """
    time_dep = all([len(H.shape) == 3, isinstance(U, np.ndarray)])
    time_indep = all([len(H.shape) == 2, isinstance(U, float)])
    assert time_dep ^ time_indep
    B_n = basis.to_multiatom(n) if basis_n is None else basis_n
    if not time_dep:
        H_n = sum(
            np.kron(
                np.kron(
                    np.eye(len(basis)**k, dtype=np.complex128),
                    H
                ),
                np.eye(len(basis)**(n - k - 1), dtype=np.complex128)
            )
            for k in range(n)
        )
        for i, s in enumerate(B_n.labels):
            u = sum(
                sum(
                    U / (i - j)**6
                    for j, aj in enumerate(s)
                    if aj.is_rydberg() and i != j
                )
                for i, ai in enumerate(s)
                if ai.is_rydberg()
            ) / 2
            H_n[i, i] += u
        return B_n, H_n
    else:
        assert H.shape[-1] == U.shape[0]
        H_n = np.array([
            gen_chain(basis, n, H[:, :, i], U[i], B_n)[1].T
            for i in range(U.shape[0])
        ], dtype=np.complex128).T
        return B_n, H_n

def gen_multiatom_per(basis, HH, U, basis_n=None):
    """
    Generate the n-atom Hamiltonian from a collection of single-atom
    Hamiltonians.

    Parameters
    ----------
    basis : Basis
        Single-atom basis.
    HH : list[numpy.ndarray[shape=(_, _, k), dtype=numpy.complex128]]
        List-like of single-atom Hamiltonians. Can be time-dependent, with the
        third index corresponding to time, which requires `U` be time-dependent
        as well.
    U : float | numpy.ndarray[shape=(k,), dtype=numpy.complex128]
        Rydberg interaction strength. If time-dependent (passed as an array),
        then `H` is required to be time-dependent as well.

    Returns
    -------
    B_n : Basis
        n-atom basis.
    H_n : numpy.ndarray[shape=(_, _, k), dtype=numpy.complex128]
        n-atom Hamiltonian.
    """
    time_dep = all([
        all([len(H.shape) == 3 for H in HH]),
        isinstance(U, np.ndarray)
    ])
    time_indep = all([
        all([len(H.shape) == 2 for H in HH]),
        isinstance(U, float)
    ])
    assert time_dep ^ time_indep
    n = len(HH)
    B_n = basis.to_multiatom(n) if basis_n is None else basis_n
    if not time_dep:
        H_n = sum(
            np.kron(
                np.kron(
                    np.eye(len(basis)**k, dtype=np.complex128),
                    H
                ),
                np.eye(len(basis)**(n - k - 1), dtype=np.complex128)
            )
            for k, H in enumerate(HH)
        )
        for i, s in enumerate(B_n.labels):
            k = sum(map(lambda x: x.is_rydberg(), s))
            if k > 1:
                H_n[i, i] += (k - 1) * U
    else:
        assert all(H.shape[-1] == U.shape[0] for H in HH)
        H_n = np.array([
            gen_multiatom_per(
                basis, [H[:, :, i] for H in HH], U[i], B_n)[1].T
            for i in range(U.shape[0])
        ], dtype=np.complex128).T
        return B_n, H_n

def gen_chain_per(basis, HH, U, basis_n=None):
    """
    Generate the n-atom Hamiltonian for a 1D Rydberg chain from a collection of
    single-atom Hamiltonians.

    Parameters
    ----------
    basis : Basis
        Single-atom basis.
    HH : list[numpy.ndarray[shape=(_, _, k), dtype=numpy.complex128]]
        List-like of single-atom Hamiltonians. Can be time-dependent, with the
        third index corresponding to time, which requires `U` be time-dependent
        as well.
    U : float | numpy.ndarray[shape=(k,), dtype=numpy.complex128]
        Rydberg interaction strength. If time-dependent (passed as an array),
        then `H` is required to be time-dependent as well.

    Returns
    -------
    B_n : Basis
        n-atom basis.
    H_n : numpy.ndarray[shape=(_, _, k), dtype=numpy.complex128]
        n-atom Hamiltonian.
    """
    time_dep = all([
        all([len(H.shape) == 3 for H in HH]),
        isinstance(U, np.ndarray)
    ])
    time_indep = all([
        all([len(H.shape) == 2 for H in HH]),
        isinstance(U, float)
    ])
    assert time_dep ^ time_indep
    n = len(HH)
    B_n = basis.to_multiatom(n) if basis_n is None else basis_n
    if not time_dep:
        H_n = sum(
            np.kron(
                np.kron(
                    np.eye(len(basis)**k, dtype=np.complex128),
                    H
                ),
                np.eye(len(basis)**(n - k - 1), dtype=np.complex128)
            )
            for k, H in enumerate(HH)
        )
        for i, s in enumerate(B_n.labels):
            u = sum(
                sum(
                    U / (i - j)**6
                    for j, aj in enumerate(s)
                    if aj.is_rydberg() and i != j
                )
                for i, ai in enumerate(s)
                if ai.is_rydberg()
            ) / 2
            H_n[i, i] += u
        return B_n, H_n
    else:
        assert all(H.shape[-1] == U.shape[0] for H in HH)
        H_n = np.array([
            gen_chain_per(
                basis, [H[:, :, i] for H in HH], U[i], B_n)[1].T
            for i in range(U.shape[0])
        ], dtype=np.complex128).T
        return B_n, H_n

def enumerators(basis, n=1, enumeration=None):
    """
    Generate "enumeration" operators that assign numbers to each of the
    single-atom states in `basis` for a single atom in the `n`-atom basis.

    Parameters
    ----------
    basis : Basis
        Single-atom basis.
    n : int (optional)
        Number of atoms.
    enumeration : list[int] (optional)
        Customize the enumeration of the single-atom states. If left
        unspecified, use [0, ..., B - 1], where B is the length of `basis`.

    Returns
    -------
    ops : list[numpy.ndarray[dim=2, dtype=numpy.float64]]
        Enumeration operators for single atoms, written in the `n`-atom basis.
    """
    assert enumeration is None or len(enumeration) == len(basis)
    enum = np.arange(len(basis), dtype=np.float64) if enumeration is None \
            else enumeration
    E = np.diag(enum)
    ops = list()
    for k in range(n):
        e = np.eye(len(basis)**k, dtype=np.float64)
        e = np.kron(e, E)
        e = np.kron(e, np.eye(len(basis)**(n - k - 1), dtype=np.float64))
        ops.append(e)
    return ops

def enumerator_avg(basis, n=1, enumeration=None):
    """
    Generate an "average enumeration" operator that assigns a number 0 ... B - 1
    to each of the single-atom states in `basis` for `n` atoms (where B is the
    length of `basis`) and gives the average such number in a many-bady state.

    Parameters
    ----------
    basis : Basis
        Single-atom basis.
    n : int
        Number of atoms.
    enumeration : list[int] (optional)
        Customize the enumeration of the single-atom states. If left
        unspecified, use [0, ..., B - 1], where B is the length of `basis`.

    Returns
    -------
    E_n : numpy.ndarray[dim=2, dtype=numpy.float64]
        `n`-atom average enumeration operator.
    """
    return sum(enumeration_ops(basis, n)) / n

# Targeted keys
# -------------
# W : float = 6.0
#   Drive strength in MHz. Returned in us^-1 (angular frequency).
# r_sep : float = 3.5
#   Inter-atom separation in um. Returns the equivalent Rydberg interaction
#   shift, U = C6 / r_sep^6 in us^-1.
# ND : int = 100
#   Number of points to use on each dimension of any renderable data array,
#   except for time.
# B : float = 150.0
#   Upper end of the magnetic field strength plotting range in G.
load_config, Config = config_fn(
    "config.toml",
    "rydberg",
    [
        ("W", 6.0, float, "W", lambda W: 2.0 * np.pi * W),
        ("r_sep", 3.5, float, "U", lambda r: 2.0 * np.pi * C6 / r**6),
        ("ND", 100, int, "ND", lambda ND: ND),
        ("Bmax", 150.0, float, "Bmax", lambda B: B),
    ],
)

