from __future__ import annotations
import numpy as np
from lib.hilbert import *
from lib import config_fn
from lib.constants import G_3D1_F12, G_3P0_F12, MU_B
from pathlib import Path
import toml

"""
Functions to generate Hamiltonians for the 171Yb clock(3P0)-telecom(3D1, F=1/2)
manifold:

                             t1 ======
                 t0 ======


                c0  ======
                            c1  ======

Provides additional functionality for non-trivial time dependence as well as
couplings for sigma+, sigma-, and pi polarized light with adjustable
polarization impurity. Implicitly applies the rotating wave approximation. All
energies are to be taken in s^-1 (i.e. angular frequency).
"""

class TelecomState(State):
    c0 = 0
    c1 = 1
    t0 = 2
    t1 = 3

    def spin(self) -> (float, float):
        if self == TelecomState.c0:
            return (1/2, -1/2)
        elif self == TelecomState.c1:
            return (1/2, +1/2)
        elif self == TelecomState.t0:
            return (1/2, -1/2)
        elif self == TelecomState.t1:
            return (1/2, +1/2)

MU_T = MU_B * G_3D1_F12 # MHz G^-1
MU_C = MU_B * G_3P0_F12 # MHz G^-1
_zm_br_c0 = zm_br_fn("_zm_br_c0", "",
    [
        +1.01151916e+02,
        +9.40966754e-05,
        -1.01151915e+02,
        -1.99260954e-06,
        +2.02381457e-09,
    ]
)
_zm_br_c1 = zm_br_fn("_zm_br_c1", "",
    [
        +4.64712454e+00,
        -2.98286729e-04,
        -4.64711085e+00,
        -4.43638992e-05,
        +4.39216548e-08,
    ]
)
def zm(state: TelecomState, B: float) -> float:
    if state == TelecomState.c0:
        return _zm_br_c0(B)
    elif state == TelecomState.c1:
        return _zm_br_c1(B)
    elif state in {TelecomState.t0, TelecomState.t1}:
        return MU_T * state.spin()[1] * B
    else:
        raise Exception("Invalid state")

class PulseType(Enum):
    Sigma_p = 0
    Sigma_m = 1
    Pi = 2

H_sigma_p = pulse_fn(
    name = "H_sigma_p",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a sigma+
    drive.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : float
        Drive frequency relative to the energies defined in `basis`.
    W : float
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : float
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma+ light.
    """,
    principal = (TelecomState.c0, TelecomState.t1),
    parasitic = [
        (TelecomState.c0, TelecomState.t0),
        (TelecomState.c1, TelecomState.t1),
        (TelecomState.c1, TelecomState.t0),
    ],
)

H_sigma_m = pulse_fn(
    name = "H_sigma_m",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a sigma-
    drive.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : float
        Drive frequency relative to the energies defined in `basis`.
    W : float
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : float
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma- light.
    """,
    principal = (TelecomState.c1, TelecomState.t0),
    parasitic = [
        (TelecomState.c1, TelecomState.t1),
        (TelecomState.c0, TelecomState.t1),
        (TelecomState.c0, TelecomState.t0),
    ],
)

H_pi = pulse_fn(
    name = "H_pi",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a pi
    drive.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : float
        Drive frequency relative to the energies defined in `basis`.
    W : float
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : float
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for pi light.
    """,
    principal = (TelecomState.c1, TelecomState.t1),
    parasitic = [
        (TelecomState.c1, TelecomState.t0),
        (TelecomState.c0, TelecomState.t1),
        (TelecomState.c0, TelecomState.t0),
    ],
)

def H_pulse(pulse_type: PulseType, basis: Basis, w, W, chi, t, phi=0.0) \
        -> np.ndarray:
    """
    Thin wrapper around `H_sigma_p`, `H_sigma_m`, or `H_pi`, depending on the
    value of `pulse_type`.
    """
    if pulse_type == PulseType.Sigma_p:
        return H_sigma_p(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Sigma_m:
        return H_sigma_m(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Pi:
        return H_pi(basis, w, W, chi, t, phi)
    else:
        raise Exception

H_poincare = pulse_fn_poincare(
    name = "H_poincare",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for arbitrary
    polarizations of the drive with respect to the quantization axis.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : float
        Drive frequency relative to the energies defined in `basis`.
    W : float
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    th, ph : float
        Poincare sphere coordinates.
    t : numpy.ndarray[dim=1, dtype=numpy.float64]
        Time coordinate grid.
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for the specified
        polarization.
    """,
    transitions = [
        (TelecomState.c0, TelecomState.t1),
        (TelecomState.c0, TelecomState.t0),
        (TelecomState.c1, TelecomState.t1),
        (TelecomState.c1, TelecomState.t0),
    ],
)

H_sigma_p_t = pulse_fn_t(
    name = "H_sigma_p_t",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a sigma+
    drive. Requires time dependence

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.float64]
        Drive frequency relative to the energies defined in `basis`.
    W : numpy.ndarray[dim=1, dtype=numpy.float64]
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : numpy.ndarray[dim=1, dtype=numpy.float64]
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma+ light.
    """,
    principal = (TelecomState.c0, TelecomState.t1),
    parasitic = [
        (TelecomState.c0, TelecomState.t0),
        (TelecomState.c1, TelecomState.t1),
        (TelecomState.c1, TelecomState.t0),
    ],
)

H_sigma_m_t = pulse_fn_t(
    name = "H_sigma_m_t",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a sigma-
    drive. Requires time dependence.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.float64]
        Drive frequency relative to the energies defined in `basis`.
    W : numpy.ndarray[dim=1, dtype=numpy.float64]
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : numpy.ndarray[dim=1, dtype=numpy.float64]
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma- light.
    """,
    principal = (TelecomState.c1, TelecomState.t0),
    parasitic = [
        (TelecomState.c1, TelecomState.t1),
        (TelecomState.c0, TelecomState.t1),
        (TelecomState.c0, TelecomState.t0),
    ],
)

H_pi_t = pulse_fn_t(
    name = "H_pi_t",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a pi
    drive. Requires time dependence.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.float64]
        Drive frequency relative to the energies defined in `basis`.
    W : numpy.ndarray[dim=1, dtype=numpy.float64]
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : numpy.ndarray[dim=1, dtype=numpy.float64]
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for pi light.
    """,
    principal = (TelecomState.c1, TelecomState.t1),
    parasitic = [
        (TelecomState.c1, TelecomState.t0),
        (TelecomState.c0, TelecomState.t1),
        (TelecomState.c0, TelecomState.t0),
    ],
)

def H_pulse_t(pulse_type: PulseType, basis: Basis, w, W, chi, t, phi=0.0) \
        -> np.ndarray:
    """
    Thin wrapper around `H_sigma_p_t`, `H_sigma_m_t`, or `H_pi_t`, depending on
    the value of `pulse_type`.
    """
    if pulse_type == PulseType.Sigma_p:
        return H_sigma_p(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Sigma_m:
        return H_sigma_m(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Pi:
        return H_pi(basis, w, W, chi, t, phi)
    else:
        raise Exception

H_poincare_t = pulse_fn_poincare_t(
    name = "H_poincare_t",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for arbitrary
    polarizations of the drive with respect to the quantization axis. Requires
    time dependence.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.float64]
        Drive frequency relative to the energies defined in `basis`.
    W : numpy.ndarray[dim=1, dtype=numpy.float64]
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    th, ph : numpy.ndarray[dim=1, dtype=numpy.float64]
        Poincare sphere coordinates.
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for the specified
        polarization.
    """,
    transitions = [
        (TelecomState.c0, TelecomState.t1),
        (TelecomState.c0, TelecomState.t0),
        (TelecomState.c1, TelecomState.t1),
        (TelecomState.c1, TelecomState.t0),
    ],
)

# Targeted keys
# -------------
# W : float = 5.0
#   Drive strength in MHz. Returned in us^-1 (angular frequency).
# Weff : float = 1.0
#   Targeted effective Rabi frequency in MHz. Returned in us^-1 (angular
#   frequency).
# B : float = 200.0
#   Magnetic field strength in G.
# ND : int = 100
#   Number of points to use on each dimension of any renderable data array,
#   except for time.
load_config, Config = config_fn(
    "config.toml",
    "telecom",
    [
        ("W", 5.0, float, "W", lambda W: 2.0 * np.pi * W),
        ("Weff", 1.0, float, "Weff", lambda W: 2.0 * np.pi * W),
        ("B", 200.0, float, "B", lambda B: B),
        ("ND", 100, int, "ND", lambda ND: ND),
    ],
)

