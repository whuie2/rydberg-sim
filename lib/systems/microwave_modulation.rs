use std::{
    f64::consts::PI,
    path::PathBuf,
};
use crate::{
    mkstate,
    mkbasis,
    pulse_fn_poincare,
    pulse_fn_poincare_t,
    pulse_fn_poincare_trace,
    pulse_fn_poincare_trace_t,
    config_fn,
    hilbert::{
        *,
    },
};

mkstate!(
    MicroModState : {
        g = 0.5, 0.5,
        r = 1.5, 1.5,
        s = 1.5, 1.5,
    }
);
mkbasis!(MicroModBasis[MicroModState]);

pulse_fn_poincare!(
    H_poincare : {
        basis: MicroModBasis : MicroModState,
        transitions: {
            MicroModState::g => MicroModState::r,
            MicroModState::r => MicroModState::s,
        },
        stretch: MicroModState::g => MicroModState::r,
    }
);

pulse_fn_poincare_t!(
    H_poincare_t : {
        basis: MicroModBasis : MicroModState,
        transitions: {
            MicroModState::g => MicroModState::r,
            MicroModState::r => MicroModState::s,
        },
        stretch: MicroModState::g => MicroModState::r,
    }
);

pulse_fn_poincare_trace!(
    H_poincare_trace : {
        basis: MicroModBasis : MicroModState,
        transitions: {
            MicroModState::g => MicroModState::r,
            MicroModState::r => MicroModState::s,
        },
        stretch: MicroModState::g => MicroModState::r,
    }
);

pulse_fn_poincare_trace_t!(
    H_poincare_trace_t : {
        basis: MicroModBasis : MicroModState,
        transitions: {
            MicroModState::g => MicroModState::r,
            MicroModState::r => MicroModState::s,
        },
        stretch: MicroModState::g => MicroModState::r,
    }
);

/// Targeted keys
/// -------------
/// W: f64 = 5.0
///     Drive strength of the main |g> -> |r> drive in MHz. Returned in us^-1
///     (angular frequency).
/// Wu: f64 = 1.0
///     Drive strength of the modulating |r> -> |s> drive in MHz. Returned in
///     us^-1 (angular frequency).
/// Du: f64 = 1.0
///     Detuning of the modulating |r> -> |s> drive in MHz. Returned in us^-1
///     (angular frequency).
/// N: usize = 10
///     Number of trials to execute for things involving randomized parameters.
/// ND: usize = 101
///     Number of Wu/Du points to test.
/// spectrum: String = "microwave_modulation_spectrum.npz"
///     Input file (.npz format) containing the noise spectrum of the main
///     drive. Returned as a PathBuf.
config_fn!(
    "config.toml", "microwave_modulation" => {
        "W", 5.0, f64
            => W : f64 = |W: f64| -> f64 { 2.0 * PI * W },
        "Wu", 1.0, f64
            => Wu : f64 = |Wu: f64| -> f64 { 2.0 * PI * Wu },
        "Du", 1.0, f64
            => Du : f64 = |Du: f64| -> f64 { 2.0 * PI * Du },
        "N", 10, usize
            => N : usize = |N: usize| -> usize { N },
        "ND", 101, usize
            => ND : usize = |ND: usize| -> usize { ND },
        "spectrum", "microwave_modulation_spectrum.npz".to_string(), String
            => spec_file: PathBuf = |s: String| -> PathBuf { PathBuf::from(s) },
    }
);

