use std::{
    f64::consts::PI,
};
use crate::{
    zm_br_fn,
    mkstate,
    mkbasis,
    pulse_fn_poincare,
    pulse_fn_poincare_t,
    couplings_fn_lindblad,
    config_fn,
    hilbert::{
        *,
    },
    constants::{
        G_3P1_F32,
        G_1S0_F12,
        MU_B,
    },
};

pub const GAMMA: f64 = 182.0e-3; // MHz

pub const MU_N: f64 = MU_B * G_3P1_F32; // MHz G^-1
pub const MU_G: f64 = MU_B * G_1S0_F12; // MHz G^-1
zm_br_fn!(
    zm_br_g0 : {
        -1.10727922e-08,
         3.74955336e-04,
         7.63360649e-09,
         1.94851695e-02,
         1.39214067e-08,
    }
);
zm_br_fn!(
    zm_br_g1 : {
        -1.41632243e-06,
        -3.78301182e-04,
         1.41632838e-06,
         4.72462887e+00,
         5.58055706e+00,
    }
);

pub fn zm(state: NarrowlineState, B: f64) -> f64 {
    return match state {
        NarrowlineState::g0 => zm_br_g0(B),
        NarrowlineState::g1 => zm_br_g1(B),
        NarrowlineState::n0 => MU_N * state.spin().1 * B,
        NarrowlineState::n1 => MU_N * state.spin().1 * B,
        NarrowlineState::n2 => MU_N * state.spin().1 * B,
        NarrowlineState::n3 => MU_N * state.spin().1 * B,
    };
}

mkstate!(
    NarrowlineState : {
        g0 = 0.5, -0.5,
        g1 = 0.5,  0.5,
        n0 = 1.5, -1.5,
        n1 = 1.5, -0.5,
        n2 = 1.5,  0.5,
        n3 = 1.5,  1.5,
    }
);

mkbasis!( NarrowlineBasis[NarrowlineState] );

pulse_fn_poincare!(
    H_poincare : {
        basis: NarrowlineBasis : NarrowlineState,
        transitions: {
            NarrowlineState::g1 => NarrowlineState::n3,
            NarrowlineState::g1 => NarrowlineState::n2,
            NarrowlineState::g1 => NarrowlineState::n1,
            NarrowlineState::g0 => NarrowlineState::n2,
            NarrowlineState::g0 => NarrowlineState::n1,
            NarrowlineState::g0 => NarrowlineState::n0,
        },
        stretch: NarrowlineState::g1 => NarrowlineState::n3,
    }
);

pulse_fn_poincare_t!(
    H_poincare_t : {
        basis: NarrowlineBasis : NarrowlineState,
        transitions: {
            NarrowlineState::g1 => NarrowlineState::n3,
            NarrowlineState::g1 => NarrowlineState::n2,
            NarrowlineState::g1 => NarrowlineState::n1,
            NarrowlineState::g0 => NarrowlineState::n2,
            NarrowlineState::g0 => NarrowlineState::n1,
            NarrowlineState::g0 => NarrowlineState::n0,
        },
        stretch: NarrowlineState::g1 => NarrowlineState::n3,
    }
);

couplings_fn_lindblad!(
    Y_lindblad : {
        basis: NarrowlineBasis : NarrowlineState,
        decay: {
            NarrowlineState::n3 => NarrowlineState::g1,
            NarrowlineState::n2 => NarrowlineState::g1,
            NarrowlineState::n1 => NarrowlineState::g1,
            NarrowlineState::n2 => NarrowlineState::g0,
            NarrowlineState::n1 => NarrowlineState::g0,
            NarrowlineState::n0 => NarrowlineState::g0,
        } = (2.0 * PI * GAMMA)
    }
);

/// Targeted keys
/// -------------
/// W = 5.0_f64
///     Drive strength in MHz. Returned in us^-1 (angular frequency).
/// B = 250.0_f64
///     Magnetic field strength in G.
/// aa = 0.0_f64
///     Polarization angle wrt horizontal in deg. Returned in rad.
/// bb = 0.0_f64
///     Relative phase between the horizontal and vertical components of the
///     incoming light in deg. Returned in rad.
/// th = 90.0_f64
///     Relative angle between the magnetic field and k-vector of the incoming
///     light in deg. Returned in rad.
config_fn!(
    "config.toml", "m3h_imaging" => {
        "W", 5.0, f64
            => W : f64 = |W: f64| -> f64 { 2.0 * PI * W },
        "B", 250.0, f64
            => B : f64 = |B: f64| -> f64 { B },
        "aa", 0.0, f64
            => aa : f64 = |aa: f64| -> f64 { aa * PI / 180.0 },
        "bb", 0.0, f64
            => bb : f64 = |bb: f64| -> f64 { bb * PI / 180.0 },
        "th", 90.0, f64
            => th : f64 = |th: f64| -> f64 { th * PI / 180.0 },
    }
);

