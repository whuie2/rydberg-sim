//! Super-module to contain definitions related to various physical systems.

pub mod rydberg;
pub mod rydberg_generic;
pub mod clock;
pub mod telecom_F32;
pub mod telecom_F12;
pub mod broadline_F12;
pub mod narrowline_F12;
pub mod ground_prep;
pub mod m3h_imaging;
pub mod microwave_modulation;

