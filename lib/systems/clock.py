from __future__ import annotations
import numpy as np
from lib.hilbert import *
from lib import config_fn
from lib.constants import G_3P0_F12, G_1S0_F12, MU_B
from enum import Enum

"""
Functions to generate Hamiltonians for the 171Yb ground(1S0)-clock(3P0)
manifold:

                c0  ======
                            c1  ======


                g0  ======
                            g1  ======

Provides additional functionality for non-trivial time-dependence as well as
couplings for sigma+, sigma-, and pi polarized light with adjustable
polarization impurity. Implicitly applies the rotating wave approximation. All
energies are to be taken in s^-1 (i.e. angular frequency).
"""

class ClockState(State):
    g0 = 0
    g1 = 1
    c0 = 2
    c1 = 3

    def spin(self) -> (float, float):
        if self == ClockState.g0:
            return (1/2, -1/2)
        elif self == ClockState.g1:
            return (1/2, +1/2)
        elif self == ClockState.c0:
            return (1/2, -1/2)
        elif self == ClockState.c1:
            return (1/2, +1/2)

MU_C = MU_B * G_3P0_F12 # MHz G^-1
MU_G = MU_B * G_1S0_F12 # MHz G^-1
_zm_br_g0 = zm_br_fn("_zm_br_g0", "",
    [
        -1.10727922e-08,
        +3.74955336e-04,
        +7.63360649e-09,
        +1.94851695e-02,
        +1.39214067e-08,
    ]
)
_zm_br_g1 = zm_br_fn("_zm_br_g1", "",
    [
        -1.41632243e-06,
        -3.78301182e-04,
        +1.41632838e-06,
        +4.72462887e+00,
        +5.58055706e+00,
    ]
)
_zm_br_c0 = zm_br_fn("_zm_br_c0", "",
    [
        +1.01151916e+02,
        +9.40966754e-05,
        -1.01151915e+02,
        -1.99260954e-06,
        +2.02381457e-09,
    ]
)
_zm_br_c1 = zm_br_fn("_zm_br_c1", "",
    [
        +4.64712454e+00,
        -2.98286729e-04,
        -4.64711085e+00,
        -4.43638992e-05,
        +4.39216548e-08,
    ]
)
def zm(state: ClockState, B: float) -> float:
    if state == ClockState.g0:
        return _zm_br_g0(B)
    elif state == ClockState.g1:
        return _zm_br_g1(B)
    elif state == ClockState.c0:
        return _zm_br_c0(B)
    elif state == ClockState.c1:
        return _zm_br_c1(B)
    else:
        raise Exception("Invalid state")

class PulseType(Enum):
    Sigma_p = 0
    Sigma_m = 1
    Pi = 2

H_sigma_p = pulse_fn(
    name = "H_sigma_p",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a sigma+
    drive.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : float
        Drive frequency relative to the energies defined in `basis`.
    W : float
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : float
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma+ light.
    """,
    principal = (ClockState.g0, ClockState.c1),
    parasitic = [
        (ClockState.g0, ClockState.c0),
        (ClockState.g1, ClockState.c1),
        (ClockState.g1, ClockState.c0),
    ],
)

H_sigma_m = pulse_fn(
    name = "H_sigma_m",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a sigma-
    drive.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : float
        Drive frequency relative to the energies defined in `basis`.
    W : float
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : float
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma- light.
    """,
    principal = (ClockState.g1, ClockState.c0),
    parasitic = [
        (ClockState.g1, ClockState.c1),
        (ClockState.g0, ClockState.c1),
        (ClockState.g0, ClockState.c0),
    ],
)

H_pi = pulse_fn(
    name = "H_pi",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a pi
    drive.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : float
        Drive frequency relative to the energies defined in `basis`.
    W : float
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : float
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for pi light.
    """,
    principal = (ClockState.g1, ClockState.c1),
    parasitic = [
        (ClockState.g1, ClockState.c0),
        (ClockState.g0, ClockState.c1),
        (ClockState.g0, ClockState.c0),
    ],
)

def H_pulse(pulse_type: PulseType, basis: Basis, w, W, chi, t, phi=0.0) \
        -> np.ndarray:
    """
    Thin wrapper around `H_sigma_p`, `H_sigma_m`, or `H_pi`, depending on the
    value of `pulse_type`.
    """
    if pulse_type == PulseType.Sigma_p:
        return H_sigma_p(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Sigma_m:
        return H_sigma_m(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Pi:
        return H_pi(basis, w, W, chi, t, phi)
    else:
        raise Exception

H_poincare = pulse_fn_poincare(
    name = "H_poincare",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for arbitrary
    polarizations of the drive with respect to the quantization axis.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : float
        Drive frequency relative to the energies defined in `basis`.
    W : float
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    psi, chi : float
        Angles (psi for polarization angle relative to the direction
        perpendicular to the quantization axis and chi for ellipticity) that
        generate Stokes parameters and double to give coordinates on the
        Poincare sphere.
    theta : float
        Angle relative to the quantization axis at which light meets the atom.
    t : numpy.ndarray[dim=1, dtype=numpy.float64]
        Time coordinate grid.
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for the specified
        polarization.
    """,
    transitions = [
        (ClockState.g1, ClockState.c1),
        (ClockState.g1, ClockState.c0),
        (ClockState.g0, ClockState.c1),
        (ClockState.g0, ClockState.c0),
    ],
)

H_sigma_p_t = pulse_fn_t(
    name = "H_sigma_p_t",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a sigma+
    drive. Requires time dependence

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.float64]
        Drive frequency relative to the energies defined in `basis`.
    W : numpy.ndarray[dim=1, dtype=numpy.float64]
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : numpy.ndarray[dim=1, dtype=numpy.float64]
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma+ light.
    """,
    principal = (ClockState.g0, ClockState.c1),
    parasitic = [
        (ClockState.g0, ClockState.c0),
        (ClockState.g1, ClockState.c1),
        (ClockState.g1, ClockState.c0),
    ],
)

H_sigma_m_t = pulse_fn_t(
    name = "H_sigma_m_t",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a sigma-
    drive. Requires time dependence.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.float64]
        Drive frequency relative to the energies defined in `basis`.
    W : numpy.ndarray[dim=1, dtype=numpy.float64]
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : numpy.ndarray[dim=1, dtype=numpy.float64]
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for sigma- light.
    """,
    principal = (ClockState.g1, ClockState.c0),
    parasitic = [
        (ClockState.g1, ClockState.c1),
        (ClockState.g0, ClockState.c1),
        (ClockState.g0, ClockState.c0),
    ],
)

H_pi_t = pulse_fn_t(
    name = "H_pi_t",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for a pi
    drive. Requires time dependence.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.float64]
        Drive frequency relative to the energies defined in `basis`.
    W : numpy.ndarray[dim=1, dtype=numpy.float64]
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    chi : numpy.ndarray[dim=1, dtype=numpy.float64]
        Polarization impurity, defined for the range [0, 2/3].
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for pi light.
    """,
    principal = (ClockState.g1, ClockState.c1),
    parasitic = [
        (ClockState.g1, ClockState.c0),
        (ClockState.g0, ClockState.c1),
        (ClockState.g0, ClockState.c0),
    ],
)

def H_pulse_t(pulse_type: PulseType, basis: Basis, w, W, chi, t, phi=0.0) \
        -> np.ndarray:
    """
    Thin wrapper around `H_sigma_p_t`, `H_sigma_m_t`, or `H_pi_t`, depending on
    the value of `pulse_type`.
    """
    if pulse_type == PulseType.Sigma_p:
        return H_sigma_p(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Sigma_m:
        return H_sigma_m(basis, w, W, chi, t, phi)
    elif pulse_type == PulseType.Pi:
        return H_pi(basis, w, W, chi, t, phi)
    else:
        raise Exception

H_poincare_t = pulse_fn_poincare_t(
    name = "H_poincare_t",
    docstring = """
    Construct the Hamiltonian with only off-diagonal elements for arbitrary
    polarizations of the drive with respect to the quantization axis. Requires
    time dependence.

    Parameters
    ----------
    basis: Basis
        Single-atom basis.
    w : numpy.ndarray[dim=1, dtype=numpy.float64]
        Drive frequency relative to the energies defined in `basis`.
    W : numpy.ndarray[dim=1, dtype=numpy.float64]
        Strength of the drive. Defined to correspond to the frequencies of
        oscillations in populations, not amplitudes.
    psi, chi : numpy.ndarray[dim=1, dtype=numpy.float64]
        Angles (psi for polarization angle relative to the direction
        perpendicular to the quantization axis and chi for ellipticity) that
        generate Stokes parameters and double to give coordinates on the
        Poincare sphere.
    theta : numpy.ndarray[dim=1, dtype=numpy.float64]
        Angle relative to the quantization axis at which light meets the atom.
    phi: float (optional)
        Phase shift on the drive. Defaults to zero.

    Returns
    -------
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian with only off-diagonal elements for the specified
        polarization.
    """,
    transitions = [
        (ClockState.g1, ClockState.c1),
        (ClockState.g1, ClockState.c0),
        (ClockState.g0, ClockState.c1),
        (ClockState.g0, ClockState.c0),
    ],
)

# Targeted keys
# -------------
# W : float =  0.1
#   Drive strength in MHz. Returned in us^-1 (angular frequency).
# ND : int = 100
#   Number of points to use on each dimension of any renderable data array,
#   except for time.
# Bmax : float = 200.0
#   Upper end of the magnetic field strength plotting range in G.
load_config, Config = config_fn(
    "config.toml",
    "telecom",
    [
        ("W", 0.1, float, "W", lambda W: 2.0 * np.pi * W),
        ("ND", 100, int, "ND", lambda ND: ND),
        ("Bmax", 200.0, float, "Bmax", lambda B: B),
    ],
)

