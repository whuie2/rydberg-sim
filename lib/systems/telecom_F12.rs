use std::{
    f64::consts::PI,
};
use ndarray as nd;
use num_complex::Complex64 as C64;
use crate::{
    zm_br_fn,
    mkstate,
    mkbasis,
    pulse_fn,
    pulse_fn_t,
    pulse_fn_poincare,
    pulse_fn_poincare_t,
    config_fn,
    error::*,
    hilbert::{
        *,
    },
    constants::{
        G_3D1_F12,
        G_3P0_F12,
        MU_B,
    },
};

pub const MU_T: f64 = MU_B * G_3D1_F12; // MHz G^-1
pub const MU_C: f64 = MU_B * G_3P0_F12; // MHz G^-1
zm_br_fn!(
    zm_br_c0 : {
         1.01151916e+02,
         9.40966754e-05,
        -1.01151915e+02,
        -1.99260954e-06,
         2.02381457e-09,
    }
);
zm_br_fn!(
    zm_br_c1 : {
         4.64712454e+00,
        -2.98286729e-04,
        -4.64711085e+00,
        -4.43638992e-05,
         4.39216548e-08,
    }
);

pub fn zm(state: TelecomState, B: f64) -> f64 {
    return match state {
        TelecomState::c0 => zm_br_c0(B),
        TelecomState::c1 => zm_br_c1(B),
        TelecomState::t0 => MU_T * state.spin().1 * B,
        TelecomState::t1 => MU_T * state.spin().1 * B,
    };
}

mkstate!(
    TelecomState : {
        c0 = 0.5, -0.5,
        c1 = 0.5,  0.5,
        t0 = 0.5, -0.5,
        t1 = 0.5,  0.5,
    }
);

mkbasis!( TelecomBasis[TelecomState] );

pub enum PulseType {
    Sigma_p,
    Sigma_m,
    Pi,
}

pulse_fn!(
    H_sigma_p : {
        basis: TelecomBasis : TelecomState,
        principal:
            TelecomState::c0 => TelecomState::t1,
        parasitic: {
            TelecomState::c0 => TelecomState::t0,
            TelecomState::c1 => TelecomState::t1,
            TelecomState::c1 => TelecomState::t0,
        }
    }
);

pulse_fn!(
    H_sigma_m : {
        basis: TelecomBasis : TelecomState,
        principal:
            TelecomState::c1 => TelecomState::t0,
        parasitic: {
            TelecomState::c1 => TelecomState::t1,
            TelecomState::c0 => TelecomState::t1,
            TelecomState::c0 => TelecomState::t0,
        }
    }
);

pulse_fn!(
    H_pi : {
        basis: TelecomBasis : TelecomState,
        principal:
            TelecomState::c1 => TelecomState::t1,
        parasitic: {
            TelecomState::c1 => TelecomState::t0,
            TelecomState::c0 => TelecomState::t1,
            TelecomState::c0 => TelecomState::t0,
        }
    }
);

pub fn H_pulse(pulse_type: PulseType, basis: TelecomBasis, w: f64, W: f64,
        chi: f64, t: nd::ArrayView1<f64>, phi: Option<f64>)
    -> HilbertResult<nd::Array3<C64>>
{
    return match pulse_type {
        PulseType::Sigma_p
            => H_sigma_p(basis, w, W, chi, t, phi.unwrap_or(0.0)),
        PulseType::Sigma_m
            => H_sigma_m(basis, w, W, chi, t, phi.unwrap_or(0.0)),
        PulseType::Pi
            => H_pi(basis, w, W, chi, t, phi.unwrap_or(0.0)),
    };
}

pulse_fn_poincare!(
    H_poincare : {
        basis: TelecomBasis : TelecomState,
        transitions: {
            TelecomState::c1 => TelecomState::t1,
            TelecomState::c1 => TelecomState::t0,
            TelecomState::c0 => TelecomState::t1,
            TelecomState::c0 => TelecomState::t0,
        },
        stretch: TelecomState::c0 => TelecomState::t1,
    }
);

pulse_fn_t!(
    H_sigma_p_t : {
        basis: TelecomBasis : TelecomState,
        principal:
            TelecomState::c0 => TelecomState::t1,
        parasitic: {
            TelecomState::c0 => TelecomState::t0,
            TelecomState::c1 => TelecomState::t1,
            TelecomState::c1 => TelecomState::t0,
        }
    }
);

pulse_fn_t!(
    H_sigma_m_t : {
        basis: TelecomBasis : TelecomState,
        principal:
            TelecomState::c1 => TelecomState::t0,
        parasitic: {
            TelecomState::c1 => TelecomState::t1,
            TelecomState::c0 => TelecomState::t1,
            TelecomState::c0 => TelecomState::t0,
        }
    }
);

pulse_fn_t!(
    H_pi_t : {
        basis: TelecomBasis : TelecomState,
        principal:
            TelecomState::c1 => TelecomState::t1,
        parasitic: {
            TelecomState::c1 => TelecomState::t0,
            TelecomState::c0 => TelecomState::t1,
            TelecomState::c0 => TelecomState::t0,
        }
    }
);

pub fn H_pulse_t(pulse_type: PulseType, basis: TelecomBasis,
                 w: nd::ArrayView1<f64>, W: nd::ArrayView1<f64>,
                 chi: nd::ArrayView1<f64>, t: nd::ArrayView1<f64>,
                 phi: Option<f64>)
    -> HilbertResult<nd::Array3<C64>>
{
    return match pulse_type {
        PulseType::Sigma_p
            => H_sigma_p_t(basis, w, W, chi, t, phi.unwrap_or(0.0)),
        PulseType::Sigma_m
            => H_sigma_m_t(basis, w, W, chi, t, phi.unwrap_or(0.0)),
        PulseType::Pi
            => H_pi_t(basis, w, W, chi, t, phi.unwrap_or(0.0)),
    };
}

pulse_fn_poincare_t!(
    H_poincare_t : {
        basis: TelecomBasis : TelecomState,
        transitions: {
            TelecomState::c1 => TelecomState::t1,
            TelecomState::c1 => TelecomState::t0,
            TelecomState::c0 => TelecomState::t1,
            TelecomState::c0 => TelecomState::t0,
        },
        stretch: TelecomState::c0 => TelecomState::t1,
    }
);

/// Targeted keys
/// -------------
/// W = 5.0_f64
///     Drive strength in MHz. Returned in us^-1 (angular frequency).
/// Weff = 1.0_f64
///     Targeted effective Rabi frequency in MHz. Returned in us^-1 (angular
///     frequency).
/// B = 200.0_f64
///     Magnetic field strength in G.
/// ND = 100_usize
///     Number of points to use on each dimension of any renderable data array,
///     except for time.
config_fn!(
    "config.toml", "telecom" => {
        "W", 5.0, f64
            => W : f64 = |W: f64| -> f64 { 2.0 * PI * W },
        "Weff", 1.0, f64
            => Weff : f64 = |W: f64| -> f64 { 2.0 * PI * W },
        "B", 3.5, f64
            => B : f64 = |B: f64| -> f64 { B },
        "ND", 100, i64
            => ND : usize = |ND: i64| -> usize { ND as usize }
    }
);

