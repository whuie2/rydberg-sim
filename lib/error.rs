//! Provides simple error types to organize the possible errors that can occur.

pub trait ErrMsg {
    fn msg(&self) -> &'static str;
}

#[macro_export]
macro_rules! mkerr {
    ( $name:ident : {$( $var:ident => $msg:literal ),*} ) => {
        #[derive(Clone, Copy, Debug)]
        pub enum $name {
            $( $var, )*
        }

        impl ErrMsg for $name {
            fn msg(&self) -> &'static str {
                return match *self {
                    $( $name::$var => $msg, )*
                }
            }
        }

        impl std::fmt::Display for $name {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                return f.write_str(self.msg());
            }
        }

        impl std::error::Error for $name { }
    }
}

mkerr!(
    RabiError : {
        Generic => "Unspecified Rabi error occurred",
        Eigen => "Encountered a diagonalization error",
        Linalg => "Encountered a lingalg error"
    }
);

mkerr!(
    PerlinError : {
        Generic => "Unspecified Perlin error occurred"
    }
);

mkerr!(
    HilbertError : {
        Generic => "Unspecified Hilbert error occurred",
        NotInBasis => "Specified state is not a member of this basis",
        NonEqNProdState => "Encountered product states of unequal length",
        NonSquareHamiltonian => "Encountered non-square Hamiltonian",
        NonEqSizeHamiltonian => "Encountered Hamiltonians of unequal sizes",
        NonEqTimeDep => "Encountered non-equivalent time dependences",
        EmptyPer => "Encountered specification for zero Hamiltonians"
    }
);

mkerr!(
    RydbergError : {
        Generic => "Unspecified Rydberg error occurred"
    }
);

mkerr!(
    ClockError : {
        Generic => "Unspecified clock error occurred"
    }
);

mkerr!(
    TelecomError : {
        Generic => "Unspecified telecom error occurred"
    }
);

pub type RabiResult<T> = Result<T, RabiError>;
pub type PerlinResult<T> = Result<T, PerlinError>;
pub type HilbertResult<T> = Result<T, HilbertError>;
pub type RydbergResult<T> = Result<T, RydbergError>;
pub type ClockResult<T> = Result<T, ClockError>;
pub type TelecomResult<T> = Result<T, TelecomError>;

