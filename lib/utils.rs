//! Various utilities related to computation of FFT-related quantities and very
//! simple peak-finding. Also provides macros for succinct parameter-scanning
//! operations as well as simple file I/O.

use std::{
    f64::consts::PI,
};
use ndarray as nd;
use rand::{
    prelude as rnd,
    Rng,
};
use rustfft as fft;
use num_complex::Complex64 as C64;

/// Comparison function for `f64`, since `f64` doesn't implement `Ord`.
pub fn float_iter_cmp(a: &&f64, b: &&f64) -> std::cmp::Ordering {
    return a.total_cmp(b);
    // return a.partial_cmp(b).unwrap();
}

/// Return an array containing frequency-domain coordinates associated with a
/// signal of `N` points with spacing `dt`.
pub fn fft_freq(N: usize, dt: f64) -> nd::Array1<f64> {
    return if N % 2 == 0 {
        let fp: Vec<f64>
            = (0..N / 2)
            .map(|k| k as f64 / (N as f64 * dt))
            .collect();
        let fm: Vec<f64>
            = (1..N / 2 + 1).rev()
            .map(|k| -(k as f64) / (N as f64 * dt))
            .collect();
        nd::Array::from([fp, fm].concat())
    } else {
        let fp: Vec<f64>
            = (0..(N + 1) / 2)
            .map(|k| k as f64 / (N as f64 * dt))
            .collect();
        let fm: Vec<f64>
            = (1..(N + 1) / 2).rev()
            .map(|k| -(k as f64) / (N as f64 * dt))
            .collect();
        nd::Array::from([fp, fm].concat())
    };
}

/// Perform the complex-valued FFT, returning both the FFT result and an array
/// of frequency-domain coordinates.
pub fn do_fft(x: &nd::Array1<C64>, dt: f64)
    -> (nd::Array1<C64>, nd::Array1<f64>)
{
    let N: usize = x.len();
    let mut buf: Vec<C64> = x.to_vec();
    let mut plan = fft::FftPlanner::new();
    let fft_plan = plan.plan_fft_forward(N);
    fft_plan.process(&mut buf);
    return (
        nd::Array::from(buf),
        fft_freq(N, dt),
    );
}

/// Return an array containing time-domain coordinates associated with a
/// frequency-domain signal of `N` points with spacing `df`.
pub fn ifft_time(N: usize, df: f64) -> nd::Array1<f64> {
    return nd::Array1::range(
        0.0,
        1.0 / df,
        1.0 / (N as f64 * df),
    );
}

/// Perform the complex-valued invert FFT, returning both the iFFT and an array
/// of time-domain coordinates.
pub fn do_ifft(F: &nd::Array1<C64>, df: f64)
    -> (nd::Array1<C64>, nd::Array1<f64>)
{
    let N: usize = F.len();
    let mut buf: Vec<C64> = F.to_vec();
    let mut plan = fft::FftPlanner::new();
    let ifft_plan = plan.plan_fft_inverse(N);
    ifft_plan.process(&mut buf);
    return (
        nd::Array::from(buf),
        ifft_time(N, df),
    );
}

/// Generate an array of `N` complex phase factors uniformly sampled from `[0,
/// 2π)`.
pub fn random_phases<R>(N: usize, rng: &mut R) -> nd::Array1<C64>
where R: ?Sized + Rng
{
    return (0..N)
        .map(|_| (C64::i() * 2.0 * PI * rng.gen::<f64>()).exp())
        .collect();
}

/// Inject phase noise into a frequency-domain signal characterized by PSD `S`
/// with frequency-domain coordinates `f`, and compute the resulting real-valued
/// signal in the time-domain, sampled at time coordinates `t`.
///
/// The injected noise is of the form of random phases uniformly sampled from
/// `[0, 2π)`.
pub fn do_ifft_real_noise(
    S: &nd::Array1<f64>,
    f: &nd::Array1<f64>,
    t: &nd::Array1<f64>
) -> nd::Array1<f64>
{
    let df: f64 = (f[1] - f[0]).abs();
    let mut x: nd::Array1<f64> = nd::Array1::zeros(t.raw_dim());
    let mut rng: rnd::ThreadRng = rnd::thread_rng();
    let phase_shift: nd::Array1<f64>
        = (0..f.len()).map(|_| 2.0 * PI * rng.gen::<f64>()).collect();
    for ((Sk, fk), phk) in S.iter().zip(f.iter()).zip(phase_shift.iter()) {
        x += &(
            (2.0 * *Sk * df).sqrt()
            * (2.0 * PI * *fk * t + *phk).mapv(f64::cos)
        );
    }
    return x;
}

/// Scan over the array `y` and return the index and value of the first point at
/// which it begins to decrease.
pub fn find_first_max(y: &nd::Array1<f64>) -> (usize, f64) {
    let mut y0: f64 = y[0];
    let mut k0: usize = 0;
    for (k, yk) in y.iter().enumerate() {
        if yk < &y0 {
            break;
        } else {
            y0 = *yk;
            k0 = k;
        }
    }
    return (k0, y0);
}

/// Generate a function to read values from a subtable of a TOML-formatted file
/// into a struct. This macro can be invoked at most once per scope.
///
/// Each struct member / field to be read is associated with a function through
/// which the value read from the file is passed before being stored in the
/// struct.
///
/// This macro expands into one struct definition and one function definition of
/// the forms:
/// ```ignore
/// #[derive(Clone, Debug)]
/// pub struct Config {
///     /* member variables are all `pub` */
/// }
///
/// pub fn load_config(infile: Option<std::path::PathBuf>) -> Config {
///     /* reads from `infile`, defaulting to the $file parameter of the macro */
/// }
/// ```
#[macro_export]
macro_rules! config_fn {
    (
        $file:expr, $subtab:expr => {
            $( $key:expr, $def:expr, $intype:ty
                => $field:ident : $outtype:ty = $foo:expr ),+ $(,)?
        }
    ) => {
        #[derive(Clone, Debug)]
        pub struct Config {
            $(
                pub $field: $outtype,
            )+
        }

        pub fn load_config(infile: Option<std::path::PathBuf>) -> Config {
            let infile_: std::path::PathBuf
                = infile.unwrap_or(std::path::PathBuf::from($file));
            let table: toml::Value
                = std::fs::read_to_string(infile_.clone())
                .expect(
                    format!("Couldn't read config file {:?}", infile_)
                    .as_str()
                )
                .parse::<toml::Value>()
                .expect(
                    format!("Couldn't parse config file {:?}", infile_)
                    .as_str()
                );
            let mut config = Config {
                $(
                    $field: ($foo)($def),
                )+
            };
            if let Some(value) = table.get($subtab) {
                $(
                    if let Some(X) = value.get($key) {
                        let x: $intype
                            = X.clone().try_into()
                            .expect(format!(
                                "Couldn't coerce type for key {:?}", $key)
                                .as_str()
                            );
                        config.$field = ($foo)(x);
                    }
                )+
            }
            return config;
        }
    }
}

/// Handles repeated calls with varied inputs to a closure outputting some
/// number of values, storing them in `ndarray::Array`s of appropriate shape.
///
/// Suppose we have some function $`f`$ that is defined over $`N`$ input
/// variables and returns $`M`$ output values. Given $`N`$ arrays of lengths
/// $`n_k`$ ($`k = 0, \dots, N - 1`$) sampling values over the $`N`$ variables,
/// this macro calls $`f`$ on each element of the Cartesian product of all $`N`$
/// arrays and collects the outputs of $`f`$ into $`M`$ arrays whose $`k`$-th
/// axis is associated with the $`k`$-th input array.
///
/// When calling this macro, the function `$caller` should take a single
/// `Vec<usize>` of indices, which should be used in the function body to refer
/// to input values in the sampling arrays, which in turn should be defined
/// beforehand, outside the macro. Output arrays must be returned as `IxDyn`.
///
/// # Example
/// ```ignore
/// # extern crate ndarray;
/// # fn main() {
/// use ndarray::{ Array1, Array3, ArrayD, array };
/// use smorgasbord::{ loop_call };
///
/// let var1: Array1<f64> = array![0.5, 1.0, 2.0];
/// let var2: Array1<i32> = array![-1, 0, 1, 2];
/// let var3: Array1<bool> = array![true, false];
///
/// let caller
///     = |Q: Vec<usize>| -> (f64, f64) {
///         let val: f64 = var1[Q[0]].powi(var2[Q[1]]);
///         if var3[Q[2]] {
///             (val, 2.0 * val)
///         } else {
///             (-val, -2.0 * val)
///         }
///     };
/// let (ret1, ret2): (ArrayD<f64>, ArrayD<f64>)
///     = loop_call!(
///         caller => ( ret1: f64, ret2: f64 ),
///         vars: { var1, var2, var3 }
///     );
///
/// let ret1: Array3<f64> = ret1.into_dimensionality().unwrap();
/// let ret2: Array3<f64> = ret2.into_dimensionality().unwrap();
///
/// assert_eq!(
///     ret1,
///     array![
///         [
///             [ 2.00, -2.00 ],
///             [ 1.00, -1.00 ],
///             [ 0.50, -0.50 ],
///             [ 0.25, -0.25 ],
///         ],
///         [
///             [ 1.00, -1.00 ],
///             [ 1.00, -1.00 ],
///             [ 1.00, -1.00 ],
///             [ 1.00, -1.00 ],
///         ],
///         [
///             [ 0.50, -0.50 ],
///             [ 1.00, -1.00 ],
///             [ 2.00, -2.00 ],
///             [ 4.00, -4.00 ],
///         ],
///     ]
/// );
/// assert_eq!(
///     ret2,
///     array![
///         [
///             [ 4.00, -4.00 ],
///             [ 2.00, -2.00 ],
///             [ 1.00, -1.00 ],
///             [ 0.50, -0.50 ],
///         ],
///         [
///             [ 2.00, -2.00 ],
///             [ 2.00, -2.00 ],
///             [ 2.00, -2.00 ],
///             [ 2.00, -2.00 ],
///         ],
///         [
///             [ 1.00, -1.00 ],
///             [ 2.00, -2.00 ],
///             [ 4.00, -4.00 ],
///             [ 8.00, -8.00 ],
///         ],
///     ]
/// );
/// # }
/// ```
#[macro_export]
macro_rules! loop_call {
    (
        $caller:ident => ( $( $rvar:ident: $rtype:ty ),+ $(,)? ),
        vars: { $( $var:ident ),+ $(,)? } $(,)?
    ) => {
        loop_call!(
            $caller => ( $( $rvar: $rtype ),+ ),
            vars: { $( $var ),+ },
            printflag: true,
            lspace: 2
        )
    };
    (
        $caller:ident => ( $( $rvar:ident: $rtype:ty ),+ $(,)? ),
        vars: { $( $var:ident ),+ $(,)? },
        printflag: $printflag:expr $(,)?
    ) => {
        loop_call!(
            $caller => ( $( $rvar: $rtype ),+ ),
            vars: { $( $var ),+ },
            printflag: $printflag,
            lspace: 2
        )
    };
    (
        $caller:ident => ( $( $rvar:ident: $rtype:ty ),+ $(,)? ),
        vars: { $( $var:ident ),+ $(,)? },
        printflag: $printflag:expr,
        lspace: $lspace:expr $(,)?
    ) => {
        {
            let _caller_ = &$caller;
            let _Nvals_: Vec<usize> = vec![ $( $var.len() ),+ ];
            let _nvars_: usize = _Nvals_.len();
            let _Z_: Vec<usize>
                = _Nvals_.iter()
                .map(|n| (*n as f64).log10().floor() as usize + 1)
                .collect();
            let _tot_: usize = _Nvals_.iter().product();
            let mut _NN_: Vec<usize>
                = (1.._nvars_).rev()
                .map(|k| _Nvals_[_nvars_ - k.._nvars_].iter().product())
                .collect();
            _NN_.push(1);

            let _mk_outstr_ = |Q: Vec<usize>, last: bool| -> String {
                let mut outstr_items: Vec<String>
                    = Vec::with_capacity(_nvars_ + 3);
                outstr_items.push(" ".repeat($lspace));
                for (k, idx) in Q.iter().enumerate() {
                    outstr_items.push(
                        format!("{0:2$}/{1:2$};  ",
                            idx + !last as usize, _Nvals_[k], _Z_[k]));
                }
                outstr_items.push(
                    format!(
                        "[{:6.2}%] \r",
                        100.0 * (
                            (0.._nvars_)
                                .map(|k| (Q[k] - last as usize) * _NN_[k])
                                .sum::<usize>() as f64
                            + last as usize as f64
                        ) / _tot_ as f64,
                    )
                );
                outstr_items.iter().flat_map(|s| s.chars()).collect()
            };

            let _input_idx_
                = itertools::Itertools::multi_cartesian_product(
                    _Nvals_.iter().map(|n| 0..*n)
                );
            let mut _outputs_: Vec<( $( $rtype ),+ ,)>
                = Vec::with_capacity(_tot_);
            let _t0_: std::time::Instant = std::time::Instant::now();
            for Q in _input_idx_ {
                if $printflag {
                    print!("{}", _mk_outstr_(Q.clone(), false));
                    std::io::Write::flush(&mut std::io::stdout()).unwrap();
                }
                _outputs_.push($caller(Q.clone()));
            }
            let _dt_: std::time::Duration = std::time::Instant::now() - _t0_;
            if $printflag {
                println!("{}", _mk_outstr_(_Nvals_.clone(), true));
                std::io::Write::flush(&mut std::io::stdout()).unwrap();
                println!(
                    "{}total time elapsed: {:.3} s",
                    " ".repeat($lspace),
                    _dt_.as_secs_f32(),
                );
                println!(
                    "{}average time per call: {:.3} s",
                    " ".repeat($lspace),
                    _dt_.as_secs_f32() / _tot_ as f32,
                );
            }

            let ( $( $rvar ),+ ,): ( $( Vec<$rtype> ),+ ,)
                = itertools::Itertools::multiunzip(_outputs_.into_iter());
            (
                $(
                    ndarray::Array::from_vec($rvar)
                        .into_shape(_Nvals_.as_slice())
                        .expect("couldn't reshape")
                ),+
            ,)
        }
    };
}

/// Writes a set of arrays to a file using [`ndarray_npy::NpzWriter`].
///
/// The file must be specified as a [`std::path::PathBuf`], the keys must be
/// (coercible to ) `&str`, and the arrays must be passed as references.
#[macro_export]
macro_rules! write_npz {
    (
        $file_pathbuf:expr,
        arrays: { $( $key_str:expr => $arrayref:expr ),+ $(,)? }
    ) => {
        {
            let mut _output_filepath_ = $file_pathbuf.clone();
            let mut _output_writer_
                = ndarray_npy::NpzWriter::new(
                    std::fs::File::create(_output_filepath_.to_str().unwrap())
                        .expect(
                            format!(
                                "Couldn't create npz file '{:?}'",
                                _output_filepath_
                            ).as_str()
                        )
                );
            $(
                _output_writer_.add_array($key_str, $arrayref)
                    .expect(
                        format!(
                            "Couldn't add array under key '{}' to file",
                            $key_str
                        ).as_str()
                    );
            )+
            _output_writer_.finish()
                .expect("Couldn't write out data file");
        }
    }
}

/// Reads arrays from a file using [`ndarray_npy::NpzReader`].
///
/// This macro can either return the reader object as is, or attempt pull data
/// for a specific set of keys from it, discarding the reader afterward.
#[macro_export]
macro_rules! read_npz {
    ( $file_pathbuf:expr ) => {
        {
            let _input_filepath_ = $file_pathbuf.clone();
            ndarray_npy::NpzReader::new(
                    std::fs::File::open($file_pathbuf.clone())
                        .expect(
                            format!(
                                "Couldn't open npz file '{:?}'",
                                _input_filepath_
                            ).as_str()
                        )
                ).expect(
                    format!(
                        "Couldn't read npz file '{:?}'",
                        _input_filepath_
                    ).as_str()
                )
        }
    };
    (
        $file_pathbuf:expr,
        arrays: { $( $key_str:expr ),+ $(,)? }
    ) => {
        {
            let mut _input_reader_ = read_npz!($file_pathbuf);
            ( $(
                _input_reader_.by_name($key_str)
                    .expect(
                        format!(
                            "Couldn't retrieve array for key '{:?}'",
                            $key_str
                        ).as_str()
                    ),
            )+ )
        }
    }
}

