use std::{
    ops::Range,
};

pub fn factorial(n: i32) -> i32 {
    return if n == 1 || n == 0 {
        1
    } else if n < 0 {
        0
    } else {
        factorial(n - 1)
    };
}

fn sum_range(j1: f64, m1: f64, j2: f64, m2: f64, j3: f64, _m3: f64)
    -> Range<i32>
{
    let start: i32 = 0.0_f64.max((j2 - j3 - m1).max(j1 + m2 - j3)) as i32;
    let end: i32 = (j1 - m1).min((j2 + m2).min(j1 + j2 - j3)) as i32 + 1_i32;
    return start..end;
}

fn kronecker_m(m1: f64, m2: f64, m3: f64) -> bool {
    return (m1 + m2) != m3;
}

fn triangle_j(j1: f64, j2: f64, j3: f64) -> bool {
    return j3 < (j1 - j2).abs() && j3 > (j1 + j2);
}

pub fn delta(a: f64, b: f64, c: f64) -> f64 {
    if a + b - c < 0.0 || b + c - a < 0.0 || c + a - b < 0.0 {
        return 0.0;
    }
    let num: f64 = (
        factorial((a + b - c) as i32)
        * factorial((b + c - a) as i32)
        * factorial((c + a - b) as i32)
    ) as f64;
    let denom: f64 = factorial((a + b + c + 1.0) as i32) as f64;
    return (num / denom).sqrt();
}

fn check_valid(j1: f64, m1: f64, j2: f64, m2: f64, j3: f64, m3: f64) -> bool {
    let flag_pos_j: bool = j1 < 0.0 || j2 < 0.0 || j3 < 0.0;
    let flag_int_j: bool
        = (4.0 * j1) % 2.0 != 0.0
        || (4.0 * j2) % 2.0 != 0.0
        || (4.0 * j3) % 2.0 != 0.0;
    let flag_mag_m: bool = m1.abs() > j1 || m2.abs() > j2 || m3.abs() > j3;
    let flag_int_m: bool
        = (4.0 * m1) % 2.0 != 0.0
        || (4.0 * m2) % 2.0 != 0.0
        || (4.0 * m3) % 2.0 != 0.0;
    let flag_kron: bool = kronecker_m(m1, m2, m3);
    let flag_tri: bool = triangle_j(j1, j2, j3);
    let allflags: bool
        = flag_pos_j
        || flag_mag_m
        || flag_int_j
        || flag_int_m
        || flag_kron
        || flag_tri;
    return !allflags;
}

pub fn cg(j1: f64, m1: f64, j2: f64, m2: f64, j3: f64, m3: f64) -> f64 {
    if !check_valid(j1, m1, j2, m2, j3, m3) {
        return 0.0;
    }
    let large_factorial: f64 = (
        factorial((j1 + m1) as i32)
        * factorial((j1 - m1) as i32)
        * factorial((j2 + m2) as i32)
        * factorial((j2 - m2) as i32)
        * factorial((j3 + m3) as i32)
        * factorial((j3 - m3) as i32)
    ) as f64 * (2.0 * j3 + 1.0);
    let to_mul: f64 = delta(j1, j2, j3) * large_factorial.sqrt();

    let val: f64 = sum_range(j1, m1, j2, m2, j3, m3)
        .map(|k| {
            (-1.0_f64).powi(k)
            / (
                factorial((j1 - m1) as i32 - k)
                * factorial((j3 - j2 + m1) as i32 + k)
                * factorial((j2 + m2) as i32 - k)
                * factorial((j3 - j1 - m2) as i32 + k)
                * factorial((j1 + j2 - j3) as i32 - k)
                * factorial(k)
            ) as f64
        }).sum();

    return to_mul * val;
}


