import numpy as np
import numpy.linalg as la
import numpy.random as rand

### USE UNITS WHERE HBAR = 1 ###

def outer_prod(a: np.ndarray, b: np.ndarray) -> np.ndarray:
    """
    Compute the outer product of two 1D arrays,
    (a \\otimes b)_{i,j} = a_i * (b^*)_j

    Parameters
    ----------
    a, b : numpy.ndarray[dim=1, dtype=numpy.complex128]

    Returns
    -------
    res : numpy.ndarray[dim=2, dtype=numpy.complex128]
    """
    return np.ndarray([
        [a[i] * b[j].conj() for j in range(b.shape[0])]
        for i in range(a.shape[0])
    ])

def commutator(A: np.ndarray, B: np.ndarray) -> np.ndarray:
    """
    Compute [A, B] = A*B - B*A.
    """
    return A @ B - B @ A

def anti_commutator(A: np.ndarray, B: np.ndarray) -> np.ndarray:
    """
    Compute {A, B} = A*B + B*A.
    """
    return A @ B + B @ A

def lindbladian(Y: np.ndarray, rho: np.ndarray) -> np.ndarray:
    """
    Compute the Lindbladian terms (those relevant to spontaneous decay) of the
    master equation.
    """
    n = Y.shape[0]
    L = np.zeros(Y.shape, dtype=np.complex128)
    for k, y in Y.flatten():
        if y <= np.finfo(np.float64).eps:
            continue
        a = k // n
        b = k % n
        M = np.array([
            [
                y * (
                    (rho[b, b] if i == a and j == a else 0.0j)
                    - (rho[i, j] / 2.0 if i == b else 0.0j)
                    - (rho[i, j] / 2.0 if j == b else 0.0j)
                )
                for j in range(n)
            ]
            for i in range(n)
        ])
        L += M
    return L

def trace(A: np.ndarray) -> np.complex128:
    """
    Compute Tr(A) = \sum_i A_{i,i}.
    """
    return sum(A[i, i] for i in range(A.shape[0]))

def eigen_evolve(psi0: np.ndarray, H: np.ndarray, t: np.ndarray) -> np.ndarray:
    """
    Compute the time-dependent behavior of an initial state by diagonalizing the
    Hamiltonian. Time-independent Hamiltonians only.

    Parameters
    ----------
    psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Initial state.
    H : numpy.ndarray[dim=2, dtype=numpy.complex128]
        Hamiltonian (s^-1). Must be square, of shape compatible with `psi0`.
    t : numpy.ndarray[dim=1, dtype=numpy.float64]
        Array of evenly spaced time gridpoints.

    Returns
    -------
    psi : numpy.ndarray[dim=2, dtype=numpy.complex128]
        `psi0` evolved through time according to `H`. Has shape (I, J) where I
        is equal to the length of `psi0` and J is equal to the length of `t`.
    """
    E, V = la.eigh(H)
    c = la.solve(V, psi0)
    psi = np.array([
        V @ (c * np.exp(-1j * E * ti)) for ti in t
    ]).T
    return psi

def eigen_evolve_t(psi0: np.ndarray, H: np.ndarray, t: np.ndarray) \
        -> np.ndarray:
    """
    Compute the time-dependent behavior of an initial state by diagonalizing the
    Hamiltonian at each time step.

    Paramters
    ---------
    psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Initial state.
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian (in s^-1). The first two indices must correspond to states
        (and be compatible with `psi0`) and the last must correspond to time
        (and be compatible with `t`).
    t : numpy.ndarray[dim=1, dtype=numpy.float64]
        Array of evenly spaced time gridpoints.

    Returns
    -------
    psi : numpy.ndarray[dim=2, dtype=numpy.complex128]
        `psi0` evolved through time according to `H`. Has shape (I, J) where I
        is equal to the length of `psi0` and J is equal to the length of `t`.
    """
    dt = abs(t[1] - t[0])
    psi = np.zeros((psi0.shape[0], t.shape[0]), dtype=np.complex128)
    psi[:, 0] = psi0
    for k in range(t.shape[0] - 1):
        E, V = la.eigh(H[:, :, k])
        c = la.solve(V, psi[:, k])
        psi[:, k + 1] = V @ (c * np.exp(-1j * E * dt))
    return psi

def schrodinger_evolve(psi0: np.ndarray, H: np.ndarray, t: np.ndarray) \
        -> np.ndarray:
    """
    Compute the time-dependent behavior of an initial state by numerically
    integrating the time-dependent Schrodinger equation using a simple central
    finite difference scheme (Error = O(dt^2)).

    Parameters
    ----------
    psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Initial state.
    H : numpy.ndarray[dim=2, dtype=numpy.complex128]
        Hamiltonian (in s^-1). Must be square, of shape compatible with `psi0`.
    t : numpy.ndarray[dim=1, dtype=numpy.float64]
        Array of evenly spaced time gridpoints.

    Returns
    -------
    psi : numpy.ndarray[dim=2, dtype=numpy.complex128]
        `psi0` evolved through time according to `H`. Has shape (I, J) where I
        is equal to the length of `psi0` and J is equal to the length of `t`.
    """
    dt = abs(t[1] - t[0])
    psi = np.zeros((psi0.shape[0], t.shape[0]), dtype=np.complex128)
    psi[:, 0] = psi0
    psi[:, 1] = psi0
    for k in range(1, t.shape[0] - 1):
        psi[:, k + 1] = psi[:, k - 1] \
                - 2* dt * 1j * (H @ psi[:, k])
        psi[:, k + 1] /= np.sqrt(np.sum(np.abs(psi[:, k + 1])**2))
    return psi

def schrodinger_evolve_t(psi0: np.ndarray, H: np.ndarray, t: np.ndarray) \
        -> np.ndarray:
    """
    Compute the time-dependent behavior of an initial state for a time-dependent
    Hamiltonian by numerically integrating the time-dependent Schrodinger
    equation using a simple central finite difference scheme (Error = O(dt^2)).

    Parameters
    ----------
    psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Initial state.
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian (in s^-1). The first two indices must correspond to states
        (and be compatible with `psi0`) and the last must correspond to time
        (and be compatible with `t`).
    t : numpy.ndarray[dim=1, dtype=numpy.float64]
        Array of evenly spaced time gridpoints.

    Returns
    -------
    psi : numpy.ndarray[dim=2, dtype=numpy.complex128]
        `psi0` evolved through time according to `H`. Has shape (I, J) where I
        is equal to the length of `psi0` and J is equal to the length of `t`.
    """
    dt = abs(t[1] - t[0])
    psi = np.zeros((psi0.shape[0], t.shape[0]), dtype=np.complex128)
    psi[:, 0] = psi0
    psi[:, 1] = psi0
    for k in range(1, t.shape[0] - 1):
        psi[:, k + 1] = psi[:, k - 1] \
                - 2 * dt * 1j * (H[:, :, k] @ psi[:, k])
        psi[:, k + 1] /= np.sqrt(np.sum(np.abs(psi[:, k + 1])**2))
    return psi

def schrodinger_evolve_rk4(psi0: np.ndarray, H: np.ndarray, t: np.ndarray) \
        -> np.ndarray:
    """
    Compute the time-dependent behavior of an initial state for a time-dependent
    Hamiltonian by numerically integrating the time-dependent Schrodinger
    equation using fourth-order Runge-Kutta (Error = O(dt^5)).

    Parameters
    ----------
    psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Initial state.
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian (in s^-1). The first two indices must correspond to states
        (and be compatible with `psi0`) and the last must correspond to time
        (and be compatible with `t`).
    t : numpy.ndarray[dim=1, dtype=numpy.float64]
        Array of evenly spaced time gridpoints.

    Returns
    -------
    psi : numpy.ndarray[dim=2, dtype=numpy.complex128]
        `psi0` evolved through time according to `H`. Has shape (I, J) where I
        is equal to the length of `psi0` and J is equal to the length of `t`.
    """
    dt = abs(t[1] - t[0])
    psi = np.zeros((psi0.shape[0], t.shape[0]), dtype=np.complex128)
    psi[:, 0] = psi0
    psi[:, 1] = psi0
    rhs = lambda H, p: -1j * H @ p
    for k in range(t.shape[0] - 2):
        psi_old = psi[:, k]

        phi1 = rhs(H[:, :, k], psi_old)
        phi2 = rhs(H[:, :, k + 1], psi_old + dt * phi1)
        phi3 = rhs(H[:, :, k + 1], psi_old + dt * phi2)
        phi4 = rhs(H[:, :, k + 2], psi_old + 2 * dt * phi3)

        psi_new = psi_old + (dt / 3) * (phi1 + 2 * phi2 + 2 * phi3 + phi4)
        N = np.sqrt(np.sum(np.abs(psi_new)**2))

        psi[:, k + 2] = psi_new / N
    return psi

def liouville_evolve(psi0: np.ndarray, H: np.ndarray, t: np.ndarray) \
        -> np.ndarray:
    """
    Compute the time-dependent behavior of an initial state by numerically
    integrating the Liouville equation using a simple central finite difference
    scheme (Error = O(dt^2)).

    Parameters
    ----------
    psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Initial state.
    H : numpy.ndarray[dim=2, dtype=numpy.complex128]
        Hamiltonian (in s^-1). Must be square, of shape compatible with `psi0`.
    t : numpy.ndarray[dim=1, dtype=numpy.float64]
        Array of evenly spaced time gridpoints.

    Returns
    -------
    rho : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Density matrix of `psi0` evolved through time according to `H`. Has
        shape (I, J, K) where I and J are equal to the length of `psi0` and K is
        equal to the length of `t`.
    """
    dt = abs(t[1] - t[0])
    rho0 = np.outer(psi0, psi0.conj())
    rho = np.zeros(
        (rho0.shape[0], rho0.shape[1], t.shape[0]),
        dtype=np.complex128
    )
    rho[:, :, 0] = rho0
    rho[:, :, 1] = rho0
    for k in range(1, t.shape[0] - 1):
        rho[:, :, k + 1] = rho[:, :, k - 1] \
                - 2 * dt * 1j * commutator(H, rho[:, :, k])
        rho[:, :, k + 1] /= rho[:, :, k + 1].trace()
    return rho

def liouville_evolve_t(psi0: np.ndarray, H: np.ndarray, t: np.ndarray) \
        -> np.ndarray:
    """
    Compute the time-dependent behavior of an initial state for a time-dependent
    Hamiltonian by numerically integrating the Liouville equation using a simple
    central finite difference scheme (Error = O(dt^2)).

    Parameters
    ----------
    psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Initial state.
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian (in s^-1). The first two indices must correspond to states
        (and be compatible with `psi0`) and the last must correspond to time
        (and be compatible with `t`).
    t : numpy.ndarray[dim=1, dtype=numpy.float64]
        Array of evenly spaced time gridpoints.

    Returns
    -------
    rho : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Density matrix of `psi0` evolved through time according to `H`. Has
        shape (I, J, K) where I and J are equal to the length of `psi0` and K is
        equal to the length of `t`.
    """
    dt = abs(t[1] - t[0])
    rho0 = np.outer(psi0, psi0.conj())
    rho = np.zeros(
        (rho0.shape[0], rho0.shape[1], t.shape[0]),
        dtype=np.complex128
    )
    rho[:, :, 0] = rho0
    rho[:, :, 1] = rho0
    for k in range(1, t.shape[0] - 1):
        rho[:, :, k + 1] = rho[:, :, k - 1] \
                - 2 * dt * 1j * commutator(H[:, :, k], rho[:, :, k])
        rho[:, :, k + 1] /= rho[:, :, k + 1].trace()
    return rho

def liouville_evolve_rk4(psi0: np.ndarray, H: np.ndarray, t: np.ndarray) \
        -> np.ndarray:
    """
    Compute the time-dependent behavior of an initial state for a time-dependent
    Hamiltonian by numerically integrating the Liouville equation using
    fourth-order Runge-Kutta (Error = O(dt^5)).

    Parameters
    ----------
    psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Initial state.
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian (in s^-1). The first two indices must correspond to states
        (and be compatible with `psi0`) and the last must correspond to time
        (and be compatible with `t`).
    t : numpy.ndarray[dim=1, dtype=numpy.float64]
        Array of evenly spaced time gridpoints.

    Returns
    -------
    rho : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Density matrix of `psi0` evolved through time according to `H`. Has
        shape (I, J, K) where I and J are equal to the length of `psi0` and K is
        equal to the length of `t`.
    """
    dt = abs(t[1] - t[0])
    rho0 = np.outer(psi0, psi0.conj())
    rho = np.zeros(
        (rho.shape[0], rho.shape[1], t.shape[0]),
        dtype=np.complex128
    )
    rho[:, :, 0] = rho0
    rho[:, :, 1] = rho0
    rhs = lambda H, p: -1j * commutator(H, p)
    for k in range(t.shape[0] - 2):
        rho_old = rho[:, :, k]

        r1 = rhs(H[:, :, k], rho_old)
        r2 = rhs(H[:, :, k + 1], psi_old + dt * r1)
        r3 = rhs(H[:, :, k + 1], psi_old + dt * r2)
        r4 = rhs(H[:, :, k + 2], psi_old + 2 * dt * r3)

        rho_new = rho_old + (dt / 3) * (r1 + 2 * r2 + 2 * r3 + r4)
        N = rho_new.trace()

        rho[:, :, k + 2] = rho_new / N
    return rho

def lindblad_evolve(psi0: np.ndarray, H: np.ndarray, Y: np.ndarray,
        t: np.ndarray) -> np.ndarray:
    """
    Compute the time-dependent behavior of an initial state by numerically
    integrating the Lindblad equation using a simple central finite difference
    scheme (Error = O(dt^2)).

    Parameters
    ----------
    psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Initial state.
    H : numpy.ndarray[dim=2, dtype=numpy.complex128]
        Hamiltonian (in s^-1). Must be square, of shape compatible with `psi0`.
    Y : numpy.ndarray[dim=2, dtype=numpy.float64]
        Decay rates in s^-1. Must be square, of shape compatible with `psi0`.
    t : numpy.ndarray[dim=1, dtype=numpy.float64]
        Array of evenly spaced time gridpoints.

    Returns
    -------
    rho : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Density matrix of `psi0` evolved through time according to `H`. Has
        shape (I, J, K) where I and J are equal to the length of `psi0` and K is
        equal to the length of `t`.
    """
    dt = abs(t[1] - t[0])
    rho0 = np.outer(psi0, psi0.conj())
    rho = np.zeros(
        (rho0.shape[0], rho0.shape[1], t.shape[0]),
        dtype=np.complex128
    )
    rho[:, :, 0] = rho0
    rho[:, :, 1] = rho0
    for k in range(1, t.shape[0] - 1):
        rho[:, :, k + 1] = rho[:, :, k - 1] \
                (2 * dt) * (
                    -1j * commutator(H, rho[:, :, k])
                    + lindbladian(Y, rho[:, :, k])
                )
        rho[:, :, k + 1] /= rho[:, :, k + 1].trace()
    return rho

def lindblad_evolve_t(psi0: np.ndarray, H: np.ndarray, Y: np.ndarray,
        t: np.ndarray) -> np.ndarray:
    """
    Compute the time-dependent behavior of an initial state for a time-dependent
    Hamiltonian by numerically integrating the Lindblad equation using a simple
    central finite difference scheme (Error = O(dt^2)).

    Parameters
    ----------
    psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Initial state.
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian (in s^-1). The first two indices must correspond to states
        (and be compatible with `psi0`) and the last must correspond to time
        (and be compatible with `t`).
    Y : numpy.ndarray[dim=2, dtype=numpy.float64]
        Decay rates in s^-1. Must be square, of shape compatible with `psi0`.
    t : numpy.ndarray[dim=1, dtype=numpy.float64]
        Array of evenly spaced time gridpoints.

    Returns
    -------
    rho : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Density matrix of `psi0` evolved through time according to `H`. Has
        shape (I, J, K) where I and J are equal to the length of `psi0` and K is
        equal to the length of `t`.
    """
    dt = abs(t[1] - t[0])
    rho0 = np.outer(psi0, psi0.conj())
    rho = np.zeros(
        (rho0.shape[0], rho0.shape[1], t.shape[0]),
        dtype=np.complex128
    )
    rho[:, :, 0] = rho0
    rho[:, :, 1] = rho0
    for k in range(1, t.shape[0] - 1):
        rho[:, :, k + 1] = rho[:, :, k - 1] \
                (2 * dt) * (
                    -1j * commutator(H[:, :, k], rho[:, :, k])
                    + lindbladian(Y, rho[:, :, k])
                )
        rho[:, :, k + 1] /= rho[:, :, k + 1].trace()
    return rho

def lindblad_evolve_rk4(psi0: np.ndarray, H: np.ndarray, Y: np.ndarray,
        t: np.ndarray) -> np.ndarray:
    """
    Compute the time-dependent behavior of an initial state for a time-dependent
    Hamiltonian by numerically integrating the Lindblad equation using
    fourth-order Runge-Kutta (Error = O(dt^5)).

    Parameters
    ----------
    psi0 : numpy.ndarray[dim=1, dtype=numpy.complex128]
        Initial state.
    H : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Hamiltonian (in s^-1). The first two indices must correspond to states
        (and be compatible with `psi0`) and the last must correspond to time
        (and be compatible with `t`).
    Y : numpy.ndarray[dim=2, dtype=numpy.float64]
        Decay rates in s^-1. Must be square, of shape compatible with `psi0`.
    t : numpy.ndarray[dim=1, dtype=numpy.float64]
        Array of evenly spaced time gridpoints.

    Returns
    -------
    rho : numpy.ndarray[dim=3, dtype=numpy.complex128]
        Density matrix of `psi0` evolved through time according to `H`. Has
        shape (I, J, K) where I and J are equal to the length of `psi0` and K is
        equal to the length of `t`.
    """
    dt = abs(t[1] - t[0])
    rho0 = np.outer(psi0, psi0.conj())
    rho = np.zeros(
        (rho.shape[0], rho.shape[1], t.shape[0]),
        dtype=np.complex128
    )
    rho[:, :, 0] = rho0
    rho[:, :, 1] = rho0
    rhs = lambda H, p: -1j * commutator(H, p) + lindbladian(Y, rho)
    for k in range(t.shape[0] - 2):
        rho_old = rho[:, :, k]

        r1 = rhs(H[:, :, k], rho_old)
        r2 = rhs(H[:, :, k + 1], psi_old + dt * r1)
        r3 = rhs(H[:, :, k + 1], psi_old + dt * r2)
        r4 = rhs(H[:, :, k + 2], psi_old + 2 * dt * r3)

        rho_new = rho_old + (dt / 3) * (r1 + 2 * r2 + 2 * r3 + r4)
        N = rho_new.trace()

        rho[:, :, k + 2] = rho_new / N
    return rho

def find_2pi_idx(X: np.ndarray, M: int=1, eps: float=1e-3) -> (int, float):
    """
    Find the index of the `M`-th local maximum in an array of oscillating
    values.
    """
    x0 = X[0]
    k0 = 0
    switch = [1 - 2 * (m % 2) for m in range(2 * M)]
    m = 0
    for k, x in enumerate(X):
        if switch[m] * (x - x0) <= -eps:
            x0 = x
            k0 = k
        elif switch[m] * (x - x0) > eps:
            if m < 2 * M - 1:
                m += 1
            else:
                break
    return (k0, x0)

