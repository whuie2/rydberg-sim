from __future__ import annotations
import numpy as np
from sympy.physics.wigner import wigner_3j
from enum import Enum
from itertools import product
import pathlib
import toml

class State(Enum):
    def __eq__(self, other):
        assert isinstance(other, (type(self), int))
        return self.value == other

    def __hash__(self):
        return hash(self.value)

    def spin(self) -> (float, float):
        raise NotImplemented

class Basis:
    def __init__(self, energies: dict[State, float]):
        self.energies = energies
        self.arrays = {
            S: np.array(
                [1.0 + 0.0j if s == S else 0.0 + 0.0j for s in energies.keys()],
                dtype=np.complex128
            )
            for S in energies.keys()
        }
        self.labels = list(energies.keys())

    def len(self) -> int:
        return len(self.labels)

    def __len__(self) -> int:
        return self.len()

    def get_order(self) -> list[State]:
        return self.labels

    def get_idx(self, state: State) -> int:
        return self.labels.index(state)

    def get_energy(self, state: State) -> float:
        return self.energies[state]

    def get_energy_i(self, idx: int) -> float:
        return self.energies[self.labels[idx]]

    def get_array(self, state: State) -> np.ndarray:
        return self.arrays[state]

    def get_array_i(self, idx: int) -> np.ndarray:
        return self.arrays[self.labels[idx]]

    def to_multiatom(self, n: int):
        return Basis({
            S: sum(self.get_energy(s) for s in S)
            for S in product(*[self.labels for k in range(n)])
        })

def trapz_prog(y, dx=1.0):
    return np.append(0.0, np.cumsum(dx * (y[:-1] + y[1:]) / 2.0))

def gen_time_dep(t, integrate: list[float | np.ndarray],
        extrude: list[float]) -> list[np.ndarray]:
    assert all(np.shape(X) in {t.shape, ()} for X in integrate)
    dt = abs(t[1] - t[0])
    ones = np.ones(t.shape)
    return [
        trapz_prog(X, dt) if len(np.shape(X)) > 0 else X * t
        for X in integrate
    ] + [X * ones for X in extrude]

def trans_w3j(F0: float, mF0: float, F1: float, mF1: float) -> float:
    return float(
        abs(
            np.sqrt(2.0 * F0 + 1.0)
            * wigner_3j(F1, 1, F0, mF1, mF0 - mF1, -mF0).doit().evalf()
        )
    )

def phase_angle(z: np.complex128) -> np.float64:
    with np.errstate(divide="ignore"):
        ph = np.log(z / abs(z)).imag
    return ph

def pulse_fn(name: str, docstring: str, principal: (State, State),
        parasitic: list[(State, State)]) -> type(lambda: np.ndarray):
    """
    Returns a function that gives the correct generates a Hamiltonian containing
    the correct driven couplings for a system. Returned functions have the
    signature

    func(
        basis: Basis,
        w: float,
        W: float,
        chi: float,
        t: numpy.ndarray,
        phi: float=0.0
    ) -> numpy.ndarray

    Parameters
    ----------
    name : str
        Name of the returned function.
    docstring : str
        Docstring of the returned function.
    principal : (State, State)
        Principal transition.
    parasitic : list[(State, State)]
        List of all other allowed transitions.

    Returns
    -------
    func : Function
        Function for generating Hamiltonian matrices with the above signature.
    """
    def hamiltonian(basis: Basis, w: float, W: float, chi: float, t: np.ndarray,
            phi: float=0.0) -> np.ndarray:
        n = basis.len()
        energies = [basis.get_energy_i(k) for k in range(n)]

        H = np.zeros((n, n, len(t)), dtype=np.complex128)
        transitions = product(
            enumerate(basis.get_order()), enumerate(basis.get_order()))
        N_w3j = trans_w3j(
            principal[0].spin()[0],
            principal[0].spin()[0],
            principal[1].spin()[0],
            principal[1].spin()[0],
        )
        q = principal[1].spin()[1] - principal[0].spin()[1]

        pol = lambda s1, s2: (
            np.sqrt(1.0 - chi) if s2.spin()[1] - s1.spin()[1] == q
            else np.sqrt(chi / 2.0)
        )
        cg = lambda s1, s2: (
            trans_w3j(*s1.spin(), *s2.spin()) / N_w3j
        )
        drive = lambda i, j: (
            (W / 2.0)
            * np.exp(+1.0j * ((w - (energies[j] - energies[i])) * t + phi))
            ,
            (W / 2.0)
            * np.exp(-1.0j * ((w - (energies[j] - energies[i])) * t + phi))
        )

        for ((i, s1), (j, s2)) in transitions:
            if (s1, s2) == principal or (s1, s2) in parasitic:
                _pol = pol(s1, s2)
                _cg = cg(s1, s2)
                _drive = drive(i, j)
                H[j, i, :] = _pol * _cg * _drive[0]
                H[i, j, :] = _pol * _cg * _drive[1]
        return H
    hamiltonian.__name__ = name
    hamiltonian.__doc__ = docstring
    return hamiltonian

def pulse_fn_t(name: str, docstring: str, principal: (State, State),
        parasitic: list[(State, State)]) -> type(lambda: np.ndarray):
    """
    Returns a function that gives the correct generates a Hamiltonian containing
    the correct driven couplings for a system. Returned functions have the
    signature

    func(
        basis: Basis,
        w: numpy.ndarray,
        W: numpy.ndarray,
        chi: numpy.ndarray,
        t: numpy.ndarray,
        phi: float=0.0
    ) -> numpy.ndarray

    Parameters
    ----------
    name : str
        Name of the returned function.
    docstring : str
        Docstring of the returned function.
    principal : (State, State)
        Principal transition.
    parasitic : list[(State, State)]
        List of all other allowed transitions.

    Returns
    -------
    func : Function
        Function for generating Hamiltonian matrices for time dependent drives
        with the above signature.
    """
    def hamiltonian_t(basis: Basis, w: np.ndarray, W: np.ndarray, chi: np.ndarray,
            t: np.ndarray, phi: float=0.0) -> np.ndarray:
        if not all(len(X) == len(t) for X in [w, W, chi]):
            raise Exception("Encountered non-equivalent time dependences")

        n = basis.len()
        energies = [basis.get_energy_i(k) for k in range(n)]
        time_dep = gen_time_dep(t, energies + [w], [W, chi])
        # time_dep = [E1, ..., En, w, W, chi]

        H = np.zeros((n, n, len(t)), dtype=np.complex128)
        transitions = product(
            enumerate(basis.get_order()), enumerate(basis.get_order()))
        N_w3j = trans_w3j(
            principal[0].spin()[0],
            principal[0].spin()[0],
            principal[1].spin()[0],
            principal[1].spin()[0],
        )
        q = principal[1].spin()[1] - principal[0].spin()[1]

        pol = lambda s1, s2: (
            np.sqrt(1.0 - chi) if s2.spin()[1] - s1.spin()[1] == q
            else np.sqrt(chi / 2.0)
        )
        cg = lambda s1, s2: (
            trans_w3j(*s1.spin(), *s2.spin()) / N_w3j
        )
        drive = lambda i, j: (
            (W / 2.0)
            * np.exp(+1.0j * ((w - (energies[j] - energies[i])) * t + phi))
            ,
            (W / 2.0)
            * np.exp(-1.0j * ((w - (energies[j] - energies[i])) * t + phi))
        )

        for ((i, s1), (j, s2)) in transitions:
            if (s1, s2) == principal or (s1, s2) in parasitic:
                _pol = pol(s1, s2)
                _cg = cg(s1, s2)
                _drive = drive(i, j)
                H[j, i, :] = _pol * _cg * _drive[0]
                H[i, j, :] = _pol * _cg * _drive[1]
        return H
    hamiltonian_t.__name__ = name
    hamiltonian_t.__doc__ = docstring
    return hamiltonian_t

def pulse_fn_poincare(name: str, docstring: str,
        transitions: list[(State, State)]) -> type(lambda: np.ndarray):
    """
    Returns a function that gives the correct generates a Hamiltonian containing
    the correct driven couplings for a system. Returned functions have the
    signature

    func(
        basis: Basis,
        w: float,
        W: float,
        alpha: float,
        beta: float,
        theta: float,
        t: numpy.ndarray,
        phi: float=0.0
    ) -> numpy.ndarray

    Parameters
    ----------
    name : str
        Name of the returned function.
    docstring : str
        Docstring of the returned function.
    transitions : list[(State, State)]
        List of all allowed transitions.

    Returns
    -------
    func : Function
        Function for generating Hamiltonian matrices with the above signature.
    """
    def hamiltonian(
        basis: Basis,
        w: float,
        W: float,
        alpha: float,
        beta: float,
        theta: float,
        t: np.ndarray,
        phi: float=0.0
    ) -> np.ndarray:
        n = basis.len()
        energies = [basis.get_energy_i(k) for k in range(n)]

        H = np.zeros((n, n, len(t)), dtype=np.complex128)
        pairs = product(
            enumerate(basis.get_order()), enumerate(basis.get_order()))
        _w3j = [
            (s1, s1.spin(), s2, s2.spin(), trans_w3j(*s1.spin(), *s2.spin()))
            for s1, s2 in transitions
        ]
        N_w3j = max(
            trans_w3j(*s1.spin(), *s2.spin()) for s1, s2 in transitions)

        pol_weights = {
            -1: (
                np.cos(alpha)
                - 1.0j * np.exp(1.0j * beta) * np.sin(alpha) * np.cos(theta)
            ) / np.sqrt(2.0),
             0: np.exp(1.0j * beta) * np.sin(alpha) * np.sin(theta),
            +1: (
                np.cos(alpha)
                + 1.0j * np.exp(1.0j * beta) * np.sin(alpha) * np.cos(theta)
            ) / np.sqrt(2.0),
        }
        pol = lambda s1, s2: (
            pol_weights.get(round(s2.spin()[1] - s1.spin()[1]), 0.0)
        )
        cg = lambda s1, s2: (
            trans_w3j(*s1.spin(), *s2.spin()) / N_w3j
        )
        drive = lambda i, j: (
            (W / 2.0)
            * np.exp(+1.0j * ((w - (energies[j] - energies[i])) * t + phi))
        )

        for ((i, s1), (j, s2)) in pairs:
            if (s1, s2) in transitions:
                _pol = pol(s1, s2)
                _cg = cg(s1, s2)
                _drive = drive(i, j)
                X = _pol * _cg * _drive
                H[j, i, :] = X
                H[i, j, :] = X.conj()
        return H
    hamiltonian.__name__ = name
    hamiltonian.__doc__ = docstring
    return hamiltonian

def pulse_fn_poincare_t(
    name: str,
    docstring: str,
    transitions: list[(State, State)]
) -> type(lambda: np.ndarray):
    """
    Returns a function that gives the correct generates a Hamiltonian containing
    the correct driven couplings for a system. Returned functions have the
    signature

    func(
        basis: Basis,
        w: numpy.ndarray,
        W: numpy.ndarray,
        alpha: numpy.ndarray,
        beta: numpy.ndarray,
        t: numpy.ndarray,
        phi: float=0.0
    ) -> numpy.ndarray

    Parameters
    ----------
    name : str
        Name of the returned function.
    docstring : str
        Docstring of the returned function.
    transitions : list[(State, State)]
        List of all allowed transitions.

    Returns
    -------
    func : Function
        Function for generating Hamiltonian matrices for time dependent drives
        with the above signature.
    """
    def hamiltonian_t(
        basis: Basis,
        w: np.ndarray,
        W: np.ndarray,
        alpha: np.ndarray,
        beta: np.ndarray,
        theta: np.ndarray,
        t: np.ndarray,
        phi: float=0.0
    ) -> np.ndarray:
        if not all(len(X) == len(t) for X in [w, W, alpha, beta, theta]):
            raise Exception("Encountered non-equivalent time dependences")

        n = basis.len()
        energies = [basis.get_energy_i(k) for k in range(n)]
        time_dep = gen_time_dep(t, energies + [w], [W, alpha, beta, theta])
        # time_dep = [E1, ..., En, w, W, alpha, beta, theta]

        H = np.zeros((n, n, len(t)), dtype=np.complex128)
        pairs = product(
            enumerate(basis.get_order()), enumerate(basis.get_order()))
        N_w3j = trans_w3j(
            transitions[0][0].spin()[0],
            transitions[0][0].spin()[0],
            transitions[0][1].spin()[1],
            transitions[0][1].spin()[1],
        )

        pol_weights = {
            -1: (
                np.cos(alpha)
                + 1.0j * np.exp(1.0j * beta) * np.sin(alpha) * np.cos(theta)
            ) / np.sqrt(2.0),
             0: np.exp(1.0j * beta) * np.sin(alpha) * np.sin(theta),
            +1: (
                np.cos(alpha)
                + 1.0j * np.exp(1.0j * beta) * np.sin(alpha) * np.cos(theta)
            ) / np.sqrt(2.0),
        }
        pol = lambda s1, s2: (
            pol_weights.get(round(s2.spin()[1] - s1.spin()[1]), 0.0)
        )
        cg = lambda s1, s2: (
            trans_w3j(*s1.spin(), *s2.spin()) / N_w3j
        )
        drive = lambda i, j: (
            (W / 2.0)
            * np.exp(+1.0j * ((w - (energies[j] - energies[i])) * t + phi))
        )

        for ((i, s1), (j, s2)) in pairs:
            if (s1, s2) in transitions:
                _pol = pol(s1, s2)
                _cg = cg(s1, s2)
                _drive = drive(i, j)
                X = _pol * _cg * _drive
                H[j, i, :] = X
                H[i, j, :] = X.conj()
        return H
    hamiltonian_t.__name__ = name
    hamiltonian_t.__doc__ = docstring
    return hamiltonian_t

def couplings_fn_lindblad(name: str, docstring: str,
        decay: tuple[list[(State, State)], float] | dict[(State, State), float]
    ) -> type(lambda: np.ndarray):
    """
    Returns a function that generates a matrix containing the
    Clebsch-Gordan-weighted decay couplings for a system. A non-zero element in
    the [i, j]-th position gives a decay pathway from the j-th state in the
    basis to the i-th. Returned functions have the signature

    func(basis: Basis) -> numpy.ndarray

    Parameters
    ----------
    name : str
        Name of the returned function.
    docstring : str
        Docstring of the returned function.
    decay : tuple[list[(State, State)], float] or dict[(State, State), float]
        List of all decay pathways with nominal decay rate for each one (in
        rad/s) or dictionary of decay pathways with decay rates for each one.

    Returns
    -------
    func : Function
        Function for generating Lindblad coupling matrices with the above
        signature.
    """
    if (
        isinstance(decay, tuple)
        and len(decay) == 2
        and isinstance(decay[0], list)
        and isinstance(decay[1], float)
    ):
        _decay = {decay_path: decay[1] for decay_path in decay[0]}
    elif isinstance(decay, dict):
        _decay = decay.copy()
    else:
        raise Exception("invalid decay pathways provided")
    def couplings(basis: Basis) -> np.ndarray:
        n = basis.len()
        transitions = product(basis.get_order(), basis.get_order())
        Y = np.array([
            _decay[(s2, s1)] * trans_w3j(*s1.spin(), *s2.spin())
            if (s2, s1) in _decay.keys() else 0.0
            for s1, s2 in transitions
        ]).reshape((n, n))
        return Y
    couplings.__name__ = name,
    couplings.__doc__ = docstring
    return couplings

def zm_br_fn(name: str, docstring: str, A: list[float]) \
    -> type(lambda float: float):
    """
    Returns a function that computes the nonlinear Zeeman shift for a specific
    state using the Breit-Rabi form

    A[0] + A[1] * x + A[2] * sqrt(1 + A[3] * x + A[4] * x**2)

    where A[k] are the constants passed here. The returned function has the
    signature

    func(B: float) -> float

    where B is the magnetic field strength in G, and the returned value is in
    MHz.

    Parameters
    ----------
    name : str
        Name of the returned function.
    docstring : str
        Docstring of the returned function.
    A : list[float]
        Parameters for the returned function.

    Returns
    -------
    func : Function
        Function for calculating the nonlinear Zeeman shift.
    """
    def zm_br(B: float) -> float:
        return (
            A[0] + A[1] * B + A[2] * np.sqrt(1 + A[3] * B + A[4] * B**2)
        )
    zm_br.__name__ = name
    zm_br.__doc__ = docstring
    return zm_br

